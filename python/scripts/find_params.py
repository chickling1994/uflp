import numpy as np
import matplotlib.pyplot as plt

current_neg = -671 / 2**11
current_pos = 1416 / 2**11
current_zer = 347 / 2**11

bias_neg = -4701 / 2**5
bias_pos = -1701 / 2**5
bias_zer = -2453 / 2**5

cap_measure = bias_zer

Sampled_Temp = 3000 / 2**7
Sampled_V_Float = 4295 / 2**5

I_Sat_Guess = (current_neg / (np.exp((bias_neg - Sampled_V_Float)/Sampled_Temp)-1))
Temp_Guess = ((bias_pos - Sampled_V_Float)/(np.log(current_pos/I_Sat_Guess + 1)))
Temp_Guess = 3000/2**7
V_Float_Guess = (bias_zer - Temp_Guess*(np.log(current_zer/I_Sat_Guess + 1)))

I_Sat = I_Sat_Guess * 2**11
Te = Temp_Guess * 2**7
V_Float = V_Float_Guess * 2**5

print("I_Sat is " + str(I_Sat))
print("Te is " + str(Te))
print("V_Float is " + str(V_Float))

