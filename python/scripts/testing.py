import numpy as np
from prettytable import PrettyTable
    
x = PrettyTable()
y = PrettyTable()


x.field_names = ["Bias_State","Bias","Bias VHDL Input", "Current", "Current VHDL Input"]
y.field_names = ["","iSat","iSat VHDL","Temperature","Temperature VHDL","vFloat","vFloat VHDL"]

bias = np.zeros(3)
current = np.zeros(3)

# iSat = 0.25
# Te = 28.43
# Vf = -74.75


iSat = 511 / 2**11
Te = 1120 / 2**5
Vf = -2400 / 2**5

# iSat = 512 / 2**11
# Te = 3639 / 2**7
# Vf = -2392 / 2**5

# neg_bais_val = -6328.0 / 2**5
# pos_bais_val = -1080.0 / 2**5
# zero_bais_val = -2392.0 / 2**5
# neg_bais_val = -122 / 2**5
# pos_bais_val = -2626 / 2**5
# zero_bais_val = -2000 / 2**5
# bias_vals = [neg_bais_val, zero_bais_val , pos_bais_val]
bias_vals = [-3*Te + Vf, 0 + Vf , Te + Vf]
j = 0
for i in bias_vals:
    # current[j] = iSat * (- 1.0 + np.exp((Vf - i)/Te))
    # current[j] = iSat* (-1 + np.exp((Vf - i)/Te))
    current[j] = iSat * (-1 + np.exp((i - Vf)/Te))
    j += 1

for i in [0,1,2]:
    bias[i] =  Vf + Te*np.log((current[i]/iSat) + 1)

iSat_Calc = current[0] / (np.exp((bias[0] - Vf)/(Te)) - 1)
Temp_Calc = (bias[2] - Vf)/(np.log(current[2]/iSat + 1))
vFloat_Calc = bias[1] - Te * np.log(current[1]/iSat + 1)
# vFloat_Calc = -Te - Te * np.log(current[2]/iSat + 1)

x.add_row(["neg_bais", np.round(bias[0],2), np.round(bias[0]*2**5,2), np.round(current[0],2), np.round(current[0] * 2**11,2)])
x.add_row(["zero_bais", np.round(bias[1],2), np.round(bias[1]*2**5,2), np.round(current[1],2), np.round(current[1] * 2**11,2)])
x.add_row(["pos_bais", np.round(bias[2],2), np.round(bias[2]*2**5,2), np.round(current[2],2), np.round(current[2] * 2**11,2)])

y.add_row(["Value", np.round(iSat_Calc,2), np.round(iSat_Calc * 2**11,2), np.round(Temp_Calc,2), np.round(Temp_Calc*2**7,2), np.round(vFloat_Calc,2), np.round(vFloat_Calc* 2**5,2)])
y.add_row(["Original", np.round(iSat,2), np.round(iSat*2**11,2), np.round(Te,2), np.round(Te * 2**7,2), np.round(Vf,2), np.round(Vf * 2**5 ,2)])

print(x)
print(y)