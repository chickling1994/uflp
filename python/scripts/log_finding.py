import numpy as np
import math
import matplotlib.pyplot as plt

x = 8.0 
x_array = np.arange(x,8000,x) 
y = 7.0 
y_array = np.arange(y,7000,y) 

best_result = 1
index = []
result = np.zeros(len(x_array))
best_index = 0 
for i in range(0,100):
    result[i] = abs(math.log(y) * i,2) - int(math.log(y*i,2)))
    # if (i / y == int(i/y)) :
        # print("Ignored ", i)
    # else:
    index.append(i) 
    if result[i] < best_result :
        best_result = result[i]
        best_index = i
        
    
plt.plot(index,result[index], 'o')
plt.show()