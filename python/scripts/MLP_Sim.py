import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
from bitstring import BitArray
import csv

# from MLP_Sim_Optimum_Cap import Ion_Saturation

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = printEnd)
    # Print New Line on Complete
    if iteration == total: 
        print()

def LP_Current(Voltage ,Ion_Saturation, Electron_Temperature, Floating_Potential):
    solved_current = Ion_Saturation*(np.exp((Voltage-Floating_Potential)/Electron_Temperature) - 1)
    return solved_current

def smoothing_average(x, n):
    smoothed_average = np.empty(0)
    for i in range(n,len(x)-n):
        smoothed_average = np.append(smoothed_average, np.mean(x[i-n:i+n]))
    return smoothed_average

def dummy_probe_current(Voltage):
    solved_current = line(Voltage)
    return solved_current

# voltages = np.linspace(-100,20,1000)
# test_LP_Curve = []
# for i in range(0, len(voltages)):
#     test_LP_Curve.append(LP_Current(voltages[i],0.2,30,-70))


def MLP_sat_current(Current, Voltage, Floating_Potential, Electron_Temperature):
    return Current/(np.exp((Voltage-Floating_Potential)/Electron_Temperature) - 1)

def MLP_temperature(Current, Voltage, Floating_Potential, Ion_Saturation):
    return (Voltage - Floating_Potential)/np.log((Current/Ion_Saturation) + 1)

def MLP_floating(Current, Voltage, Electron_Temperature, Ion_Saturation):
    return Voltage - Electron_Temperature*np.log((Current/Ion_Saturation) + 1)

def temperature_plot():
    fig, ax1 = plt.subplots()
    color = 'tab:blue'
    ax1.scatter(x, Temperature_guess, s=2, color=color)
    ax1.plot(x, Te_Calc_Index , color='tab:orange')
    ax1.set_title("Electron Temperature time evolution with capacitence")
    ax1.set_xlabel("Time, s")
    # ax1.set_xlim(left=0, right=max(x))
    # ax1.set_ylim(bottom=np.mean(Temperature)*(0.5),top=np.mean(Temperature)*(1.5))
    # ax1.set_ylim(bottom=29.5,top=35)
    ax1.set_ylabel("Electron Temperature, eV", color=color)
    ax1.tick_params(axis='y',labelcolor=color)
    
    # color = 'tab:red'
    # ax2 = ax1.twinx()
    # ax2.scatter(x, capacitor_time*1E9, s=1, color=color)
    # ax2.set_ylabel("Capacitence, nF", color=color)
    # ax2.tick_params(axis='y',labelcolor=color)

    plt.show()

def iSat_plot():
    fig, ax1 = plt.subplots()
    color = 'tab:blue'
    ax1.scatter(x, iSat_guess, s=1, color=color)
    ax1.plot(x, iSat_Calc_Index, color='tab:orange')
    ax1.set_title("Ion Saturation Current time evolution with capacitence")
    ax1.set_xlabel("Time, s")
    # ax1.set_xlim(left=0, right=max(x))
    ax1.set_ylabel("iSat, A", color=color)
    ax1.tick_params(axis='y',labelcolor=color)

    # color = 'tab:red'
    # ax2 = ax1.twinx()
    # ax2.scatter(x, capacitor_time*1E9, s=0.1, color=color)
    # ax2.set_ylabel("Capacitence, nF", color=color)
    # ax2.tick_params(axis='y',labelcolor=color)

    plt.show()

def Vf_plot():
    fig, ax1 = plt.subplots()
    color = 'tab:blue'
    ax1.scatter(x, vFloat_guess, s=1, color=color)
    ax1.scatter(x, Vf_Calc_Index, s=1 , color='tab:orange')
    ax1.set_title("Floating Potential time evolution with capacitence")
    ax1.set_xlabel("Time, s")
    # ax1.set_xlim(left=0, right=max(x))
    # ax1.set_ylim(top=-69, bottom=-75)
    ax1.set_ylabel("Floating Potential, V", color=color)
    ax1.tick_params(axis='y',labelcolor=color)
    
    # color = 'tab:red'
    # ax2 = ax1.twinx()
    # ax2.scatter(x, capacitor_time*1E9, s=1, color=color)
    # ax2.set_ylabel("Capacitence, nF", color=color)
    # ax2.tick_params(axis='y',labelcolor=color)

    plt.show()

def Probe_Voltage_Plot():
    fig, ax1 = plt.subplots()
    color = 'tab:blue'
    ax1.plot(x, Vp, color=color)
    ax1.set_title("Probe Voltage time evolution with capacitence")
    ax1.set_xlabel("Time, s")
    ax1.set_xlim(left=0, right=max(t))
    ax1.set_ylabel("Probe Voltage, V", color=color)
    ax1.tick_params(axis='y',labelcolor=color)
    
    # color = 'tab:red'
    # ax2 = ax1.twinx()
    # ax2.scatter(t, t_capacitor*1E9, s=1, color=color)
    # ax2.set_ylabel("Capacitence, nF", color=color)
    # ax2.tick_params(axis='y',labelcolor=color)
    # ax2.set_xlim(left=0, right=max(t))

    plt.show()

def converge_plot():
    fig, ax1 = plt.subplots()
    color = 'tab:blue'
    ax1.plot(Temperature_Range, Converged_Temp, color=color)
    ax1.set_title("")
    ax1.set_xlabel("")
    ax1.set_xlim(left=0, right=max(t))
    ax1.set_ylabel("", color=color)
    ax1.tick_params(axis='y',labelcolor=color)
    
    # color = 'tab:red'
    # ax2 = ax1.twinx()
    # ax2.scatter(, t_capacitor*1E9, s=1, color=color)
    # ax2.set_ylabel("Capacitence, nF", color=color)
    # ax2.tick_params(axis='y',labelcolor=color)
    # ax2.set_xlim(left=0, right=max(t))

    plt.show()


LP_Time_Data = np.empty(0)
LP_Ch1_Data = np.empty(0)
LP_Ch2_Data = np.empty(0)
# with open('IV responce dummy probe 10kR_1kR - Modifed Range.csv') as csv_file:
with open('IV responce dummy probe 10kR_1kR - 1V_max.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    next(csv_reader, None)
    line_count = 1
    for row in csv_reader:
        LP_Time_Data = np.append(LP_Time_Data,float(row[0]))
        LP_Ch1_Data = np.append(LP_Ch1_Data,float(row[1]))
        LP_Ch2_Data = np.append(LP_Ch2_Data,float(row[2]))
        line_count += 1
Dummy_Voltage = smoothing_average(LP_Ch1_Data,5)
Dummy_Current = smoothing_average(LP_Ch2_Data,5) / 100
fit = np.polyfit(Dummy_Voltage,Dummy_Current,5)
line = np.poly1d(fit)

DAC_bits = 14
def lim_14_bit(x, bits=DAC_bits):
    if np.isnan(x) or np.isinf(x):
        x = 0
    a.int = int(x)
    return a[bit_length-bits:bit_length].int
    # return x



data_len = 1000
iterations = 1
x = np.arange(0,data_len)
cycle_rate = 1E6 # 1MHz
bias_rate = cycle_rate * 3
time_step = 1E-8
change_initial_time = int(1/(cycle_rate * time_step))

start_time = 0.0
stop_time = 0.1
t = np.arange(start_time, stop_time, time_step)


iSat = 0.02
Temperature = 1.5
vFloat = -8

# iSat = 2500/2**15
# Temperature = 3072/2**11
# vFloat = -4096/2**9

min_Te = Temperature
max_Te = 7
iSat_Calc_Index = np.zeros(len(x))
Te_Calc_Index = np.zeros(len(x))
Vf_Calc_Index = np.zeros(len(x))

for i in range(0, len(x)):
    iSat_Calc_Index[i] = iSat*(1+np.random.random_sample()*0.00) # A
    Vf_Calc_Index[i] =  vFloat*(1+np.random.random_sample()*0.00) # V
    Te_Calc_Index[i] =  Temperature*(1+np.random.random_sample()*0.00) # V

# for i in range(int(data_len/2), data_len):
    # Te_Calc_Index[i] = (min_Te + 2*(i-int(data_len/2))*max_Te/len(x))*(1+np.random.random_sample()*0.01) # eV
    # Vf_Calc_Index[i] = (min_Te + 2*(i-int(data_len/2))*max_Te/len(x))*(1+np.random.random_sample()*0.01) # eV

Temperature_shift = 11
iSat_shift = Temperature_shift + 4
vFloat_shift = Temperature_shift - 2

bit_length = 1024
a = BitArray(bit_length)

iSat_guess = np.zeros(data_len)
Temperature_guess = np.zeros(data_len)
vFloat_guess  = np.zeros(data_len)

iterations_3 = int(np.round(iterations/3))
Temperature_Range = np.linspace(-2,3,iterations_3)
vFloat_Range = np.linspace(-8,8,iterations_3)
iSat_Range = np.linspace(-1,1,iterations_3)
Converged_Temp = np.zeros(iterations)
Converged_iSat = np.zeros(iterations)
Converged_vFloat = np.zeros(iterations)

iSat_guess[0] = 7000/2**iSat_shift
Temperature_guess[0] = 7000/2**Temperature_shift
vFloat_guess[0] = 0



Vp = np.zeros([data_len,3])
# Bias_Multipliers = [-2.4, 0.64, 0]
Bias_Multipliers = [-2.4, 0.64, 0]
I = np.zeros([data_len,3])

current_offset = 0
voltage_offset = 0

# for k in range(0,1)):
for k in range(0,iterations):
    # if k < iterations_3:
    #     vFloat = vFloat_Range[k]
    # elif k >= iterations_3 or k < 2*iterations_3:
    #     Temperature = Temperature_Range[k]
    # else:
    #     iSat = iSat_Range[k]
    for i in range(1, data_len):
        for j in range(0,len(Bias_Multipliers)):
            if Temperature_guess[i-1] < 500/2**Temperature_shift :
            # if Temperature_guess[i-1] == 0 :
                # Temperature_guess[i-1] = 50*(8192/2**Temperature_shift)/2**14#Temperature_guess[0]
                Temperature_guess[i-1] = Temperature_guess[0]
            # Bias_Value = (Bias_Multipliers[j]*Temperature_guess[i-1] + vFloat_guess[i-1]) *(1+np.random.random_sample()*0.00)
            Bias_Value = (Bias_Multipliers[j]*Temperature_guess[i-1] + vFloat) *(1+np.random.random_sample()*0.00)
            # Bias_Value = (Bias_Multipliers[j]*Temperature_guess[i-1] + vFloat_guess[i-1]) *(1+np.random.random_sample()*0.05)
            Vp[i-1][j] = lim_14_bit(Bias_Value*2**vFloat_shift + voltage_offset,DAC_bits) / 2**vFloat_shift
            # Vp[i-1][j] = Bias_Value
            # Vp[i-1][j] = Vp[i-1][j]*(1+np.random.random_sample()*0.01)
            # I[i-1][j] = LP_Current(Vp[i-1][j],iSat, Temperature, vFloat_Range[k]) + LP_Current(Vp[i-1][j],iSat, Temperature, vFloat_Range[k])*np.random.random_sample()*0.05
            Current_value = LP_Current(Vp[i-1][j],iSat, Temperature, vFloat) *(1+np.random.random_sample()*0.00) 
            # Current_value = dummy_probe_current(Vp[i-1][j])
            I[i-1][j] = lim_14_bit(Current_value*2**iSat_shift + current_offset,DAC_bits) / 2**iSat_shift 

        iSat_Value = MLP_sat_current(I[i-1][0],Vp[i-1][0],vFloat_guess[i-1],Temperature_guess[i-1])
        iSat_guess[i] = lim_14_bit(iSat_Value*2**iSat_shift) / 2**iSat_shift

        te_value = MLP_temperature(I[i-1][1],Vp[i-1][1],vFloat_guess[i-1],iSat_guess[i])
        Temperature_guess[i] = lim_14_bit(te_value*2**Temperature_shift) / 2**Temperature_shift

        Vf_value = MLP_floating(I[i-1][2],Vp[i-1][2],Temperature_guess[i], iSat_guess[i])
        # vFloat_guess[i] = lim_14_bit(Vf_value*2**vFloat_shift) / 2**vFloat_shift
        vFloat_guess[i] = Vf_value
        

        Converged_Temp[k] = Temperature_guess[i]
        Converged_iSat[k] = iSat_guess[i]
        Converged_vFloat[k] = vFloat_guess[i]
    
print(np.mean(iSat_guess))
print(np.mean(Temperature_guess))
print(np.mean(vFloat_guess))

Converged_Temp[np.isnan(Converged_Temp)] = 0
Converged_iSat[np.isnan(Converged_iSat)] = 0
Converged_vFloat[np.isnan(Converged_vFloat)] = 0