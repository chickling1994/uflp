########################
###                 ####
###  Marcus Law     ####
###  Langmiur Probe ####
###  Test           ####
###                 ####
########################
########################

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit, fsolve
from scipy.signal import savgol_filter as sav
import scipy.constants as const

def MLP_Voltage_Current(Voltage, Current):
    num = 20
    num_pointer = num
    line = np.polyfit(Voltage, Current, num)
    High_Res_Voltage = np.linspace(-10,10,2**14)
    High_Res_Current = np.zeros(len(High_Res_Voltage))
    for i in range(0, len(High_Res_Voltage)):
        for m in line:
            High_Res_Current[i] = (High_Res_Voltage[i]**num_pointer)*m + High_Res_Current[i]
            num_pointer = num_pointer - 1
        if High_Res_Voltage[i] > 5.09:
            High_Res_Current[i] = 0.112
        num_pointer = num
            
    plt.figure()
    plt.plot(Voltage, Current)
    plt.plot(High_Res_Voltage,High_Res_Current)
    return High_Res_Voltage, High_Res_Current

### filenames ### ### put the filenames here

test4 = '12 mTorr 200W DC 80mm Flag.csv'

#### read in data ####

Voltage = np.loadtxt(test4,delimiter = ';',skiprows = 10, usecols = 0)
Current = np.loadtxt(test4,delimiter = ';', skiprows = 10,  usecols = 1)

High_Res_Voltage, High_Res_Current = MLP_Voltage_Current(Voltage[200:277], Current[200:277])
#Current = Current + (4.2*10**-5) ### offset from the box


##### binning data #####

bin_num = 10

startindiciesx = 0
endindiciesx = startindiciesx + bin_num
binx = []
for b in np.arange(len(Voltage)/bin_num):
    binsx = np.average(Voltage[startindiciesx:endindiciesx])
    binx.append(binsx)
    startindiciesx = startindiciesx + bin_num
    endindiciesx = endindiciesx + bin_num

startindiciesy = 0
endindiciesy = startindiciesy + bin_num
biny = []
for bb in np.arange(len(Current)/bin_num):
    binsy = np.average(Current[startindiciesy:endindiciesy])
    biny.append(binsy)
    startindiciesy = startindiciesy + bin_num
    endindiciesy = endindiciesy + bin_num
    

#Voltage = np.asarray(binx)     ## uncomment these to use binning
#Current = np.asarray(biny)     ## uncomment these to use binning

############smoothing######## 

smooth = sav(Current,21,2)
binsmooth = sav(biny,3,2)

### derivatives and associated coordinates ###

N = len(Current)

da = np.diff(smooth) ## diff in neighbouring I 
dx = np.diff(Voltage) ## diff in neighbouring V

D = da/dx       ## 1st derivitive 
x1 = (Voltage[0:N-1] + Voltage[1:N])/2  ## x coordinates of the first derivative

N2 = len(x1)

D2 = np.diff(D)/np.diff(x1) ## 2nd derivative
x2 = (x1[0:N2-1] + x1[1:N2])/2 ## x coordinates of the second deriv

grad = np.gradient(Current,Voltage)
gradsmooth = np.gradient(smooth,Voltage)
SG1 = sav(Current,21,2,deriv = 1)
SG2 = sav(Current,21,3, deriv = 2)

##### Finding the plasma potential ####

max_grad = max(SG1) # maxiumum gradient
index_of_max = np.where(SG1 == max_grad)
turning_point_V = Voltage[(index_of_max)]
turning_point_I = Current[(index_of_max)]

##### finding the floating potential ####

FloatV = np.interp(0,smooth,Voltage)


#### plotting ###### 

plt.figure(1)
plt.plot (Voltage,Current,'kx', label = 'data')    
plt.plot(Voltage,smooth, 'rx', label = 'smoothed data')
plt.plot(turning_point_V,turning_point_I, 'X', label = '1st deriv Plasma potential')
plt.plot(FloatV,0, 'X', label = 'Floating potential')
plt.xlabel(r'$Voltage (V)$')
plt.ylabel("Current (A)")
plt.legend()



##### ion current peters method  #####

xion = []  # ion region
yion = []  # ion region
for j in np.arange(len(smooth)):
    if Voltage[j] < FloatV -20:
        xion.append(Voltage[j])
        yion.append(smooth[j])
        
def ion_current(xion, A, B):
    return A*np.sqrt(turning_point_V - xion) + B*(turning_point_V - xion)


poption, pcovion = curve_fit(ion_current, xion, yion)
xdataion = np.linspace(np.min(xion),np.max(xion),100)
ydataion = ion_current(xdataion, poption[0],poption[1])


plt.figure(2)
plt.plot (xion,yion,'kx', label = 'ion current region')    
plt.plot(xdataion,ydataion, 'r', label = 'ion fit')
plt.xlabel(r'$Voltage (V)$')
plt.ylabel("Current (A)")
plt.legend()



xionplasmaV = []   # data upto the plasma potential
yionplasmaV = []   # data upto the plasma potential
xafterplasmaV = []  # data after the plasma potential
yafterplasmaV = []  # data after the plasma potential
for p in np.arange(len(smooth)):
    if Voltage[p] < turning_point_V :
        xionplasmaV.append(Voltage[p])
        yionplasmaV.append(smooth[p])
    if Voltage[p] > turning_point_V:
        xafterplasmaV.append(Voltage[p])
        yafterplasmaV.append(smooth[p])


extrapolation = ion_current(xionplasmaV,poption[0],poption[1]) # extrapolation to plasma potential
subtractedioncurrent = yionplasmaV- extrapolation # data minus ion current

corrected_ydata = np.concatenate([subtractedioncurrent,yafterplasmaV]) ### the data - ion current
corrected_xdata = np.concatenate([xionplasmaV,xafterplasmaV])   ## corresponding voltage axis 

#### logging the current axis ####

logy = []
logx = []
for m in np.arange(len(corrected_ydata)):
    if corrected_xdata[m] > FloatV :
        logy.append(corrected_ydata[m])
        logx.append(corrected_xdata[m])
        

lnI = np.log(logy) # logging the current axis

lnIyfittingregion1 = []
logxfittingregion1 = []
for m in np.arange(len(lnI)):
    if FloatV < logx[m] < turning_point_V:                   ####### fitting region for fit 1
        lnIyfittingregion1.append(lnI[m])
        logxfittingregion1.append(logx[m])
        


def straightline(logxfittingregion1, a, b): # this is your 'straight line' y=f(x)
    return a*logxfittingregion1 + b

popt2, pcov2 = curve_fit(straightline, logxfittingregion1, lnIyfittingregion1)
xdata2 = np.linspace(np.min(logx),turning_point_V,100)
ydata2 = straightline(xdata2,popt2[0],popt2[1])

Te = 1/popt2[0] ### electron temperature in eV
Te_K = (1/popt2[0])*(const.e/const.Boltzmann) #### electron temperature in Kelvin

plt.figure(3)
plt.plot (logx,lnI,'kx', label = 'lnI')
plt.plot (xdata2,ydata2, label = 'Fit')   
plt.xlabel(r'$Voltage (V)$')
plt.ylabel("$lnI_e$")
plt.legend()
plt.show()
    

#### calculaing denisty ###

R_p = 0.08*10**-3   # radius of the probe
L = 5.5*10**-3      # length of the probe
A_p = 2*np.pi*R_p*L    # area of the probe


I_esat_raw_data = np.interp(turning_point_V,Voltage,smooth) ## electron saturation from raw data
I_esat_fit = np.interp(turning_point_V,np.reshape(xdata2,100),np.reshape(ydata2,100)) ### finding electron sat from fit. note the refit to make the arrays 1D
I_esat_fit = np.e**I_esat_fit ### exponential to get back to current 

n_e = (4*I_esat_fit/(const.e*A_p))*(np.sqrt(np.pi*const.m_e/(8*const.Boltzmann*Te_K)))

### test of bimax thing ### petes double bimax fit 1st population

'''
def bimax(logxfittingregion1,Ic):
    return(np.log(Ic) + ((const.e*(logxfittingregion1-turning_point_V))/(const.Boltzmann*Te_K)))
    
popt3, pcov3 = curve_fit(bimax, logxfittingregion1, lnIyfittingregion1)
xdata3 = np.linspace(np.min(logx),turning_point_V,100)
ydata3 = bimax(xdata3,popt3[0])

    
'''


print('plasma potential from first derviv is' , turning_point_V)
print('Floating potential is' , FloatV)
print('electron temperature is' , Te)
print('electron density is' , n_e)
