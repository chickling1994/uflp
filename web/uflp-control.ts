class UflpDriver {

    private driver: Driver;
    private id: number;
    private cmds: Commands;

    constructor(public client: Client) {
        this.driver = this.client.getDriver('UFLP');
        this.id = this.driver.id;
        this.cmds = this.driver.getCmds();
    }

    set_Reset(): void {
        this.client.send(Command(this.id, this.cmds['set_Reset']));
    }

    set_Trigger(): void {
        this.client.send(Command(this.id, this.cmds['set_Trigger']));
    }

    setAcqTime(val: number): void {
        this.client.send(Command(this.id, this.cmds['setAcqTime'], val));
    }
}

class UflpControl {
    private ResetSpan: HTMLSpanElement;
    private UFLPControlInputs: HTMLInputElement[];
    private canvas: HTMLCanvasElement;

    constructor(private document: Document, private driver: UflpDriver) {
        this.ResetSpan = <HTMLSpanElement>document.getElementById('Reset');
        this.initReset();

        this.UFLPControlInputs = <HTMLInputElement[]><any>document.getElementsByClassName("uflp-control-input");
        this.initUFLPControlInputs();

        this.canvas = <HTMLCanvasElement>document.getElementById('canvas');
        this.canvas.width = (<HTMLInputElement>document.querySelector(".uflp-control-input[type='range']")).offsetWidth;
    }

    initReset(): void {
        let ResetBtn = <HTMLButtonElement>document.getElementById("reset-btn");
        ResetBtn.addEventListener('click', (event) => {
            this.driver.set_Reset();
        })
    }

    initTrigger(): void {
        let TriggerBtn = <HTMLButtonElement>document.getElementById("trigger-btn");
        TriggerBtn.addEventListener('click', (event) => {
            this.driver.set_Trigger();
        })
    }

    initUFLPControlInputs(): void {
        let events = ['change', 'input'];
        for (let j = 0; j < events.length; j++) {
            for (let i = 0; i < this.UFLPControlInputs.length; i++) {
                this.UFLPControlInputs[i].addEventListener(events[j], (event) => {
                    let counterType: string = "number";
                    if ((<HTMLInputElement>event.currentTarget).type == "number") {
                        counterType = "range";
                    }
                    let command = (<HTMLInputElement>event.currentTarget).dataset.command;
                    let value = (<HTMLInputElement>event.currentTarget).value;
                    (<HTMLInputElement>document.querySelector(".uflp-control-input[data-command='" + command + "'][type='" + counterType + "']")).value = value ;
                    this.driver[command](parseFloat(value));
                })
            }
        }

    }
}