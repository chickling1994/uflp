----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : September 2019
-- 
-- Module Name: current_responce
-- Project Name: PCR
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Calculates the current from predefined values of:
--              Temp, vFloat, iSat, LP_bias_rx
-- 
-- Dependencies: constants, current_LUT, types
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library packages;
    use packages.types.all;
    use packages.LT_IV_LUT.all;
    use packages.constants.all;

entity gen_cal is
  port (
    -- Generic to all cores
    clk : in std_logic;

    adc_1 : in std_logic_vector(13 downto 0);
    adc_2 : in std_logic_vector(13 downto 0);

    trigger : in std_logic;
    acquisition_length : in std_logic_vector(31 downto 0);

    -- Fifo is 32-bit
    data_out : out std_logic_vector(31 downto 0);
    tvalid : out std_logic;
    current_out : out std_logic_vector(13 downto 0);
    voltage_out : out std_logic_vector(13 downto 0)
  );
end gen_cal; 

architecture rtl of gen_cal is
    -- LUT saved to rom to speed up process
    constant rom : LUT_type := LT_IV_LUT;

    signal acquire_complete : std_logic := '1';
    signal acquisition_counter : unsigned(31 downto 0) := (others => '0');

    type cal_state is (reset,cal_1, cal_2, cal_3, cal_4, cal_5, cal_6, cal_7, cal_8, cal_9);
    signal State : cal_state;
    signal Counter : unsigned(11 downto 0) := to_unsigned(0, 12);
    signal counter_loop : unsigned(13 downto 0) := to_unsigned(0, 14);
    signal max_counter : unsigned(11 downto 0) := to_unsigned(100, 12);
begin
    -- purpose: Return LUT value at given address and the bit shift associated
    --          with that value. Bit shift input manually. LUT generated in python.
    -- type   : synchronus process
    -- inputs : clk, rom, iSat_addr, DAC_bits
    -- outputs: iSat_LUT_value, iSat_LUT_shift
    PROC_LUT : process(clk)
    begin
        if rising_edge(clk) then
            current_out <= std_logic_vector(rom(to_integer(signed(adc_1)+2**13 - 3328)));
            voltage_out <= std_logic_vector(signed(adc_1) - 3328 );
        end if;
    end process; -- PROC_LUT
    
    -- -- purpose: Calculation the current value
    -- -- type   : synchronus
    -- -- inputs : clk, current_LUT_shift, sat_current, current_LUT_value
    -- -- outputs: current_out
    -- PROC_CURRENT : process(clk)

    -- begin
    --     if rising_edge(clk) then
    --         tvalid <= '0';
    --         if trigger = '1' and acquire_complete = '1' then
    --             acquire_complete <= '0';
    --         end if;
    --         if acquire_complete = '0' then
    --             Counter <= Counter + 1;
    --             if Counter = max_counter then
    --                 counter_loop <= counter_loop + 1;
    --                 Counter <= to_unsigned(0, Counter'length);
    --                 acquisition_counter <= acquisition_counter + 1;
    --                 tvalid <= '1';
    --                 if acquisition_counter = unsigned(acquisition_length) then
    --                     acquire_complete <= '1';
    --                     acquisition_counter <= (others => '0');
    --                 end if;
    --             end if;
    --         end if;
    --         voltage_out <= std_logic_vector(rom(to_integer(counter_loop)));
    --         -- case State is
    --         --     when reset => 
    --         --         State <= cal_1;
    --         --         Counter <= to_unsigned(0, Counter'length);
    --         --     when cal_1 =>
    --         --         voltage_out <= std_logic_vector(to_signed(-8192,voltage_out'length));
    --         --         if Counter = max_counter then
    --         --             State <= cal_2;
    --         --             Counter <= to_unsigned(0, Counter'length);
    --         --             tvalid <= '1';
    --         --         end if;
    --         --     when cal_2 => 
    --         --         voltage_out <= std_logic_vector(to_signed(-6144,voltage_out'length));
    --         --         if Counter = max_counter then
    --         --             State <= cal_3;
    --         --             Counter <= to_unsigned(0, Counter'length);
    --         --             tvalid <= '1';
    --         --         end if;
    --         --     when cal_3 =>
    --         --         voltage_out <= std_logic_vector(to_signed(-4096,voltage_out'length));
    --         --         if Counter = max_counter then
    --         --             State <= cal_4;
    --         --             Counter <= to_unsigned(0, Counter'length);
    --         --             tvalid <= '1';
    --         --         end if;
    --         --     when cal_4 =>
    --         --         voltage_out <= std_logic_vector(to_signed(-2048,voltage_out'length));
    --         --         if Counter = max_counter then
    --         --             State <= cal_5;
    --         --             Counter <= to_unsigned(0, Counter'length);
    --         --             tvalid <= '1';
    --         --         end if;
    --         --     when cal_5 => 
    --         --         voltage_out <= std_logic_vector(to_signed(0,voltage_out'length));
    --         --         if Counter = max_counter then
    --         --             State <= cal_6;
    --         --             Counter <= to_unsigned(0, Counter'length);
    --         --             tvalid <= '1';
    --         --         end if;
    --         --     when cal_6 =>
    --         --         voltage_out <= std_logic_vector(to_signed(2048,voltage_out'length));
    --         --         if Counter = max_counter then
    --         --             State <= cal_7;
    --         --             Counter <= to_unsigned(0, Counter'length);
    --         --             tvalid <= '1';
    --         --         end if;
    --         --     when cal_7 =>
    --         --         voltage_out <= std_logic_vector(to_signed(4096,voltage_out'length));
    --         --         if Counter = max_counter then
    --         --             State <= cal_8;
    --         --             Counter <= to_unsigned(0, Counter'length);
    --         --             tvalid <= '1';
    --         --         end if;
    --         --     when cal_8 => 
    --         --         voltage_out <= std_logic_vector(to_signed(6144,voltage_out'length));
    --         --         if Counter = max_counter then
    --         --             State <= cal_9;
    --         --             Counter <= to_unsigned(0, Counter'length);
    --         --             tvalid <= '1';
    --         --         end if;
    --         --     when cal_9 =>
    --         --         voltage_out <= std_logic_vector(to_signed(8191,voltage_out'length));
    --         --         if Counter = max_counter then
    --         --             State <= reset;
    --         --             Counter <= to_unsigned(0, Counter'length);
    --         --             tvalid <= '1';
    --         --         end if;
    --         -- end case;
    --     end if;
    -- end process; -- PROC_CURRENT

    -- purpose: Concatenate 3 variables with a 1 padding at the start
    --          ensures bits are not lost returning from fifo
    -- type   : synchronus with reset
    -- inputs : clk, rst, sat_current, temp, V_Float
    -- outputs: data_out
    PROC_DATA_OUT : process(clk)
    begin
        if rising_edge(clk) then
            -- data_out <= '1' &
            --             sat_current(sat_current'length - 1 downto 3) &
            --             temp(temp'length - 1 downto 3) &
            --             V_Float(V_Float'length - 1 downto 5);

            data_out(data_out'high) <= '1';
            data_out(30 downto 28) <= (others => '0');
            data_out(27 downto 14) <= adc_1;
            data_out(13 downto 0) <= adc_2;
        end if ;
    end process ; -- PROC_DATA_OUT

end architecture;