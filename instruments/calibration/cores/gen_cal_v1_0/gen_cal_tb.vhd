----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : April 2019
-- 
-- Module Name: FET_Drive_tb
-- Project Name: uflp
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Drives the bias states for all cores
-- 
-- Dependencies: constants, sim_constants, sim_packages, uflp
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use std.env.finish;
use std.textio.all;

library xil_defaultlib;

library sim_packages;
use sim_packages.sim_subprograms.all;
use sim_packages.sim_constants.all;

entity gen_cal_tb is
end gen_cal_tb; 

architecture sim of gen_cal_tb is   
    component gen_cal
    port(
        signal clk : in std_logic;

        signal adc_1 : in std_logic_vector(13 downto 0);
        signal adc_2 : in std_logic_vector(13 downto 0);

        signal trigger : in std_logic;
        signal acquisition_length : in std_logic_vector(31 downto 0);
    
        -- Fifo is 32-bit
        signal data_out : out std_logic_vector(31 downto 0);
        signal tvalid : out std_logic;
        signal voltage_out : out std_logic_vector(13 downto 0)
    );
    end component;

    -- Initialise Values
    signal clk : std_logic := '1';
    signal adc_1 : std_logic_vector(13 downto 0);
    signal adc_2 : std_logic_vector(13 downto 0);

    signal trigger : std_logic := '1';
    signal acquisition_length : std_logic_vector(31 downto 0);

    -- Fifo is 32-bit
    signal data_out : std_logic_vector(31 downto 0);
    signal tvalid : std_logic;
    signal voltage_out : std_logic_vector(13 downto 0);

    signal counter : signed(13 downto 0) := to_signed(-8192,14);
begin
    -- Generate clock as defined by sim_subprograms and sim_constants
    gen_clock(clk);

    DUT : entity xil_defaultlib.gen_cal(rtl)
    port map (
        -- Generic to all cores
        clk => clk,

        adc_1 => adc_1,
        adc_2 => adc_2,

        trigger => trigger,
        acquisition_length => acquisition_length,

        -- Fifo is 32-bit
        data_out => data_out,
        tvalid => tvalid,
        voltage_out => voltage_out
    );

    PROC_trigger : process
    begin
        acquisition_length <= std_logic_vector(to_unsigned(17000,acquisition_length'length));
        wait for 100*clock_period;
        trigger <= '0';
    end process; -- PROC_trigger


    PROC_ADC : process
    begin
        adc_1 <= std_logic_vector(counter);
        counter <= counter + 1;
        wait for clock_period;
    end process; -- PROC_trigger
end architecture;