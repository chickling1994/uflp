#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import os
import time
import matplotlib.pyplot as plt

from calibration import Calibration
from koheron import connect

# host = os.getenv('HOST', '10.210.2.152')
host = os.getenv('HOST', '192.168.1.100')
client = connect(host, name='Calibration')
driver = Calibration(client)

acquisition_length = 0.2 #seconds
driver.set_acquisition_length(acquisition_length)
buffer_length = []
i = 0
if driver.get_Reset() == 0:
    driver.set_Reset()
    time.sleep(1)
    driver.set_Reset()
else:
    driver.set_Reset()

# time.sleep(0.01)
driver.set_trigger()

while True:
    try:
        time.sleep(0.2)
        samples = driver.get_buffer_length()
        if samples == 0:
            break
        print(samples)
    except KeyboardInterrupt:
        break

# driver.set_Switch(0)


# driver.set_Temperature(5500)
# driver.set_ISat(850)
# driver.set_Vfloating(-2700)

# driver.set_Resistence(100)

dataArray_pcr = driver.get_PCR_data()
print(len(dataArray_pcr))

saveStr = "PCR_test_data"
print(saveStr)
np.save(saveStr, dataArray_pcr)

t = np.arange(0,len(dataArray_pcr))
t = 1005 * 3 * 8E-9 * t

y = np.zeros([len(dataArray_pcr), 32])
sample_length = 1000000
for i in range(0,len(dataArray_pcr[0:sample_length])): 
    y[i][0:32] = [int(d) for d in str(bin(dataArray_pcr[i]))[2:]]  

Data_Array_Max = np.zeros(len(dataArray_pcr[0:sample_length]))
Data_Array_Min = np.zeros(len(dataArray_pcr[0:sample_length]))
Current_array = np.zeros(len(dataArray_pcr[0:sample_length]))
for i in range(0,len(Current_array)): 
    for j in range(4,18):
        if j == 4:
            if int(y[i][j]) == 1:
                Current_array[i] = -(2**13)
        elif int(y[i][j]) == 1: 
            Current_array[i] = Current_array[i] + 2**(13-j+4)
    Current_array[i] = Current_array[i] / 8191
    Data_Array_Max[i] = 1
    Data_Array_Min[i] = -1

Bias_array = np.zeros(len(dataArray_pcr[0:sample_length])) 
for i in range(0,len(Bias_array)): 
    for j in range(18,32):
        if j == 18:
            if int(y[i][j]) == 1:
                Bias_array[i] = -(2**13)
        elif int(y[i][j]) == 1: 
            Bias_array[i] = Bias_array[i] + 2**(13-j+18)
    Bias_array[i] = Bias_array[i] / 8191

plt.figure("Ch1")
plt.plot(t[:sample_length], Bias_array,'o')
plt.plot(t[:sample_length], Data_Array_Max,'ro')
plt.plot(t[:sample_length], Data_Array_Min,'ro')
plt.xlabel("Time, s")
plt.ylabel("Bias, V")

plt.figure("Ch2")
plt.plot(t[:sample_length], Current_array,'o')
plt.plot(t[:sample_length], Data_Array_Max,'ro')
plt.plot(t[:sample_length], Data_Array_Min,'ro')
plt.xlabel("Time, s")
plt.ylabel("Current, A")

plt.show()