#!/bin/bash -f
# ****************************************************************************
# Vivado (TM) v2019.1 (64-bit)
#
# Filename    : simulate.sh
# Simulator   : Xilinx Vivado Simulator
# Description : Script for simulating the design by launching the simulator
#
# Generated by Vivado on Tue Nov 02 17:29:16 GMT 2021
# SW Build 2552052 on Fri May 24 14:47:09 MDT 2019
#
# Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
#
# usage: simulate.sh
#
# ****************************************************************************
set -Eeuo pipefail
echo "xsim gen_cal_tb_behav -key {Behavioral:sim_1:Functional:gen_cal_tb} -tclbatch gen_cal_tb.tcl -view /home/chicklin/workspace/home-working/koheron-sdk/instruments/calibration/calibration/gen_cal_tb_behav.wcfg -log simulate.log"
xsim gen_cal_tb_behav -key {Behavioral:sim_1:Functional:gen_cal_tb} -tclbatch gen_cal_tb.tcl -view /home/chicklin/workspace/home-working/koheron-sdk/instruments/calibration/calibration/gen_cal_tb_behav.wcfg -log simulate.log

