#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import os
import time
import matplotlib.pyplot as plt

from pcr import PCS
from koheron import connect

# host = os.getenv('HOST', '10.210.2.152')
host = os.getenv('HOST', '192.168.1.100')
client = connect(host, name='Calibration')
driver = PCS(client)

while True:
    try:
        time.sleep(0.2)
        samples = driver.get_buffer_length()
        if samples == 0:
            break
        print(samples)
    except KeyboardInterrupt:
        break

# driver.set_Switch(0)


# driver.set_Temperature(5500)
# driver.set_ISat(850)
# driver.set_Vfloating(-2700)

# driver.set_Resistence(100)

dataArray_pcr = driver.get_PCR_data()
print(len(dataArray_pcr))

saveStr = "PCR_test_data"
print(saveStr)
np.save(saveStr, dataArray_pcr)

t = np.arange(0,len(dataArray_pcr))
t = 1005 * 3 * 8E-9 * t

y = np.zeros([len(dataArray_pcr), 32])
sample_length = 1000000
for i in range(0,len(dataArray_pcr[0:sample_length])): 
    y[i][0:32] = [int(d) for d in str(bin(dataArray_pcr[i]))[2:]]  
         
iSat_array = np.zeros(len(dataArray_pcr[0:sample_length])) 
for i in range(0,len(iSat_array)): 
    for j in range(1,12):
        if j == 1:
            if int(y[i][j]) == 1:
                iSat_array[i] = -(2**13)
        elif int(y[i][j]) == 1: 
            iSat_array[i] = iSat_array[i] + 2**(13-j+1)
    iSat_array[i] = iSat_array[i] / 2**11

Temp_array = np.zeros(len(dataArray_pcr[0:sample_length])) 
for i in range(0,len(Temp_array)): 
    for j in range(12,23): 
        if j == 12:
            if int(y[i][j]) == 1:
                Temp_array[i] = -(2**13)
        elif int(y[i][j]) == 1: 
            Temp_array[i] = Temp_array[i] + 2**(13-j+12) 
    Temp_array[i] = Temp_array[i] / 2**7

vFloat_array = np.zeros(len(dataArray_pcr[0:sample_length])) 
for i in range(0,len(vFloat_array)): 
    for j in range(23,32): 
        if j == 23:
            if int(y[i][j]) == 1:
                vFloat_array[i] = -(2**13)
        elif int(y[i][j]) == 1: 
            vFloat_array[i] = vFloat_array[i] + 2**(13-j+23)
    vFloat_array[i] = vFloat_array[i] / 2**5

Current_array = np.zeros(len(dataArray_pcr[0:sample_length])) 
for i in range(0,len(Current_array)): 
    for j in range(4,18):
        if j == 4:
            if int(y[i][j]) == 1:
                Current_array[i] = -(2**13)
        elif int(y[i][j]) == 1: 
            Current_array[i] = Current_array[i] + 2**(13-j+3)
    Current_array[i] = Current_array[i] / 2**11

Bias_array = np.zeros(len(dataArray_pcr[0:sample_length])) 
for i in range(0,len(Bias_array)): 
    for j in range(18,32):
        if j == 18:
            if int(y[i][j]) == 1:
                Bias_array[i] = -(2**13)
        elif int(y[i][j]) == 1: 
            Bias_array[i] = Bias_array[i] + 2**(13-j+18)
    Bias_array[i] = Bias_array[i] / 2**5

plt.figure("Temperature")
plt.plot(t[:sample_length], Temp_array,'o')
plt.xlabel("Time, s")
plt.ylabel("Temperature, eV")

plt.figure("iSat")
plt.plot(t[:sample_length], iSat_array,'o')
plt.xlabel("Time, s")
plt.ylabel("Ion Saturation Current, A")

plt.figure("vFloat")
plt.plot(t[:sample_length], vFloat_array,'o')
plt.xlabel("Time, s")
plt.ylabel("Floating Potential, V")

plt.figure("Ch1")
plt.plot(t[:sample_length], Bias_array,'o')
plt.xlabel("Time, s")
plt.ylabel("Bias, V")

plt.figure("Ch2")
plt.plot(t[:sample_length], Current_array,'o')
plt.xlabel("Time, s")
plt.ylabel("Current, A")

plt.show()