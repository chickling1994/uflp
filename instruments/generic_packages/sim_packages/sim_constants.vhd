library packages;
use packages.constants.all;


package sim_constants is

  constant clock_period : time := 1 sec / clock_frequency;

end package;