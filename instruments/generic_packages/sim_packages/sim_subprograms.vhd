library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use ieee.numeric_std.all;

use std.textio.all;

library packages;
use packages.types.all;

library sim_packages;
use sim_packages.sim_constants.all;


package sim_subprograms is
  
  -- Generates the clock signal
  procedure gen_clock(signal clk : inout std_logic);

  -- Print message with test OK
  procedure print_test_ok(signal test_string : string(1 to 4));

  -- Generates a random number
  procedure gen_random(signal rand_num : out std_logic_vector(9 downto 0));

  procedure print_test_ok_gen;

  constant  MATH_E :   real := 2.71828_18284_59045_23536;  
  -- value of e

  procedure EXP(signal X : inout real);
  -- returns e**X; where e = MATH_E

end package;

package body sim_subprograms is

  procedure EXP(signal X : inout real) is
  begin
    X <= MATH_E ** X ;
  end procedure;

  procedure gen_clock(signal clk : inout std_logic) is
  begin
    clk <= not clk after clock_period / 2;
  end procedure;

  procedure print_test_ok(signal test_string : string(1 to 4)) is 
    variable str : line;
  begin
    write(str, string'(test_string & ": Completed Successfully"));
    writeline(output, str);
  end procedure;

  procedure print_test_ok_gen is 
    variable str : line;
  begin
    write(str, string'("Test: OK"));
    writeline(output, str);
  end procedure;

  procedure gen_random(signal rand_num : out std_logic_vector(9 downto 0)) is
    variable seed1, seed2: positive;               -- seed values for random generator
    variable rand: real;   -- random real-number value in range 0 to 1.0  
    variable range_of_rand : real := 1.0;    -- the range of random values created will be 0 to +1000.
  begin
    uniform(seed1, seed2, rand);   -- generate random number
    rand_num <= std_logic_vector(to_unsigned(natural(rand*range_of_rand),10));  -- rescale to 0..1000, convert integer part 
  end procedure;



end package body;