library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;
    use ieee.math_real.all;

library packages;
    use packages.constants.all;
    use packages.types.all;
    use packages.current_LUT.all;
    
library sim_packages;
    use sim_packages.sim_constants.all;
    use sim_packages.sim_subprograms.all;

entity current_solver_vc is
    port (
        sat_current_input :  in integer := 409;
        Temperature_input : in integer := 3639;
        V_float_input :  in integer := -2392;
        enable : in boolean;
        bias_input : in std_logic_vector;
        
        solved_current  : out std_logic_vector;
        solve_failed : out boolean
    );
end current_solver_vc ; 

architecture sim of current_solver_vc is
    constant rom : LUT_type := current_LUT;

    signal current_LUT_shift : std_logic_vector(3 downto 0);
    signal current_addr : std_logic_vector(DAC_bits - 1 downto 0);
    signal current_LUT_value : signed(DAC_bits - 1 downto 0);

    signal current_voltage_pipeline : integer;
    signal random_integer : integer;
begin

    SIM_CURRENT : process
        variable LP_bias_temp : integer;
        variable sub_calc : integer;
        variable LUT_seg : integer;
        variable seed1, seed2 : integer := 999;



        impure function rand_int(min_val, max_val : integer) return integer is
            variable r : real;
          begin
            uniform(seed1, seed2, r);
            return integer(
              round(r * real(max_val - min_val + 1) + real(min_val) - 0.5));
          end function;

    begin
        wait until enable;

        random_integer <= rand_int(-100,100);

        -- LP_bias_temp := to_integer(signed(bias_input))*90/120;
        LP_bias_temp := to_integer(signed(bias_input));
        -- Finds the correct address in the LUT based on the current Bias, Temperature and V_f
        sub_calc := ((2**12*(LP_bias_temp - integer(V_float_input))) / (integer(Temperature_input))) + 2**13;
        current_addr <= std_logic_vector(to_unsigned(sub_calc, current_addr'length));
        LUT_seg := to_integer((unsigned(Current_addr)))/2**10;

        case LUT_seg is
        when 9 to 16  =>
            -- Bits either side of the binary point are 2_12
            current_LUT_shift <= std_logic_vector(to_unsigned(2,current_LUT_shift'length));
        when 0 to 7 =>
            -- Bits either side of the binary point are 1_13
            current_LUT_shift <=  std_logic_vector(to_unsigned(13,current_LUT_shift'length));
        when others =>
            -- Bits either side of the binary point are 10_4
            current_LUT_shift <= std_logic_vector(to_unsigned(12,current_LUT_shift'length));
        end case;

        current_LUT_value <= rom(to_integer(unsigned(current_addr)));
        current_voltage_pipeline <= integer(sat_current_input) * (to_integer(current_LUT_value)) / 2**to_integer(unsigned(current_LUT_shift));
        -- current_voltage_pipeline <= integer(sat_current_input) * (to_integer(current_LUT_value)) / 2**to_integer(unsigned(current_LUT_shift));
        -- solved_current <= std_logic_vector(to_signed(current_voltage_pipeline*11, solved_current'length));
        -- solved_current <= std_logic_vector(to_signed(current_voltage_pipeline + random_integer, solved_current'length));
        solved_current <= std_logic_vector(to_signed(current_voltage_pipeline, solved_current'length));
    end process; -- PROC_SIM_CURRENT

end architecture ;