library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library packages;
  use packages.constants.all;

package types is

  -- Defines size of each look up table value
  subtype LUT_val is signed(DAC_bits - 1 downto 0);

  -- Defines length of look up table
  subtype LUT_len is integer range 0 to 2**DAC_bits - 1;
  
  -- Generates LUT array
  type LUT_type is array (LUT_len) of LUT_val;

end package;