----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : April 2019
-- 
-- Module Name: reset
-- Project Name: uflp
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Standard reset core that is agnostic to all inputs
-- 
-- Dependencies: constants
----------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity reset is
    port (
        clk : in std_logic;
        rst_in : in std_logic;
        trigger : in std_logic;

        acquisition_length : in std_logic_vector(31 downto 0);
        
        rst_out : out std_logic;
        rst_hang : out std_logic
    );
end reset; 

architecture rtl of reset is
    signal counter : unsigned(7 downto 0) := (others => '0');
    signal hang_counter : unsigned(13 downto 0) := (others => '0');
begin
    -- rst_out is 0 for 2**8 clock cycles
    rst_out <= not counter(counter'high);
    rst_hang <= not hang_counter(hang_counter'high);

    PROC_RESET : process(clk)
    begin
        if rising_edge(clk) then
            -- if button is pressed counter starts from 0
            if rst_in = '1' then -- 1 because no pullup used
                counter <= (others => '0');
                hang_counter <= (others => '0');
            else
                -- When counter maxes out reset becomes inactive
                if counter(counter'high) = '0' then
                    counter <= counter + 1;
                end if;
                if hang_counter(hang_counter'high) = '0' then
                    hang_counter <= hang_counter + 1;
                end if;

                -- if trigger = '1' then
                    
                -- end if;
            end if;
        end if;
    end process; -- PROC_RESET
end architecture;