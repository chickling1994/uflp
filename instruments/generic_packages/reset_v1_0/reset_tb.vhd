----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : April 2019
-- 
-- Module Name: reset_tb
-- Project Name: uflp
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Standard reset core that is agnostic to all inputs
-- 
-- Dependencies: constants, sim_constants, sim_packages, uflp
----------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use std.env.finish;
 
library packages;

library sim_packages;
    use sim_packages.sim_constants.all;
    use sim_packages.sim_subprograms.all;
 
entity reset_tb is
end reset_tb; 
 
architecture sim of reset_tb is
    -- DUT signals
    signal clk : std_logic := '1';
    signal rst_in : std_logic := '1'; -- Pullup
    signal rst_out : std_logic;
    
    -- Min and max duration of the reset strobe
    constant min_duration : time := 20*clock_period;
    constant max_duration : time := 100*us;

    -- Time to wait before assuming that the DUT is not resetting
    constant quiet_duration : time := max_duration * 5;

    signal rst_msg : string(1 to 4) := "Rst ";

begin
    -- Generate clock as defined by sim_subprograms and sim_constants
    gen_clock(clk);
 
    DUT : entity packages.reset(rtl)
    port map (
        clk => clk,
        rst_in => rst_in,
        rst_out => rst_out
    );
    
    -- Check that the duration of the reset strobe
    PROC_SEQUENCER : process
    procedure check_duration is
    begin
        assert rst_out = '1'
        report "rst_out should be '1' before calling this check_duration"
        severity failure;

        wait on rst_out for min_duration;
        
        assert rst_out'stable(min_duration) -- Check that a signal remains stable for given time
        report "Strobe on rst_out was shorter than min_duration: "
            & time'image(min_duration)
        severity failure;

        wait for max_duration - min_duration;
        assert rst_out = '0'
        report "rst_out didn't change from '1' to '0' within "
            & time'image(max_duration)
        severity failure;
    end procedure;

    -- Perform one test of the reset cycle
    procedure button_test is
    begin
        assert rst_out = '0'
        report "rst_out should be '0' when button_test is called"
        severity failure;

        wait for quiet_duration;
        assert rst_out = '0' and rst_out'stable(quiet_duration)
        report "rst_out asserted without any activity on rst_in"
        severity failure;

        report "Triggering a reset";
        wait until rising_edge(clk);
        rst_in <= '0';
        wait until rising_edge(clk);
        rst_in <= '1';
        wait until rising_edge(clk);
        
        assert rst_out = '1'
        report "rst_out was not asserted after a pulse on rst_in"
        severity failure;
        
        check_duration;
    end procedure;

    begin
        wait for 0 ns;

        assert rst_out = '1'
        report "rst_out was not asserted on power-on"
        severity failure;
        
        -- Check the duration of the power-on reset strobe
        check_duration;

        -- Test two reset cycles triggered by button input
        button_test;
        button_test;

        print_test_ok(rst_msg);
        finish;
    end process; -- PROC_SEQUENCER
end architecture;