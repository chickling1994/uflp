

create_debug_core u_ila_0 ila
set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_0]
set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_0]
set_property C_ADV_TRIGGER false [get_debug_cores u_ila_0]
set_property C_DATA_DEPTH 2048 [get_debug_cores u_ila_0]
set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_0]
set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_0]
set_property C_TRIGIN_EN false [get_debug_cores u_ila_0]
set_property C_TRIGOUT_EN false [get_debug_cores u_ila_0]
set_property port_width 1 [get_debug_ports u_ila_0/clk]
connect_debug_port u_ila_0/clk [get_nets [list system_i/adc_dac/pll/inst/clk_out1]]
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe0]
set_property port_width 14 [get_debug_ports u_ila_0/probe0]
connect_debug_port u_ila_0/probe0 [get_nets [list {system_i/adc0_1[0]} {system_i/adc0_1[1]} {system_i/adc0_1[2]} {system_i/adc0_1[3]} {system_i/adc0_1[4]} {system_i/adc0_1[5]} {system_i/adc0_1[6]} {system_i/adc0_1[7]} {system_i/adc0_1[8]} {system_i/adc0_1[9]} {system_i/adc0_1[10]} {system_i/adc0_1[11]} {system_i/adc0_1[12]} {system_i/adc0_1[13]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe1]
set_property port_width 14 [get_debug_ports u_ila_0/probe1]
connect_debug_port u_ila_0/probe1 [get_nets [list {system_i/adc1_1[0]} {system_i/adc1_1[1]} {system_i/adc1_1[2]} {system_i/adc1_1[3]} {system_i/adc1_1[4]} {system_i/adc1_1[5]} {system_i/adc1_1[6]} {system_i/adc1_1[7]} {system_i/adc1_1[8]} {system_i/adc1_1[9]} {system_i/adc1_1[10]} {system_i/adc1_1[11]} {system_i/adc1_1[12]} {system_i/adc1_1[13]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe2]
set_property port_width 14 [get_debug_ports u_ila_0/probe2]
connect_debug_port u_ila_0/probe2 [get_nets [list {system_i/top_mlp_Temperature_out[0]} {system_i/top_mlp_Temperature_out[1]} {system_i/top_mlp_Temperature_out[2]} {system_i/top_mlp_Temperature_out[3]} {system_i/top_mlp_Temperature_out[4]} {system_i/top_mlp_Temperature_out[5]} {system_i/top_mlp_Temperature_out[6]} {system_i/top_mlp_Temperature_out[7]} {system_i/top_mlp_Temperature_out[8]} {system_i/top_mlp_Temperature_out[9]} {system_i/top_mlp_Temperature_out[10]} {system_i/top_mlp_Temperature_out[11]} {system_i/top_mlp_Temperature_out[12]} {system_i/top_mlp_Temperature_out[13]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe3]
set_property port_width 14 [get_debug_ports u_ila_0/probe3]
connect_debug_port u_ila_0/probe3 [get_nets [list {system_i/top_mlp_V_float_out[0]} {system_i/top_mlp_V_float_out[1]} {system_i/top_mlp_V_float_out[2]} {system_i/top_mlp_V_float_out[3]} {system_i/top_mlp_V_float_out[4]} {system_i/top_mlp_V_float_out[5]} {system_i/top_mlp_V_float_out[6]} {system_i/top_mlp_V_float_out[7]} {system_i/top_mlp_V_float_out[8]} {system_i/top_mlp_V_float_out[9]} {system_i/top_mlp_V_float_out[10]} {system_i/top_mlp_V_float_out[11]} {system_i/top_mlp_V_float_out[12]} {system_i/top_mlp_V_float_out[13]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe4]
set_property port_width 14 [get_debug_ports u_ila_0/probe4]
connect_debug_port u_ila_0/probe4 [get_nets [list {system_i/top_mlp_bias_out_dac[0]} {system_i/top_mlp_bias_out_dac[1]} {system_i/top_mlp_bias_out_dac[2]} {system_i/top_mlp_bias_out_dac[3]} {system_i/top_mlp_bias_out_dac[4]} {system_i/top_mlp_bias_out_dac[5]} {system_i/top_mlp_bias_out_dac[6]} {system_i/top_mlp_bias_out_dac[7]} {system_i/top_mlp_bias_out_dac[8]} {system_i/top_mlp_bias_out_dac[9]} {system_i/top_mlp_bias_out_dac[10]} {system_i/top_mlp_bias_out_dac[11]} {system_i/top_mlp_bias_out_dac[12]} {system_i/top_mlp_bias_out_dac[13]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe5]
set_property port_width 14 [get_debug_ports u_ila_0/probe5]
connect_debug_port u_ila_0/probe5 [get_nets [list {system_i/top_mlp_output_dac[0]} {system_i/top_mlp_output_dac[1]} {system_i/top_mlp_output_dac[2]} {system_i/top_mlp_output_dac[3]} {system_i/top_mlp_output_dac[4]} {system_i/top_mlp_output_dac[5]} {system_i/top_mlp_output_dac[6]} {system_i/top_mlp_output_dac[7]} {system_i/top_mlp_output_dac[8]} {system_i/top_mlp_output_dac[9]} {system_i/top_mlp_output_dac[10]} {system_i/top_mlp_output_dac[11]} {system_i/top_mlp_output_dac[12]} {system_i/top_mlp_output_dac[13]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe6]
set_property port_width 14 [get_debug_ports u_ila_0/probe6]
connect_debug_port u_ila_0/probe6 [get_nets [list {system_i/top_mlp_sat_current_out[0]} {system_i/top_mlp_sat_current_out[1]} {system_i/top_mlp_sat_current_out[2]} {system_i/top_mlp_sat_current_out[3]} {system_i/top_mlp_sat_current_out[4]} {system_i/top_mlp_sat_current_out[5]} {system_i/top_mlp_sat_current_out[6]} {system_i/top_mlp_sat_current_out[7]} {system_i/top_mlp_sat_current_out[8]} {system_i/top_mlp_sat_current_out[9]} {system_i/top_mlp_sat_current_out[10]} {system_i/top_mlp_sat_current_out[11]} {system_i/top_mlp_sat_current_out[12]} {system_i/top_mlp_sat_current_out[13]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe7]
set_property port_width 1 [get_debug_ports u_ila_0/probe7]
connect_debug_port u_ila_0/probe7 [get_nets [list system_i/top_mlp/U0/neg_bias]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe8]
set_property port_width 1 [get_debug_ports u_ila_0/probe8]
connect_debug_port u_ila_0/probe8 [get_nets [list system_i/top_mlp/U0/pos_bias]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe9]
set_property port_width 1 [get_debug_ports u_ila_0/probe9]
connect_debug_port u_ila_0/probe9 [get_nets [list system_i/top_mlp_change_bias_out]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe10]
set_property port_width 1 [get_debug_ports u_ila_0/probe10]
connect_debug_port u_ila_0/probe10 [get_nets [list system_i/top_mlp/U0/zero_bias]]
set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets u_ila_0_clk_out1]
