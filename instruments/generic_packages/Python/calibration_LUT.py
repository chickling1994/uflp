# Python script to generate the saturation current look up table

import numpy as np
import matplotlib.pyplot as plt

value = []

LUT_len = 14
Bit_len = 14


min_val = -2.0*np.pi
max_val = 2.0*np.pi

index = np.linspace(min_val, max_val, num=2**LUT_len)



def lut_func(x):
    exp = np.sin(x)
    eqn = exp*(2**(Bit_len-1)-1)

    return int(round(eqn))

for i in range(len(index)):
    value.append(round(lut_func(index[i])))
print(len(value))

with open("calibration_LUT_pck.vhd", "w") as lut_file:
    lut_file.write("library ieee;\n")
    lut_file.write("use ieee.std_logic_1164.all;\n")
    lut_file.write("use ieee.numeric_std.all;\n\n")

    lut_file.write("library packages;\n")
    lut_file.write("use packages.types.all;\n")
    lut_file.write("use packages.constants.all;\n\n")

    lut_file.write("package calibration_LUT is\n")
    lut_file.write("    constant calibration_LUT : LUT_type := (\n")

    i = 0
    for data in value:
        if i == (2**LUT_len)-1:
            lut_file.write("        to_signed(%i, DAC_BITS)\n" % data)
        else:
            lut_file.write("        to_signed(%i, DAC_BITS),\n" % data)
            i += 1

    lut_file.write("    );\n")
    lut_file.write("end package;\n")



plt.plot(index,value)
plt.show()