# Python script to generate the saturation current look up table

import numpy as np
import matplotlib.pyplot as plt
from bitstring import BitArray

# from python.scripts.find_params import V_Float

value = []

LUT_len = 14
Bit_len = 14

Temperature_shift = 11
iSat_shift = Temperature_shift + 4
vFloat_shift = Temperature_shift - 2

min_val = -15
max_val = 15
# min_val = -8192 / 2**vFloat_shift
# max_val = 8192 / 2**vFloat_shift

index = np.linspace(min_val, max_val, num=2**LUT_len)

iSat = 0.0021
Temperature = 1.9
vFloat = -6.5

bit_length = 1024
a = BitArray(bit_length)

def lim_14_bit(x, bits=Bit_len):
    if np.isnan(x) or np.isinf(x):
        x = 0
    a.int = int(x)
    return a[bit_length-bits:bit_length].int
    # return x

def LP_Current(Voltage ,Ion_Saturation=iSat, Electron_Temperature=Temperature, Floating_Potential=vFloat):
    solved_current = Ion_Saturation*(np.exp((Voltage-Floating_Potential)/Electron_Temperature) - 1)
    return solved_current

def lut_func(x):
    eqn = LP_Current(x)
    eqn = eqn * 2**19
    # if x > min_val and x < vFloat:
    #     eqn = eqn * 2**13
    # elif x >= vFloat and x < 0:
    #     eqn = eqn * 2**11
    # else:
    #     eqn = eqn * 2**1
    # if eqn > 2**(Bit_len-1) - 1:
    #     eqn = 2**(Bit_len-1) - 1
    # elif eqn < -1.0*2**(Bit_len-1) - 1:
    #     eqn = -1.0*2**(Bit_len-1)
    return lim_14_bit(int(round(eqn)))
    # return eqn

for i in range(len(index)):
    # value.append(round(lut_func(index[i])))
    value.append(lut_func(index[i]))
print(len(value))

with open("LT_IV_LUT_pck.vhd", "w") as lut_file:
    lut_file.write("library ieee;\n")
    lut_file.write("use ieee.std_logic_1164.all;\n")
    lut_file.write("use ieee.numeric_std.all;\n\n")

    lut_file.write("library packages;\n")
    lut_file.write("use packages.types.all;\n")
    lut_file.write("use packages.constants.all;\n\n")

    lut_file.write("package LT_IV_LUT is\n")
    lut_file.write("    constant LT_IV_LUT : LUT_type := (\n")

    i = 0
    for data in value:
        if i == (2**LUT_len)-1:
            lut_file.write("        to_signed(%i, DAC_BITS)\n" % data)
        else:
            lut_file.write("        to_signed(%i, DAC_BITS),\n" % data)
            i += 1

    lut_file.write("    );\n")
    lut_file.write("end package;\n")


plt.scatter(-3.325*Temperature+vFloat, lut_func(-3.325*Temperature + vFloat), c='#ff0000')
plt.scatter(0*Temperature+vFloat, lut_func(0*Temperature + vFloat), c='#ff0000')
plt.scatter(0.64*Temperature+vFloat, lut_func(0.64*Temperature + vFloat), c='#ff0000')
plt.plot(index,value)
plt.show()