# Python script to generate the saturation current look up table

import numpy as np
import matplotlib.pyplot as plt

value = []

LUT_len = 14


min_val = -1
max_val = 15

index = np.linspace(int(min_val), int(max_val), num=2**LUT_len + 1)
index = index[1:len(index) - 1]


def lut_func(x):
    ln = np.log(x+1)
    eqn = ln
    if x > -1 and x < 0:
        eqn = eqn*(2**12)
    elif x >= 0 and x <= 2:
        eqn = eqn*(2**12)
    elif x > 2 and x <=16:
        eqn = eqn*(2**11)

    if int(round(eqn)) > (2**(LUT_len - 1) - 1 ):
        eqn = 2**(LUT_len - 1) - 1

    if int(round(eqn)) < - 2**(LUT_len - 1):
        eqn = -1 * 2**(LUT_len - 1)

    return int(round(eqn))

for i in range(len(index)):
    value.append(round(lut_func(index[i])))
print(len(value))

with open("vFloat_lut_pck.vhd", "w") as lut_file:
    lut_file.write("library ieee;\n")
    lut_file.write("use ieee.std_logic_1164.all;\n")
    lut_file.write("use ieee.numeric_std.all;\n\n")

    lut_file.write("library packages;\n")
    lut_file.write("use packages.types.all;\n")
    lut_file.write("use packages.constants.all;\n\n")

    lut_file.write("package vFloat_LUT is\n")
    lut_file.write("    constant vFloat_LUT : LUT_type := (\n")

    i = 0
    for data in value:
        if i == len(value) - 1:
            lut_file.write("        to_signed(%i, DAC_BITS)\n" % data)
        else:
            lut_file.write("        to_signed(%i, DAC_BITS),\n" % data)
            i += 1

    lut_file.write("    );\n")
    lut_file.write("end package;\n")



plt.plot(index,value)
plt.show()