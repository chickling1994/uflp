import numpy as np
import matplotlib.pyplot as plt

n_bits = 14
Temperature_shift = 11
min_temp = 0.5
starting_temp = min_temp #eV
max_temp = 1.5

Temperature_trace = np.zeros(2**n_bits)

first_POI = 0
# first_POI = int(len(Temperature_trace)*0.1)
second_POI = int(len(Temperature_trace)*0.5)
third_POI = len(Temperature_trace)
# third_POI = int(len(Temperature_trace)*0.9)

Temperature_trace[0:first_POI] = starting_temp
Temperature_trace[first_POI : second_POI] = np.linspace(starting_temp,max_temp, second_POI - first_POI)
Temperature_trace[second_POI: third_POI] = np.linspace(max_temp,starting_temp, third_POI - second_POI )
Temperature_trace[third_POI :] = starting_temp

Temperature_trace = Temperature_trace * 2**(Temperature_shift)

with open("temp_profile_pck.vhd", "w") as lut_file:
    lut_file.write("library ieee;\n")
    lut_file.write("use ieee.std_logic_1164.all;\n")
    lut_file.write("use ieee.numeric_std.all;\n\n")

    lut_file.write("library packages;\n")
    lut_file.write("use packages.types.all;\n")
    lut_file.write("use packages.constants.all;\n\n")

    lut_file.write("package temperature_profile_pck is\n")
    lut_file.write("    constant temperature_profile_LUT : LUT_type := (\n")

    i = 0
    for data in Temperature_trace:
        if i == (2**n_bits)-1:
            lut_file.write("        to_signed(%i, DAC_BITS)\n" % data)
        else:
            lut_file.write("        to_signed(%i, DAC_BITS),\n" % data)
            i += 1

    lut_file.write("    );\n")
    lut_file.write("end package;\n")

plt.plot(Temperature_trace/2**Temperature_shift)