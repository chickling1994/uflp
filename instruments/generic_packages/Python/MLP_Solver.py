import numpy as np
import math

def iSat(I, Vb, Te, Vf):
    return I/(np.exp((Vb-Vf)/(Te/2**2))-1)

def Vf(I, Vb, Te , iSat):
    return Vb - np.log(I/iSat + 1) * Te

def Te(I, Vb, iSat, Vf):
    return (Vb - Vf)/(np.exp(I/iSat + 1))

