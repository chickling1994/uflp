import numpy as np
import math
import matplotlib.pyplot as plt

iSat = 0.2
Te = 25
Vf = -70

iSat_VHDL = 511
Te_VHDL = 3639
Vf_VHDL = -2392

Vb = 0
Binary = True

def LP_Current(Vb, Binary=True,iSat=iSat, Te=Te, Vf=Vf):
    current = iSat * (-1 + np.exp((Vb - Vf)/Te))
    if Binary == True :
        current = current * 2**11
    return current

def LP_Current_VHDL(Vb, Binary=True,iSat=iSat_VHDL, Te=Te_VHDL, Vf=Vf_VHDL):
    current = (iSat/2**11) * (-1 + np.exp(((Vb/2**5) - (Vf/2**5))/(Te/2**7)))
    if Binary == True :
        current = current * 2**11
    return current