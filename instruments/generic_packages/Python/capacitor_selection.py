import math
import numpy as np
import matplotlib.pyplot as plt

n_bits = 14
iSat_shift = 11
plasma_temperature = 5 #eV
delta_voltage = 4*plasma_temperature
frequency = 3E6 #3MHz
delta_time = 1/frequency
capacitor_values = [6.85E-11, 6.85E-11, 1.2E-10, 2.22E-10, 3.93E-10, 8.27E-10, 1.51E-9, 3E-9]
capacitor_combinations = np.zeros(2**len(capacitor_values))
current_combinations = np.zeros(2**len(capacitor_values))

cap_1 = 1
cap_2 = 2
cap_3 = 4
cap_4 = 8
cap_5 = 16
cap_6 = 32
cap_7 = 64
cap_8 = 128

for i in range (0, len(capacitor_combinations)):
    if math.floor(i/cap_1) % 2 != 0:
        capacitor_combinations[i] = capacitor_combinations[i] + capacitor_values[0]    
    if math.floor(i/cap_2) % 2 != 0:
        capacitor_combinations[i] = capacitor_combinations[i] + capacitor_values[1]
    if math.floor(i/cap_3) % 2 != 0:
        capacitor_combinations[i] = capacitor_combinations[i] + capacitor_values[2]    
    if math.floor(i/cap_4) % 2 != 0:
        capacitor_combinations[i] = capacitor_combinations[i] + capacitor_values[3]
    if math.floor(i/cap_5) % 2 != 0:
        capacitor_combinations[i] = capacitor_combinations[i] + capacitor_values[4]    
    if math.floor(i/cap_6) % 2 != 0:
        capacitor_combinations[i] = capacitor_combinations[i] + capacitor_values[5]
    if math.floor(i/cap_7) % 2 != 0:
        capacitor_combinations[i] = capacitor_combinations[i] + capacitor_values[6]    
    if math.floor(i/cap_8) % 2 != 0:
        capacitor_combinations[i] = capacitor_combinations[i] + capacitor_values[7]
        

def find_capacitor(delta_voltage, delta_time, x):
    optimal_capacitance  = x/(delta_voltage/delta_time)
    return optimal_capacitance

def find_current(delta_voltage, delta_time, x):
    optimal_current  = x * (delta_voltage/delta_time)
    return optimal_current

def plot_current_v_cap():
    plt.figure()
    plt.plot(capacitor_combinations)
    plt.xlabel("Current, A")
    plt.ylabel("Capacitance, F")
    plt.show()

x = np.linspace(0,255,256)
current_combinations = find_current(delta_voltage, delta_time, capacitor_combinations)
current_gradient, current_offset = np.polyfit(x, current_combinations, 1)

y = current_gradient*np.linspace(0,255,2**n_bits) + current_offset

# y_LUT = np.round((y - current_offset)/current_gradient,0)
y_LUT = (y - current_offset)/current_gradient
x_LUT = np.round(y*2**iSat_shift,0)
# x_LUT = y

# with open("capacitor_bank_pck.vhd", "w") as lut_file:
#     lut_file.write("library ieee;\n")
#     lut_file.write("use ieee.std_logic_1164.all;\n")
#     lut_file.write("use ieee.numeric_std.all;\n\n")

#     lut_file.write("library packages;\n")
#     lut_file.write("use packages.types.all;\n")
#     lut_file.write("use packages.constants.all;\n\n")

#     lut_file.write("package capacitor_bank_pck is\n")
#     lut_file.write("    constant capacitor_bank_LUT : LUT_type := (\n")

#     i = 0
#     for data in y_LUT:
#         if i == (2**n_bits)-1:
#             lut_file.write("        to_signed(%i, DAC_BITS)\n" % data)
#         else:
#             lut_file.write("        to_signed(%i, DAC_BITS),\n" % data)
#             i += 1

#     lut_file.write("    );\n")
#     lut_file.write("end package;\n")

plt.plot(x_LUT, y_LUT)
plt.show()

