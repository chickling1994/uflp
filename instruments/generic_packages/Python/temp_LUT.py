# Python script to generate the saturation current look up table

import numpy as np
import matplotlib.pyplot as plt
from math import log

value = []

LUT_len = 14

# v_float_guess = -20
# Te_guess_eV = 1

# min_val = np.exp(-3 - (v_float_guess - v_float_guess)/Te_guess_eV)-1
min_val = 0
# max_val = np.exp(-3 - (v_float_guess + v_float_guess)/Te_guess_eV)-1
max_val = 8

index = np.linspace(int(min_val), int(max_val), num=(2**LUT_len) + 2)
index = index[1:len(index) - 1]



def lut_func(x):
    ln = log(x)
    eqn = 1/(ln)

    if x >= 0 and x < 1:
        eqn = eqn*(2**9)
    elif x >= 1 and x <= 2:
        eqn = eqn*(2**9)
    elif x > 2 and x <= 8:
        eqn = eqn*(2**12)

    if int(round(eqn)) > (2**(LUT_len - 1) - 1 ):
        eqn = 2**(LUT_len - 1) - 1

    if int(round(eqn)) < - 2**(LUT_len - 1):
        eqn = -2**(LUT_len - 1)

    return int(round(eqn))

for i in range(len(index)):
    value.append(round(lut_func(index[i])))

# with open("Temperature_LUT.vhd", "w") as lut_file:
#     lut_file.write("library ieee;\n")
#     lut_file.write("use ieee.std_logic_1164.all;\n")
#     lut_file.write("use ieee.numeric_std.all;\n\n")

#     lut_file.write("library packages;\n")
#     lut_file.write("use packages.types.all;\n")
#     lut_file.write("use packages.constants.all;\n\n")

#     lut_file.write("package Temperature_LUT is\n")
#     lut_file.write("    constant Temperature_LUT : LUT_type := (\n")

#     i = 0
#     for data in value:
#         if i == (len(index))-1:
#             lut_file.write("        to_signed(%i, DAC_BITS)\n" % data)
#         else:
#             lut_file.write("        to_signed(%i, DAC_BITS),\n" % data)
#         i += 1

#     lut_file.write("    );\n")
#     lut_file.write("end package;\n")

plt.plot(index,value,'o')
plt.show()