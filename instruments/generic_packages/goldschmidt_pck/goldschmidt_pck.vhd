library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity gold is
    GENERIC(
        n: integer:= 14; 
        p: integer:= 17; -- p - n = points above decimal point 
        m: integer:= 32);
    port (
        clk : in std_logic;
        rst : in std_logic;

        div_complete : out std_logic;

        Numerator : in std_logic_vector(n-1 downto 0);
        Denominator : in std_logic_vector(n-1 downto 0);

        Quotient : out std_logic_vector(n-1 downto 0) 
        -- 2n - p = power of answer e.g. n=14, p=16, answer is 2_12 => quotient / 2**12 = answer
    );
end gold; 

architecture rtl of gold is
    Signal N_Resize : signed(p downto 0);
    Signal N_Multiply : signed(2*p + 1 downto 0);
    Signal Prev_N : signed(p downto 0);
    
    Signal D_Resize : signed(p downto 0);
    Signal D_Multiply : signed(2*p + 1 downto 0);
    Signal Prev_D : signed(p downto 0);
    
    Signal Prev_F : signed(p downto 0);
    Signal F_Temp : signed(p downto 0);
    Signal F_solve : signed(p downto 0);

    Signal count: integer RANGE 0 TO m-1;
    TYPE finite_states IS (start, iterate, done, idle);
    Signal current_state: finite_states;

    Signal Current_Numerator : std_logic_vector(Numerator'range);
    Signal Current_Denominator : std_logic_vector(Denominator'range);    
    Signal Last_Numerator : std_logic_vector(Numerator'range);
    Signal Last_Denominator : std_logic_vector(Denominator'range);
begin
    N_Multiply <= Prev_N * F_Temp;
    -- N_Multiply <= N_Resize * F_Temp;
    D_Multiply <= Prev_D * F_Temp;
    -- D_Multiply <= D_Resize * F_Temp;
    -- F_solve <= 2**(n+1) - shift_right(D_Resize * F_Temp, n)(D_Resize'range);

    PROC_N : process(clk)
    begin
        if rising_edge(clk) then
            Prev_N <= N_Resize;
                Case current_state is
                    when start => 
                        N_Resize <= to_signed(to_integer(signed(Numerator)), N_Resize'length);
                    when iterate =>
                        -- N_Resize <= shift_right(Prev_N * F_Temp , n)(N_Resize'range);
                        N_Resize <= shift_right(N_Multiply , n)(N_Resize'range);
                    when others => NULL;
                end case;
        end if;
    end process; -- PROC_N

    PROC_D : process(clk)
    begin
        if rising_edge(clk) then
            Prev_D <= D_Resize;
                Case current_state is
                    when start => 
                        D_Resize <= to_signed(to_integer(signed(Denominator)), D_Resize'length);
                    when iterate =>
                        -- D_Resize <= shift_right(Prev_D * F_Temp + 1, n)(D_Resize'range);
                        D_Resize <= shift_right(D_Multiply + 1, n)(D_Resize'range);
                    when others => NULL;
                end case;
        end if;
    end process; -- PROC_N

    PROC_F : process(clk)
    begin
        if rising_edge(clk) then
            Prev_F <= F_Temp;
            Case current_state is
                when start => 
                    F_Temp <= to_signed(to_integer(signed(Denominator)), F_Temp'length);
                when iterate =>
                    F_Temp <= 2**(n+1) - D_Resize;
                    -- F_Temp <= 2**(n+1) - shift_right(D_Multiply, n)(D_Resize'range);
                    -- F_Temp <= F_solve;
                when others => NULL;
            end case;
        end if;
    end process; -- PROC_N

    F_counter: PROCESS(clk)
    BEGIN
        if rising_edge(clk) then
            if rst = '1' then
                count <= 0;
            else
                Case current_state is
                    when start => 
                        count <= (count+1);
                    when iterate => 
                        count <= (count+1);
                    -- when idle => 
                        -- count <= (count+1);
                    when others => 
                        count <= 0; 
                END case;
            end if;
        end if;
    END PROCESS;

    next_state: PROCESS(clk)
    BEGIN
        if rising_edge(clk) then
            if rst = '1' then
                current_state <= start;
                div_complete <= '0';
            else
                    CASE current_state IS
                        WHEN start =>
                            if count = 1 then
                                current_state <= iterate;
                                div_complete <= '0';
                            end if;
                        WHEN iterate => 
                            --if count = m - 1 or (count > 5 and abs(F_Temp - D_Resize) < 3) then
                            if count = m - 1 then
                                current_state <= done;
                            end if;
                            div_complete <= '0';
                        WHEN done => 
                            div_complete <= '1';
                            -- Quotient <= std_logic_vector(Prev_N(Prev_N'length - 1 downto Prev_N'length - Quotient'length));
                            Quotient <= std_logic_vector(N_Resize(N_Resize'length - 1 downto N_Resize'length - Quotient'length));
                            current_state <= idle;
                        WHEN idle =>
                            div_complete <= '0';
                            -- if Last_Numerator /= Current_Numerator or Last_Denominator /= Current_Denominator then
                            -- if Last_Numerator /= Current_Numerator then
                                current_state <= start;
                            -- end if;
                    END CASE;
            end if;
        end if;
    END PROCESS;

    REQUEST_DIVISION : process(clk)
    begin
        if rising_edge(clk) then
            Current_Numerator <= Numerator;
            Current_Denominator <= Denominator;
            Last_Numerator <= Current_Numerator;
            Last_Denominator <= Current_Denominator;            
        end if;
    end process; -- REQUEST_DIVISION
end architecture;