library IEEE;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

package constants is
  
    -- RedPitaya has a 125 MHz oscillator
    constant clock_frequency : real := 125.0e6;

    -- DAC bits on the RedPitaya
    constant DAC_bits: integer := 14;

    -- Desired amount of Block Ram to use
    constant BRam_width : integer := 2**15 - 1;

    
    constant Temperature_shift : integer := 11;

    -- DP: 3_11 => up to 4A with 11 bit decimal precision (0.5mA)
    constant I_shift : integer := Temperature_shift + 8;
    
    -- Shift associated with voltage input
    constant V_float_shift : integer := Temperature_shift - 2;
    constant bias_shift : integer := V_float_shift;
    
    -- constant Temperature_guess : integer := 3639;--41 * 2**Temperature_shift ; 
    constant Temperature_guess : integer := 7000;--41 * 2**Temperature_shift ; 
    
    constant V_float_guess : integer := 0;---1 * 74.75 * 2**5 ; 
    -- constant V_float_guess : integer := -2392;---1 * 74.75 * 2**5 ; 

    -- constant iSat_guess : integer := 409;--0.2 * 2**11 ;
    constant iSat_guess : integer := 7000;--0.2 * 2**11 ;
    -- constant simulation_on : integer := 1;


    constant bias_on_clock_cycles : integer := 512;
    constant bias_off_clock_cycles : integer := 5 ;
    -- constant sample_data_clock_cycles : integer := 10;
    constant sample_data_clock_cycles : integer := bias_on_clock_cycles - 20;
    constant total_sweep_cycles : integer := (bias_on_clock_cycles + bias_off_clock_cycles) * 3;

end package;