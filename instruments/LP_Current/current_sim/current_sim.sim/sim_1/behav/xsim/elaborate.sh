#!/bin/bash -f
# ****************************************************************************
# Vivado (TM) v2019.1 (64-bit)
#
# Filename    : elaborate.sh
# Simulator   : Xilinx Vivado Simulator
# Description : Script for elaborating the compiled design
#
# Generated by Vivado on Thu Jan 13 18:59:28 GMT 2022
# SW Build 2552052 on Fri May 24 14:47:09 MDT 2019
#
# Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
#
# usage: elaborate.sh
#
# ****************************************************************************
set -Eeuo pipefail
echo "xelab -wto 978bce86ca31480b8c7c954f656e6603 --incr --debug typical --relax --mt 8 -L packages -L pcr -L sim_packages -L sim_pcr -L secureip --snapshot top_pcr_tb_behav sim_pcr.top_pcr_tb -log elaborate.log"
xelab -wto 978bce86ca31480b8c7c954f656e6603 --incr --debug typical --relax --mt 8 -L packages -L pcr -L sim_packages -L sim_pcr -L secureip --snapshot top_pcr_tb_behav sim_pcr.top_pcr_tb -log elaborate.log

