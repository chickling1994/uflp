#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import os
import time
import matplotlib.pyplot as plt

from pcr import PCS
from koheron import connect

# host = os.getenv('HOST', '192.168.1.100')
host = os.getenv('HOST', '192.168.1.101')
# host = os.getenv('HOST', '192.168.1.102')
client = connect(host, name='Current_Responce_System')
driver = PCS(client)

def run_PCR():
    buffer_length = []
    i = 0
    if driver.get_Reset() == 0:
        driver.set_Reset()
        time.sleep(1)
        driver.set_Reset()
    else:
        driver.set_Reset()

    driver.set_trigger()
    while True:
        samples = driver.get_buffer_length()
        if samples == 0:
            i = i + 1
            if i == 5000:
                break
        else:
            i = 0
        buffer_length.append(samples)

    dataArray_PCR = driver.get_PCR_data()
    print(len(dataArray_PCR))

    saveStr = "PCR_test_data"
    print(saveStr)
    np.save(saveStr, dataArray_PCR)

    t = np.arange(0,len(dataArray_PCR))
    t = (acquisition_length/len(t)) * t

    Temperature_shift = 11
    iSat_shift = Temperature_shift + 4
    vFloat_shift = Temperature_shift - 2

    y = np.zeros([len(dataArray_PCR), 32])
    sample_length = len(dataArray_PCR)
    for i in range(0,len(dataArray_PCR[0:sample_length])): 
        if (len(bin(dataArray_PCR[i]))) == 34:
            y[i][0:32] = [int(d) for d in str(bin(dataArray_PCR[i]))[2:]]  

    Current_array = np.zeros(len(dataArray_PCR[0:sample_length]))
    Start_Pos = 4
    for i in range(0,len(Current_array)): 
        for j in range(Start_Pos,18):
            if j == Start_Pos:
                if int(y[i][j]) == 1:
                    Current_array[i] = -(2**13)
            elif int(y[i][j]) == 1: 
                Current_array[i] = Current_array[i] + 2**(13-j+Start_Pos)
        Current_array[i] = Current_array[i] / 2**15

    Bias_array = np.zeros(len(dataArray_PCR[0:sample_length])) 
    for i in range(0,len(Bias_array)):
        for j in range(18,32):
            if j == 18:
                if int(y[i][j]) == 1:
                    Bias_array[i] = -(2**13)
            elif int(y[i][j]) == 1: 
                Bias_array[i] = Bias_array[i] + 2**(13-j+18)
        Bias_array[i] = Bias_array[i] / 2**9

    plt.figure("Bias")
    plt.plot(t[:sample_length], Bias_array,'o')
    plt.xlabel("Time, s")
    plt.ylabel("Bias, V")

    plt.figure("Current")
    plt.plot(t[:sample_length], Current_array,'o')
    plt.xlabel("Time, s")
    plt.ylabel("Current, A")

    plt.show()

 #seconds
acquisition_length = 1
driver.set_acquisition_length(acquisition_length)

def acquire_time(acq):
    driver.set_acquisition_length(acq)

def manual_params(Te, iSat, Vf):
    driver.set_Switch(1)
    driver.set_Temperature(int(Te * 2**11))
    driver.set_ISat(int(iSat * 2**15))
    driver.set_Vfloating(int(Vf * 2**9))