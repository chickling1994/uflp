----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : Feb 2020
-- 
-- Module Name: pcr_data_aquire_tb
-- Project Name: PCR
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Aquisition Calibration
-- 
-- Dependencies: constants, sim_constants, sim_packages, pcr
----------------------------------------------------------------------------------

library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;
    use std.env.finish;

library pcr;

library packages;
    use packages.constants.all;

library sim_packages;
    use sim_packages.sim_constants.all;
    use sim_packages.sim_subprograms.all;

entity pcr_data_aquire_tb is
end pcr_data_aquire_tb ; 

architecture sim of pcr_data_aquire_tb is
    -- DUT Inputs
    signal clk : std_logic := '1';
    signal rst : std_logic := '1';
    signal bias_in : std_logic_vector(DAC_Bits - 1 downto 0);

    -- DUT Outputs
    signal bias_out : std_logic_vector(DAC_Bits - 1 downto 0);

begin
    -- Generate clock as defined by sim_subprograms and sim_constants
    gen_clock(clk);

    -- Instanciate DUT
    DUT : entity pcr.pcr_data_aquire(rtl)
    port map (
        clk => clk ,
        rst => rst ,

        bias_in => bias_in,
        bias_out => bias_out
    );

    -- Check all bias states for calibration correction
    PROC_BIAS : process
    begin
        for i in -(2**(DAC_Bits-1)) to 2**(DAC_Bits-1) - 1 loop
            bias_in <= std_logic_vector(to_signed(i, bias_in'length));
            wait until rising_edge(clk);
        end loop;
        finish;
    end process ; -- PROC_BIAS

    -- Standard reset process
    PROC_RESET : process
    begin
        wait for 20*clock_period;
        rst <= '0';
        wait for 10000*clock_period;
    end process; -- PROC_RESET
end architecture ;