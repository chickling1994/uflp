----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : Feb 2020
-- 
-- Module Name: pcr_data_aquire
-- Project Name: PCR
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Aquisition Calibration
-- 
-- Dependencies: constants
----------------------------------------------------------------------------------

library ieee ;
use ieee.std_logic_1164.all ;
use ieee.numeric_std.all ;

library packages;
use packages.constants.all;

entity pcr_data_aquire is
  port (
    -- Generic to all cores
    clk : in std_logic;
    rst : in std_logic; -- '1' = in reset
    
    -- Inputs to the core
    bias_in : in std_logic_vector(DAC_Bits - 1 downto 0);
    V_float : in std_logic_vector(DAC_Bits - 1 downto 0);

    -- Outputs from the core
    bias_out : out std_logic_vector(DAC_Bits - 1 downto 0)
  ) ;
end pcr_data_aquire ; 

architecture rtl of pcr_data_aquire is

-- Arithmatic Signals
signal bias_sample : signed(DAC_Bits - 1 downto 0);
signal bias_multiply : signed(2*DAC_Bits - 1 downto 0);
signal bias_shift : signed(2*DAC_Bits - 1 downto 0);
signal bias_offset : signed(2*DAC_Bits - 1 downto 0);
signal bias_resize : signed(DAC_Bits - 1 downto 0);

begin
    -- purpose: Provide gradient and offset to an input
    -- type   : synchronus with reset
    -- inputs : clk, rst, bias_in
    -- outputs: bias_out
    PROC_Bias : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                bias_out <= std_logic_vector(to_signed(-5000, bias_out'length));
            else
                -- -- y = 1.15x + 34.8
                -- bias_sample <= signed(bias_in);
                -- bias_multiply <= bias_sample * 4631;
                -- bias_shift <= bias_multiply / 2**12;
                -- bias_offset <= bias_shift + 59;
                -- bias_resize <= bias_offset(bias_resize'range);
                -- bias_out <= std_logic_vector(bias_resize);

                -- y = 5x
                bias_sample <= signed(bias_in);
                bias_multiply <= bias_sample * 5;
                bias_shift <= bias_multiply / 2;
                bias_offset <= bias_shift;
                bias_resize <= bias_offset(bias_resize'range);
                bias_out <= std_logic_vector(bias_resize);


                -- -- Uncomment to remove calibration
                bias_out <= std_logic_vector(signed(bias_in) + signed(V_float));
            end if;
        end if ;
    end process ; -- PROC_Current


end architecture ;