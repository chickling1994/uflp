----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : Feb 2020
-- 
-- Module Name: pcr_data_output
-- Project Name: PCR
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Output Calibration
-- 
-- Dependencies: constants
----------------------------------------------------------------------------------

library ieee ;
use ieee.std_logic_1164.all ;
use ieee.numeric_std.all ;

library packages;
    use packages.constants.all;

entity pcr_data_output is
    port (
        -- Generic to all cores
        clk : in std_logic;
        rst : in std_logic; -- '1' = in reset

        -- Inputs to the core
        bias_in : in std_logic_vector(DAC_Bits - 1 downto 0);
        LP_Current : in std_logic_vector(DAC_Bits - 1 downto 0);


        -- Outputs from the core
        current_out_dac : out std_logic_vector(DAC_Bits - 1 downto 0);
        bias_out_dac : out std_logic_vector(DAC_Bits - 1 downto 0)
    ) ;
end pcr_data_output ; 

architecture rtl of pcr_data_output is
-- Arithmatic Signals for Bias
signal bias_sample : signed(DAC_Bits - 1 downto 0);
signal bias_multiply : signed(2*DAC_Bits - 1 downto 0);
signal bias_shift : signed(2*DAC_Bits - 1 downto 0);
signal bias_offset : signed(2*DAC_Bits - 1 downto 0);
signal bias_resize : signed(DAC_Bits - 1 downto 0);

-- Arithmatic Signals for Current
signal current_sample : signed(DAC_Bits - 1 downto 0);
signal current_multiply : signed(2*DAC_Bits - 1 downto 0);
signal current_shift : signed(2*DAC_Bits - 1 downto 0);
signal current_offset : signed(2*DAC_Bits - 1 downto 0);
signal current_resize : signed(DAC_Bits - 1 downto 0);
begin
    -- purpose: Provide gradient and offset to an input
    -- type   : synchronus with reset pipelined multiplication
    -- inputs : clk, rst, LP_Current
    -- outputs: current_out_dac
    PROC_Output : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                current_out_dac <= std_logic_vector(to_signed(-800, current_out_dac'length));
            else
                -- y = 0.998x - 134 
                current_sample <= signed(LP_Current);
                -- current_multiply <= current_sample * 3932;
                -- current_shift <= current_multiply / 2**12;
                -- current_offset <= current_shift - 221;
                current_multiply <= current_sample * 3;
                -- current_multiply <= current_sample * 1;
                current_resize <= current_multiply(current_resize'range);
                current_out_dac <= std_logic_vector(current_resize);
                -- -- Uncomment to remove calibration
                current_out_dac <= LP_Current;
            end if;
        end if ;
    end process ; -- PROC_Current

    -- purpose: Provide gradient and offset to an input
    -- type   : synchronus with reset pipelined multiplication
    -- inputs : clk, rst, bias_in
    -- outputs: bias_out_dac
    PROC_Bias : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                bias_out_dac <= std_logic_vector(to_signed(-5000, bias_out_dac'length));
            else
                -- y = 1.01x - 79.8 
                bias_sample <= signed(bias_in);
                bias_multiply <= bias_sample * 4022;
                bias_shift <= bias_multiply / 2**12;
                bias_offset <= bias_shift - 304;
                bias_resize <= bias_offset(bias_resize'range);
                bias_out_dac <= std_logic_vector(bias_resize);
                -- -- Uncomment to remove calibration
                bias_out_dac <= std_logic_vector(bias_sample);   
            end if;
        end if ;
    end process ; -- PROC_Current
end architecture ;