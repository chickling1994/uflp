----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : Feb 2020
-- 
-- Module Name: pcr_data_output_tb
-- Project Name: PCR
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Aquisition Calibration
-- 
-- Dependencies: constants, sim_constants, sim_packages, pcr
----------------------------------------------------------------------------------

library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;
    use std.env.finish;

library pcr;

library packages;
    use packages.constants.all;

library sim_packages;
    use sim_packages.sim_constants.all;
    use sim_packages.sim_subprograms.all;

entity pcr_data_output_tb is
end pcr_data_output_tb ; 

architecture sim of pcr_data_output_tb is
    -- DUT Inputs
    signal clk : std_logic := '1';
    signal rst : std_logic := '1';
    signal bias_in : std_logic_vector(DAC_Bits - 1 downto 0);
    signal LP_Current : std_logic_vector(DAC_Bits - 1 downto 0);
    
    -- DUT Outputs
    signal current_out_dac :  std_logic_vector(DAC_Bits - 1 downto 0);
    signal bias_out_dac :  std_logic_vector(DAC_Bits - 1 downto 0);

begin
    -- Generate clock as defined by sim_subprograms and sim_constants
    gen_clock(clk);

    -- Instanciate DUT
    DUT : entity pcr.pcr_data_output(rtl)
    port map (
        clk => clk,
        rst => rst,
        bias_in => bias_in,
        LP_Current => LP_Current,

        current_out_dac => current_out_dac,
        bias_out_dac => bias_out_dac
    );

    -- Check all bias states for calibration correction
    PROC_BIAS : process
    begin
        for i in -(2**(DAC_Bits-1)) to 2**(DAC_Bits-1) - 1 loop
            bias_in <= std_logic_vector(to_signed(i, bias_in'length));
            wait until rising_edge(clk);
        end loop;
        finish;
    end process ; -- PROC_BIAS

    -- Check all Current states for calibration correction
    PROC_CURRENT : process
    begin
        for i in -(2**(DAC_Bits-1)) to 2**(DAC_Bits-1) - 1 loop
            LP_Current <= std_logic_vector(to_signed(i, LP_Current'length));
            wait until rising_edge(clk);
        end loop;
        finish;
    end process ; -- PROC_BIAS

    -- Standard reset process
    PROC_RESET : process
    begin
        wait for 20*clock_period;
        rst <= '0';
        wait for 10000*clock_period;
    end process; -- PROC_RESET
end architecture ;