----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : Oct 2020
-- 
-- Module Name: top_pcr
-- Project Name: PCR
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Structurally pulls together all rtl cores
-- 
-- Dependencies: constants, 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library pcr;

library packages;
use packages.constants.all;

entity top_pcr is
    port (
        -- Inputs generic to all cores
        clk : in std_logic;
        rst_button : in std_logic;

        -- Python configurable inputs
        Switch : in std_logic_vector(31 downto 0) ;
        sat_current_in : in std_logic_vector(DAC_Bits - 1  downto 0);
        temperature_in : in std_logic_vector(DAC_Bits - 1 downto 0);
        v_Float_in : in std_logic_vector(DAC_Bits - 1 downto 0);
        
        -- Input of interest
        bias_in : in std_logic_vector(DAC_Bits - 1  downto 0);
        -- Outputs of interest
        current_out_dac : out std_logic_vector(DAC_Bits - 1 downto 0);
        bias_out_dac : out std_logic_vector(DAC_Bits - 1 downto 0);

        trigger : in std_logic;
        acquisition_length : in std_logic_vector(31 downto 0);

        -- Concatenated data for status register retrieval
        tdata : out std_logic_vector(31 downto 0);
        -- Data valid signal for data_out
        tvalid : out std_logic
    );
end top_pcr; 

architecture str of top_pcr is
    -- Reset entity
    signal rst : std_logic;
    signal rst_hang : std_logic;

    -- Interal versions of bias and current
    signal LP_bias_rx : std_logic_vector(DAC_bits - 1 downto 0);
    signal LP_bias_tx : std_logic_vector(DAC_bits - 1 downto 0);
    signal LP_Current : std_logic_vector(DAC_bits - 1 downto 0);

    -- Internal versions of plasma parameters
    signal sat_current : std_logic_vector(DAC_bits - 1 downto 0);
    signal Temp : std_logic_vector(DAC_bits - 1 downto 0);
    signal V_float : std_logic_vector(DAC_bits - 1 downto 0);
begin

    PROC_RESET : entity packages.reset(rtl)
    port map (
        clk => clk,
        rst_in => rst_button,

        trigger => trigger,
        acquisition_length => acquisition_length,

        rst_out => rst,
        rst_hang => rst_hang
    );

    PCR_COLLECT : entity pcr.pcr_data_collect(rtl)
    port map (
        clk => clk ,
        rst => rst ,

        sat_current => sat_current,
        Temp => Temp,
        V_float => V_float,

        trigger => trigger,
        acquisition_length => acquisition_length,
        tvalid => tvalid,

        LP_bias_rx => LP_bias_rx,
        LP_Current => LP_Current,

        data_out => tdata
    );

    PCR_Current : entity pcr.pcr_current_responce(rtl)
    port map (
        clk => clk ,
        rst => rst ,
        LP_bias_rx => LP_bias_rx,
        
        LP_bias_tx => LP_bias_tx,
        current_out => LP_Current,

        sat_current => sat_current,
        Temp => Temp,
        V_float => V_float
    );

    PCR_AQUIRE : entity pcr.pcr_data_aquire(rtl)
    port map(
        clk => clk,
        rst =>  rst,
        V_float => V_float,
        bias_in => bias_in,

        bias_out => LP_bias_rx
    );        
    
    PCR_INPUT : entity pcr.pcr_input_control(rtl)
    port map(
        clk => clk, 
        rst => rst, 
    
        switch => switch, 
    
        sat_current_in => sat_current_in,
        temperature_in => temperature_in,
        v_Float_in => v_Float_in,
    
        sat_current_out => sat_current,
        temperature_out => Temp,
        v_Float_out => V_float
    );    
    
    PCR_OUTPUT : entity pcr.pcr_data_output(rtl)
    port map(
        clk => clk,
        rst =>  rst,

        bias_in => LP_Bias_rx,
        LP_Current => LP_Current,

        current_out_dac => current_out_dac,
        bias_out_dac => bias_out_dac
    );

end architecture;