set display_name {Top PCR core}

set core [ipx::current_core]

set_property DISPLAY_NAME $display_name $core
set_property DESCRIPTION $display_name $core

set_property VENDOR {UOL} $core
set_property VENDOR_DISPLAY_NAME {Chris} $core
set_property COMPANY_URL {https://www.liverpool.ac.uk} $core

# set_property top top [current_fileset]