----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : Feb 2020
-- 
-- Module Name: data_aquire_tb
-- Project Name: PCR
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Aquisition Calibration
-- 
-- Dependencies: constants, sim_constants, sim_packages, pcr
----------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use std.env.finish;

library pcr;

library packages;
    use packages.constants.all;

library sim_packages;
    use sim_packages.sim_constants.all;
    use sim_packages.sim_subprograms.all;

entity top_pcr_tb is
end top_pcr_tb; 

architecture sim of top_pcr_tb is
    signal clk : std_logic := '1';
    signal rst_button : std_logic := '1';

    -- Python configurable inputs
    signal Switch : std_logic_vector(31 downto 0) ;
    signal sat_current_in : std_logic_vector(DAC_Bits - 1  downto 0);
    signal temperature_in : std_logic_vector(DAC_Bits - 1 downto 0);
    signal v_Float_in : std_logic_vector(DAC_Bits - 1 downto 0);
    
    -- Input of interest
    signal bias_in : std_logic_vector(DAC_Bits - 1  downto 0);
    -- Outputs of interest
    signal current_out_dac : std_logic_vector(DAC_Bits - 1 downto 0);
    signal bias_out_dac : std_logic_vector(DAC_Bits - 1 downto 0);

    -- Concatenated data for status register retrieval
    signal data_out : std_logic_vector(31 downto 0);
    -- Data valid signal for data_out
    signal data_valid : std_logic;

    -- DUT Signals
    signal temperature_Register : signed(DAC_bits - 1 downto 0);
    signal sat_current : std_logic_vector(DAC_bits - 1 downto 0);
    signal Temperature : std_logic_vector(DAC_bits - 1 downto 0);
    signal V_float : std_logic_vector(DAC_bits - 1 downto 0);
begin
    gen_clock(clk);
    
    DUT_PCR : entity pcr.top_pcr(str)
    port map (
        clk => clk,
        rst_button => rst_button,
    
        -- Python configurable inputs
        Switch => Switch,
        sat_current_in => sat_current_in,
        temperature_in => temperature_in,
        v_Float_in => v_Float_in,
        
        -- Input of interest
        bias_in => bias_in,
        -- Outputs of interest
        current_out_dac => current_out_dac,
        bias_out_dac => bias_out_dac,
    
        -- Concatenated data for status register retrieval
        data_out => data_out,
        -- Data valid signal for data_out
        data_valid => data_valid
    );

    -- Retrieve the plasma parameters from input_control to set the bias
    DUT_INPUT_CONTROL : entity pcr.pcr_input_control(rtl)
    port map (
        -- Generic to all cores
        clk => clk,
        rst => << signal DUT_PCR.rst : std_logic >>,

        -- Inputs to the core, can be controlled via control registers
        switch => switch,
        sat_current_in => sat_current_in,
        temperature_in => temperature_in,
        v_Float_in => v_Float_in,

        -- Outputs from the core, can be assigned to LUT values
        sat_current_out => sat_current,
        temperature_out => Temperature,
        v_Float_out => V_float,
        data_valid => data_valid
    );

    -- Define constants for the plasma parameters
    PROC_PLASMA_PARAMS : process
    begin
        Switch <= (others => '0');
        wait until rising_edge(clk);
    end process ; -- PROC_PLASMA_PARAMS

    -- Convert the temperature to a bias voltage
    PROC_TEMPERATURE : process
    begin
        temperature_Register <= shift_right(signed(Temperature), (Temperature_shift - bias_shift));
        wait until rising_edge(clk);
    end process ; -- PROC_TEMPERATURE

    -- Apply the 3 bias states with a predefined clock cycle gap
    PROC_BIAS : process
    begin
        bias_in <= std_logic_vector(to_signed(to_integer((-4915 * temperature_Register)/2**11 + signed(V_float)),bias_in'length));
        wait for (bias_on_clock_cycles+bias_off_clock_cycles)*clock_period;
        bias_in <= std_logic_vector(to_signed(to_integer((1311 * temperature_Register)/2**11 + signed(V_float)),bias_in'length));
        wait for (bias_on_clock_cycles+bias_off_clock_cycles)*clock_period;
        bias_in <= std_logic_vector(to_signed(to_integer(0 * temperature_Register + signed(V_float)) ,bias_in'length));
        wait for (bias_on_clock_cycles+bias_off_clock_cycles)*clock_period;
    end process; -- PROC_BIAS
    
    -- PROC_RST : process
    -- begin
    --   wait for 150*clock_period;
    --   rst_button <= '0';
    --   wait until rising_edge(clk);
    -- end process; -- PROC_RST

end architecture;