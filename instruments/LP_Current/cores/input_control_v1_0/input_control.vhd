----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : Jan 2020
-- 
-- Module Name: pcr_input_control
-- Project Name: PCR
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Aquisition Calibration
-- 
-- Dependencies: constants
----------------------------------------------------------------------------------
library ieee ;
use ieee.std_logic_1164.all ;
use ieee.numeric_std.all ;

library packages;
use packages.constants.all;
use packages.temperature_profile_pck.all; -- Holds values for temp in LUT
use packages.types.all;


entity pcr_input_control is
  port (
    -- Generic to all cores
    clk : in std_logic;
    rst : in std_logic;

    -- Inputs to the core, can be controlled via control registers
    switch : in std_logic_vector(31 downto 0);
    sat_current_in : in std_logic_vector(Dac_Bits - 1 downto 0);
    temperature_in : in std_logic_vector(Dac_Bits - 1 downto 0);
    v_Float_in : in std_logic_vector(Dac_Bits - 1 downto 0);

    -- Outputs from the core, can be assigned to LUT values
    sat_current_out : out std_logic_vector(Dac_Bits - 1 downto 0);
    temperature_out : out std_logic_vector(Dac_Bits - 1 downto 0);
    v_Float_out : out std_logic_vector(Dac_Bits - 1 downto 0)
  ) ;
end pcr_input_control ; 

architecture rtl of pcr_input_control is
    -- Saves values of LUT into ROM
    constant temperature_profile_rom : LUT_type := temperature_profile_LUT;

    -- Counters to progress the LUT
    signal counter : unsigned(14 downto 0);
    signal LUT_counter : unsigned(DAC_bits - 1 downto 0);
begin
    -- purpose: Provide iSat, Temperature and vFloat outputs
    -- type   : synchronus with reset
    -- inputs : clk, rst, sat_current_in, temperature_in, v_Float_in
    -- outputs: sat_current_out, temperature_out, v_Float_out, data_valid
    PROC_OUPUTS : process(clk)
    -- constant sat_current_start : integer := iSat_guess;
    -- constant temperature_start : integer := Temperature_guess;
    -- constant V_float_start : integer := V_float_guess;
    constant sat_current_start : integer := 713;
    constant temperature_start : integer := 3072;
    constant V_float_start : integer := -4096;
    begin
        if rising_edge(clk) then
            if rst = '1' then
                sat_current_out <= std_logic_vector(to_signed(sat_current_start, sat_current_out'length));
                temperature_out <= std_logic_vector(to_signed(temperature_start, temperature_out'length));
                v_Float_out  <= std_logic_vector(to_signed(V_float_start, v_Float_out'length));
                LUT_counter <= to_unsigned(0, LUT_counter'length);
                counter <= to_unsigned(0, counter'length);
            else
                -- Allows switching between manual control in python and LUT control
                if switch(switch'low) = '0' then
                    counter <= counter + 1;
                    -- Only change the value based on FET sweep cycles
                    -- if counter = total_sweep_cycles then
                    if counter = 5 then
                        counter <= to_unsigned(0, counter'length);
                        sat_current_out <= std_logic_vector(to_signed(sat_current_start, sat_current_out'length));
                        temperature_out <= std_logic_vector(temperature_profile_rom(to_integer(LUT_counter)));
                        -- temperature_out  <= std_logic_vector(to_signed(temperature_start, temperature_out'length));
                        v_Float_out  <= std_logic_vector(to_signed(V_float_start, v_Float_out'length));
                        LUT_counter <= LUT_counter + 1;
                    end if;
                else
                    sat_current_out <= sat_current_in;
                    temperature_out <= temperature_in;
                    v_Float_out <= v_Float_in;
                end if;
            end if;
        end if ;
    end process ; -- PROC_OUPUTS
end architecture ;