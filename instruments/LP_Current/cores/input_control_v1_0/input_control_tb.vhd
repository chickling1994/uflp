----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : Feb 2020
-- 
-- Module Name: data_aquire_tb
-- Project Name: PCR
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Aquisition Calibration
-- 
-- Dependencies: constants, sim_constants, sim_packages, pcr
----------------------------------------------------------------------------------

library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;
    use std.env.finish;

library pcr;

library packages;
    use packages.constants.all;

library sim_packages;
    use sim_packages.sim_constants.all;
    use sim_packages.sim_subprograms.all;

entity pcr_input_control_tb is
end pcr_input_control_tb ; 

architecture sim of pcr_input_control_tb is
    -- Generic to all cores
    signal clk : std_logic := '1';
    signal rst : std_logic := '1';

    -- Inputs to the core, can be controlled via control registers
    signal switch : std_logic_vector(31 downto 0);
    signal sat_current_in : std_logic_vector(Dac_Bits - 1 downto 0);
    signal temperature_in : std_logic_vector(Dac_Bits - 1 downto 0);
    signal v_Float_in : std_logic_vector(Dac_Bits - 1 downto 0);

    -- Outputs from the core, can be assigned to LUT values
    signal sat_current_out : std_logic_vector(Dac_Bits - 1 downto 0);
    signal temperature_out : std_logic_vector(Dac_Bits - 1 downto 0);
    signal v_Float_out : std_logic_vector(Dac_Bits - 1 downto 0);
    signal data_valid : std_logic;
begin
    -- Generate clock as defined by sim_subprograms and sim_constants
    gen_clock(clk);

    -- Instanciate DUT
    DUT : entity pcr.pcr_input_control(rtl)
    port map (
        -- Generic to all cores
        clk => clk,
        rst => rst,

        -- Inputs to the core, can be controlled via control registers
        switch => switch,
        sat_current_in => sat_current_in,
        temperature_in => temperature_in,
        v_Float_in => v_Float_in,

        -- Outputs from the core, can be assigned to LUT values
        sat_current_out => sat_current_out,
        temperature_out => temperature_out,
        v_Float_out => v_Float_out,
        data_valid => data_valid
    );

    -- When switch is in state '0', use LUT, when '1' use defaults
    PROC_SWITCH : process
    begin
        -- Default switch value
        switch <= (others => '0');
        wait for 2000000*clock_period;
        switch(switch'low) <= '1';
        wait for 100000*clock_period;
    end process ; -- PROC_BIAS

    -- Standard reset process
    PROC_RESET : process
    begin
        wait for 20*clock_period;
        rst <= '0';
        wait until rising_edge(clk);
    end process; -- PROC_RESE

    -- set some standard input parameters
    PROC_PLASMA_PARAMS : process
    begin
        sat_current_in <= std_logic_vector(to_signed(511, sat_current_in'length));
        temperature_in <= std_logic_vector(to_signed(3560, temperature_in'length));
        v_Float_in  <= std_logic_vector(to_signed(-2400, v_Float_in'length));
        wait until rising_edge(clk);
    end process ; -- PROC_PLASMA_PARAMS
end architecture ;