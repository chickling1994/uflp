----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : Feb 2020
-- 
-- Module Name: data_collect_tb
-- Project Name: PCR
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Output to fifo in 32-bit register
-- 
-- Dependencies: constants, sim_constants, sim_packages, pcr
----------------------------------------------------------------------------------

library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;
    use std.env.finish;

library pcr;

library packages;
    use packages.constants.all;

library sim_packages;
    use sim_packages.sim_constants.all;
    use sim_packages.sim_subprograms.all;

entity pcr_data_collect_tb is
end pcr_data_collect_tb ; 

architecture sim of pcr_data_collect_tb is
    -- DUT Inputs
    signal clk : std_logic := '1';
    signal rst : std_logic := '1';
    signal sat_current : std_logic_vector(DAC_Bits - 1 downto 0);
    signal temp : std_logic_vector(DAC_Bits - 1 downto 0);
    signal V_Float : std_logic_vector(DAC_Bits - 1 downto 0);

    -- DUT Outputs
    signal data_out : std_logic_vector(31 downto 0);

begin
    -- Generate clock as defined by sim_subprograms and sim_constants
    gen_clock(clk);

    -- Instanciate DUT
    DUT : entity pcr.pcr_data_collect(rtl)
    port map (
        clk => clk ,
        rst => rst ,

        sat_current => sat_current,
        temp => temp,        
        V_Float => V_Float,

        data_out => data_out
    );

    -- Check all values of sat current
    PROC_SAT_CURRENT : process
    begin
        for i in -(2**(DAC_Bits-1)) to 2**(DAC_Bits-1) - 1 loop
            sat_current <= std_logic_vector(to_signed(i, sat_current'length));
            wait until rising_edge(clk);
        end loop;
        finish;
    end process ; -- PROC_BIAS

    -- Check all values of Temp
    PROC_TEMP : process
    begin
        for i in 0 to 2**(DAC_Bits-1) loop
            Temp <= std_logic_vector(to_signed(i, Temp'length));
            wait until rising_edge(clk);
        end loop;
    end process ; -- PROC_BIAS

    -- Check all values of V_Float
    PROC_V_FLOAT : process
    begin
        for i in 2**(DAC_Bits-1) - 1 downto -(2**(DAC_Bits-1)) loop
            V_Float <= std_logic_vector(to_signed(i, V_Float'length));
            wait until rising_edge(clk);
        end loop;
    end process ; -- PROC_BIAS

    -- Standard reset process
    PROC_RESET : process
    begin
        wait for 20*clock_period;
        rst <= '0';
        wait until rising_edge(clk);
    end process; -- PROC_RESET

end architecture ;