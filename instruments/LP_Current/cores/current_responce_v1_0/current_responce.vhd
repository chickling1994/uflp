----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : September 2019
-- 
-- Module Name: current_responce
-- Project Name: PCR
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Calculates the current from predefined values of:
--              Temp, vFloat, iSat, LP_bias_rx
-- 
-- Dependencies: constants, current_LUT, types
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library packages;
use packages.constants.all;
use packages.types.all;
use packages.current_LUT.all;

entity pcr_current_responce is
  port (
    -- Generic to all cores
    clk : in std_logic;
    rst : in std_logic; -- '1' = in reset

    -- Inputs to the core
    LP_bias_rx : in std_logic_vector(DAC_bits - 1 downto 0);
    sat_current : in std_logic_vector(DAC_Bits - 1 downto 0);
    Temp : in std_logic_vector(DAC_Bits - 1 downto 0);
    V_float : in std_logic_vector(DAC_Bits - 1 downto 0);

    -- Outputs from the core
    LP_bias_tx : out std_logic_vector(DAC_bits - 1 downto 0);
    current_out : out std_logic_vector(DAC_bits - 1 downto 0)
  );
end pcr_current_responce; 

architecture rtl of pcr_current_responce is
  -- LUT related signals
  constant rom : LUT_type := current_LUT;
  signal current_LUT_shift : std_logic_vector(5 downto 0);
  signal current_addr : std_logic_vector(DAC_bits - 1 downto 0);
  signal current_LUT_value : signed(DAC_bits - 1 downto 0);

  -- Division signals
  signal Numerator : std_logic_vector(DAC_Bits - 1 downto 0);
  signal Denominator : std_logic_vector(DAC_Bits - 1 downto 0);  
  signal Division_Valid : std_logic;
  signal Division_Result : std_logic_vector(DAC_Bits - 1 downto 0);

  -- Sampling Signals
  signal V_float_temp : signed(DAC_Bits - 1 downto 0) ;
  signal Te_temp : signed(DAC_Bits - 1 downto 0) ;
  signal sat_current_temp : signed(DAC_Bits - 1 downto 0) ;
  signal delay_counter : unsigned(12 downto 0);
  signal sample_probe : std_logic;
  signal sample_complete : std_logic;
  signal last_bias : signed(DAC_Bits - 1 downto 0);

  -- Arithmatic Signals
  signal LP_bias_temp : signed(DAC_Bits - 1 downto 0) ;
  signal LP_bias_received : signed(DAC_Bits - 1 downto 0) ;
  signal LP_bias_multiplied : signed(2*DAC_Bits - 1 downto 0) ;
  signal LP_bias_shift : signed(2*DAC_Bits - 1 downto 0) ;
  signal LP_bias_added : signed(DAC_Bits - 1 downto 0) ;
  signal temp_divide : signed(DAC_Bits - 1 downto 0);
  signal ans_shift : unsigned(5 downto 0);
  signal temp_multiply : signed(2*DAC_Bits - 1 downto 0);
  signal sub_calc : unsigned(DAC_Bits - 1 downto 0);

begin
  -- purpose: Perform division using Goldschmidt algorithm
  -- type   : synchronus
  -- inputs : clk, rst, Numerator, Denominator
  -- outputs: Division_Valid, Division_Results
  GOLDSCHMIDT_DIVISION : entity packages.gold(rtl)
  PORT MAP(
    clk => clk,
    rst => rst,
    div_complete => Division_Valid,
    Numerator => Numerator,
    Denominator => Denominator,
    Quotient => Division_Result
    );

  -- purpose: Sample the data at the correct point
  -- type   : synchronus
  -- inputs : clk, Current_addr
  -- outputs: current_LUT_value
  PROC_DELAY : process(clk)
  begin
      if rising_edge(clk) then
        if rst = '1' then
            delay_counter <= to_unsigned(0, delay_counter'length);
            sample_probe <= '0';
            last_bias <= (others => '0');
        else
          last_bias <= signed(LP_bias_rx);
          -- Sample when the difference between bias inputs is greater than 100
          if sample_probe = '0' and abs(last_bias - LP_bias_temp) > 100 then
            delay_counter <= delay_counter + 1;
            -- Sample x clock cycles into bias change
            if (delay_counter = bias_on_clock_cycles / 2**2)  then
                sample_probe <= '1';
            end if;
          else
            delay_counter <= to_unsigned(0, delay_counter'length);
            sample_probe <= '0';
          end if;
        end if;
      end if;
  end process; -- PROC_DELAY

  -- purpose: Sample the variables for the core within 1 clock cycle
  -- type   : synchronus with reset
  -- inputs : clk, V_float, LP_bias_rx, Temp
  -- Reqs   : constants
  -- outputs: V_float_temp, LP_bias_temp, Te_temp
  PROC_SAMPLE : process(clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        V_float_temp <= signed(V_float);
        LP_bias_temp <= (others => '0');
        
        Te_temp <= shift_right(signed(Temp), 2);
      else
          -- if sample_probe = '1' then
            V_float_temp  <= signed(V_float); 
            LP_bias_temp  <= signed(LP_bias_rx);
            Te_temp  <= shift_right(signed(Temp), (Temperature_shift - bias_shift));
            sat_current_temp <= signed(sat_current);
          -- end if;
      end if;
    end if;
  end process; -- PROC_SAMPLE

  -- purpose: Output the correct bias offset with the floating potential
  -- type   : synchronus
  -- inputs : clk, V_float, LP_bias_temp
  -- outputs: LP_bias_tx
  PROC_BIAS_TX : process(clk)
  variable offset_bias : signed(DAC_Bits - 1 downto 0);
  begin
    if rising_edge(clk) then
      -- LP_bias_tx <= std_logic_vector(LP_bias_temp + signed(V_float));
      -- LP_bias_tx <= std_logic_vector(LP_bias_temp);
    end if;
  end process; -- PROC_BIAS_OUTPUT

  -- purpose: Finds the correct address in the LUT
  -- type   : synchronus with reset
  -- Reqs   : Goldschmidt
  -- inputs : clk, rst, LP_bias_temp, Te_temp, Numerator, Denominator
  -- outputs: Current_addr
  PROC_ADDR : process(clk)
  begin -- Fix all types so that arithmatic can be done
    if rising_edge(clk) then
      if rst = '1' then
        Current_addr <= std_logic_vector(to_signed(0, Current_addr'length));
        
        Numerator <= std_logic_vector(LP_bias_temp);
        Denominator <= std_logic_vector(Te_temp);
      else
        -- Required inputs to the goldschmidt core
        Numerator <= std_logic_vector(LP_bias_temp - V_float_temp);
        Denominator <= std_logic_vector(Te_temp);

        -- When the division is complete, update the output
        if Division_Valid = '1' then
          temp_divide <= signed(Division_Result);
        end if;

        -- Addition Register
        sub_calc <= unsigned(temp_divide + 2**13);
        
        -- Output address
        Current_addr <= std_logic_vector(sub_calc - 2);
      end if;
    end if;
  end process; -- PROC_ADDR

  -- purpose: Finds the LUT shift associated with the LUT value
  -- type   : synchronus
  -- inputs : clk, Current_addr
  -- outputs: current_LUT_value
  PROC_LUT : process(clk)
  variable LUT_seg : integer;
  begin
    if rising_edge(clk) then
      LUT_seg := to_integer((unsigned(Current_addr) / 2**10)) ;
      case LUT_seg is
        when 9 to 16  =>
            -- Bits either side of the binary point are 2_12
            current_LUT_shift <= std_logic_vector(to_unsigned(2,current_LUT_shift'length));
        when 0 to 7 =>
            -- Bits either side of the binary point are 1_13
            current_LUT_shift <=  std_logic_vector(to_unsigned(13,current_LUT_shift'length));
        when others =>
            -- Bits either side of the binary point are 10_4
            current_LUT_shift <= std_logic_vector(to_unsigned(12,current_LUT_shift'length));
        end case;
      current_LUT_value <= rom(to_integer(unsigned(current_addr)));
    end if;
  end process; -- PROC_LUT

  -- purpose: Calculation the current value
  -- type   : synchronus
  -- inputs : clk, current_LUT_shift, sat_current, current_LUT_value
  -- outputs: current_out
  PROC_CURRENT : process(clk)
  begin
      if rising_edge(clk) then
          ans_shift <= unsigned(current_LUT_shift);
          temp_multiply <= sat_current_temp * current_LUT_value;
          current_out <=  std_logic_vector(to_signed(to_integer(shift_right(temp_multiply, to_integer(ans_shift))), current_out'length)); 
      end if;
  end process; -- PROC_CURRENT

end architecture;