----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : September 2019
-- 
-- Module Name: pcr_current_responce_tb
-- Project Name: PCR
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Calculates the current from predefined values of:
--              Temp, vFloat, iSat, LP_bias_rx
-- 
-- Dependencies: constants, sim_constants, sim_subprograms
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library pcr;

library packages;
use packages.constants.all;

library sim_packages;
use sim_packages.sim_constants.all;
use sim_packages.sim_subprograms.all;

entity pcr_current_responce_tb is
end pcr_current_responce_tb; 

architecture sim of pcr_current_responce_tb is
    -- DUT Inputs
    signal clk : std_logic := '1';
    signal rst : std_logic := '1';

    signal LP_bias_rx : std_logic_vector(DAC_bits - 1 downto 0);
    signal sat_current : std_logic_vector(DAC_bits - 1 downto 0);
    signal Temp : std_logic_vector(DAC_bits - 1 downto 0);
    signal V_float : std_logic_vector(DAC_bits - 1 downto 0);

    -- DUT Outputs
    signal current_out : std_logic_vector(DAC_bits - 1 downto 0);
    signal LP_bias_tx : std_logic_vector(DAC_bits - 1 downto 0);

    -- DUT Signals
    signal Temp_Register : signed(DAC_bits - 1 downto 0);
begin
    -- Generate clock as defined by sim_subprograms and sim_constants
    gen_clock(clk);
    
    -- Instanciate DUT
    DUT : entity pcr.pcr_current_responce(rtl)
    port map (
        clk => clk ,
        rst => rst ,
        
        LP_bias_rx => LP_bias_rx,
        sat_current => sat_current,
        Temp => Temp,
        V_float => V_float,

        LP_bias_tx => LP_bias_tx,
        current_out => current_out
    );
    
    -- Define constants for the plasma parameters
    PROC_PLASMA_PARAMS : process
    begin
        sat_current <= std_logic_vector(to_signed(1000,Temp'length));
        Temp <= std_logic_vector(to_signed(3600,Temp'length));
        V_float <= std_logic_vector(to_signed(-2400,Temp'length));
        wait until rising_edge(clk);
    end process ; -- PROC_PLASMA_PARAMS

    -- Convert the temperature to a bias voltage
    PROC_TEMPERATURE : process
    begin
        Temp_Register <= shift_right(signed(Temp), (Temperature_shift - bias_shift));
        wait until rising_edge(clk);
    end process ; -- PROC_TEMPERATURE

    -- Apply the 3 bias states with a predefined clock cycle gap
    PROC_BIAS : process
    begin
        LP_bias_rx <= std_logic_vector(to_signed(to_integer(-3 * Temp_Register),LP_bias_rx'length));
        wait for 42*clock_period;
        LP_bias_rx <= std_logic_vector(to_signed(to_integer(1 * Temp_Register),LP_bias_rx'length));
        wait for 42*clock_period;
        LP_bias_rx <= std_logic_vector(to_signed(to_integer(0 * Temp_Register) ,LP_bias_rx'length));
        wait for 42*clock_period;
    end process; -- PROC_BIAS

    -- Standard reset process
    PROC_RESET : process
    begin
        wait for 2*clock_period;
        rst <= '0';
        wait for 10000*clock_period;
    end process; -- PROC_RESET
end architecture;