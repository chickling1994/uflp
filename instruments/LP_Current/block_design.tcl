source $board_path/config/ports.tcl

# Add PS and AXI Interconnect
set board_preset $board_path/config/board_preset.tcl
source $sdk_path/fpga/lib/starting_point.tcl

# Add ADCs and DACs
source $sdk_path/fpga/lib/redp_adc_dac.tcl
set adc_dac_name adc_dac
add_redp_adc_dac $adc_dac_name

# Add processor system reset synchronous to adc clock
set adc_clk $adc_dac_name/adc_clk
set rst_adc_clk_name proc_sys_reset_adc_clk

cell xilinx.com:ip:proc_sys_reset:5.0 $rst_adc_clk_name {} {
  ext_reset_in $ps_name/FCLK_RESET0_N
  slowest_sync_clk $adc_clk
}

# Add config and status registers
source $sdk_path/fpga/lib/ctl_sts.tcl
add_ctl_sts $adc_clk $rst_adc_clk_name/peripheral_aresetn

# Connect LEDs
connect_port_pin led_o [get_slice_pin [ctl_pin led] 7 0]

# Connect DAC to config and ADC to status
for {set i 0} {$i < [get_parameter n_adc]} {incr i} {
  # connect_pins [ctl_pin dac$i] adc_dac/dac[expr $i+1]
  connect_pins [sts_pin adc$i] adc_dac/adc[expr $i+1]
}

# Use AXI Stream clock converter (ADC clock -> FPGA clock)
set intercon_idx 0
set idx [add_master_interface $intercon_idx]
cell xilinx.com:ip:axis_clock_converter:1.1 adc_clock_converter {
  TDATA_NUM_BYTES 4
} {
  s_axis_aresetn $rst_adc_clk_name/peripheral_aresetn
  m_axis_aresetn [set rst${intercon_idx}_name]/peripheral_aresetn
  s_axis_aclk $adc_clk
  m_axis_aclk [set ps_clk$intercon_idx]
}

# Add AXI stream FIFO to read pulse data from the PS
cell xilinx.com:ip:axi_fifo_mm_s:4.1 adc_axis_fifo {
  C_USE_TX_DATA 0
  C_USE_TX_CTRL 0
  C_USE_RX_CUT_THROUGH true
  C_RX_FIFO_DEPTH 32768
  C_RX_FIFO_PF_THRESHOLD 32763
} {
  s_axi_aclk [set ps_clk$intercon_idx]
  s_axi_aresetn [set rst${intercon_idx}_name]/peripheral_aresetn
  S_AXI [set interconnect_${intercon_idx}_name]/M${idx}_AXI
  AXI_STR_RXD adc_clock_converter/M_AXIS
}

assign_bd_address [get_bd_addr_segs adc_axis_fifo/S_AXI/Mem0]
set memory_segment  [get_bd_addr_segs /${::ps_name}/Data/SEG_adc_axis_fifo_Mem0]
set_property offset [get_memory_offset adc_fifo] $memory_segment
set_property range  [get_memory_range adc_fifo]  $memory_segment


####################################################################################################
# Instantiating  all the needed blocks and ports
create_bd_cell -type ip -vlnv UOL:user:top_pcr:1.0 top_pcr
connect_bd_net [get_bd_pins top_pcr/clk] [get_bd_pins adc_dac/adc_clk] 

connect_bd_net [get_bd_pins top_pcr/bias_in] [get_bd_pins adc_dac/adc2]
# connect_bd_net [get_bd_pins top_pcr/bias_out] [get_bd_pins adc_dac/dac1]
connect_bd_net [get_bd_pins top_pcr/current_out_dac] [get_bd_pins adc_dac/dac1]
connect_bd_net [get_bd_pins top_pcr/bias_out_dac] [get_bd_pins adc_dac/dac2]

connect_bd_net [get_bd_pins top_pcr/sat_current_in] [get_bd_pins ctl/ISat]
connect_bd_net [get_bd_pins top_pcr/temperature_in] [get_bd_pins ctl/Temperature]
connect_bd_net [get_bd_pins top_pcr/Switch] [get_bd_pins ctl/Switch]
connect_bd_net [get_bd_pins top_pcr/v_float_in] [get_bd_pins ctl/VFloating]

create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 Current_concat
set_property -dict [list CONFIG.IN0_WIDTH {14} CONFIG.IN1_WIDTH {18}] [get_bd_cells Current_concat]
create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 Bias_concat
set_property -dict [list CONFIG.IN0_WIDTH {14} CONFIG.IN1_WIDTH {18}] [get_bd_cells Bias_concat]

connect_bd_net [get_bd_pins top_pcr/current_out_dac] [get_bd_pins Current_concat/In0]
connect_bd_net [get_bd_pins top_pcr/bias_out_dac] [get_bd_pins Bias_concat/In0]

connect_bd_net [get_bd_pins Current_concat/dout] [get_bd_pins sts/Current]
connect_bd_net [get_bd_pins Bias_concat/dout] [get_bd_pins sts/Bias]

# connect_bd_net [get_bd_pins top_pcr/rst_button] [get_bd_pins proc_sys_reset_adc_clk/mb_reset]

# create_bd_port -dir I Ext_trigger
# connect_bd_net [get_bd_ports Ext_trigger] [get_bd_pins top_pcr/rst_button]
connect_bd_net [get_bd_pins ctl/Reset] [get_bd_pins top_pcr/rst_button]
connect_bd_net [get_bd_pins ctl/Reset] [get_bd_pins sts/get_Reset] -boundary_type upper

connect_bd_intf_net [get_bd_intf_pins top_pcr/interface_axis] [get_bd_intf_pins adc_clock_converter/S_AXIS]
connect_bd_net [get_bd_pins ctl/Trigger] [get_bd_pins top_pcr/trigger]
connect_bd_net [get_bd_pins ctl/Acquisition_length] [get_bd_pins top_pcr/acquisition_length]

validate_bd_design


