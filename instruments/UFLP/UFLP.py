#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import math
import numpy as np

from koheron import command

class UFLP(object):
    def __init__(self, client):
        self.client = client
        # self.n_pts = 16384
        self.n_pts = 8192
        self.fs = 125e6 # sampling frequency (Hz)

        self.adc = np.zeros((2, self.n_pts))
        self.dac = np.zeros((2, self.n_pts))


    @command()
    def set_trigger(self):
        pass

    @command()
    def set_output(self):
        pass
    
    @command()
    def set_led(self, led):
        pass

    @command()
    def set_capacitors(self, cap_bank_control):
        pass

    @command()
    def set_cap_manual(self, cap_manual):
        pass

    @command()
    def set_Reset(self):
        pass

    @command()
    def set_normal_data(self, Normal_Data_Val):
        pass
    
    @command()
    def get_Temperature(self):
        return self.client.recv_uint32()

    @command()
    def get_Isaturation(self):
        return self.client.recv_uint32()

    @command()
    def get_vFloat(self):
        return self.client.recv_uint32()   

    @command()
    def set_acquisition_length(self, acquisition_length, input_frequency):
        pass

    @command()
    def set_sample_data_clock_cycles(self, sample_data_clock_cycles):
        pass

    @command()
    def set_bias_on_clock_cycles(self, bias_on_clock_cycles):
        pass

    @command()
    def set_voltage_scale(self, voltage_scale):
        pass
    @command()
    def set_voltage_offset(self, voltage_offset):
        pass
    @command()
    def get_voltage_scale(self):
        return self.client.recv_uint32()  
    @command()
    def get_voltage_offset(self):
        return self.client.recv_uint32()  
    @command()
    def set_current_scale(self, current_scale):
        pass
    @command()
    def set_current_offset(self, current_offset):
        pass

    @command()
    def set_bias_scale(self, bias_scale):
        pass
    @command()
    def set_bias_offset(self, bias_offset):
        pass
    @command()
    def set_output_scale(self, output_scale):
        pass
    @command()
    def set_output_offset(self, output_offset):
        pass

    @command()
    def get_acquisition_length(self):
        return self.client.recv_uint32()  

    @command()
    def get_sample_data_clock_cycles(self):
        return self.client.recv_uint32()  

    @command()
    def get_UFLP_data(self):
        return self.client.recv_vector(dtype='uint32')

    @command()
    def get_buffer_length(self):
        return self.client.recv_uint32()

    @command()
    def get_Reset(self):
        return self.client.recv_uint32()

    @command()
    def reset_fifo(self):
        pass

