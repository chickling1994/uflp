import csv
import numpy as np
import matplotlib.pyplot as plt
from tkinter import filedialog
from tkinter import *
from matplotlib.mlab import stineman_interp
from scipy.signal import argrelextrema
from scipy.optimize import curve_fit
from scipy.stats import mode
from prettytable import PrettyTable

def format_e(n):
    a = '%E' % n
    return a.split('E')[0].rstrip('0').rstrip('.') + 'E' + a.split('E')[1]

def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

root = Tk()
root.filename =  filedialog.askopenfilename(initialdir = "C:\\Users\\chick\\Desktop\\chickling\\",title = "Select file",filetypes = (("csv files","*.csv"),("all files","*.*")))
#root.filename =  filedialog.askopenfilename(initialdir = "C:\\Users\\chick",title = "Select file",filetypes = (("csv files","*.csv"),("all files","*.*")))
filename = root.filename
root.destroy()
print(filename)

S1 = np.hsplit(np.genfromtxt(filename,delimiter=',',usecols=(0,1,2,3,4),skip_header=15,skip_footer=1),5)

S1_V1 = S1[1]
S1_V2 = S1[2]
S1_V3 = S1[3]
S1_V4 = S1[4]

root = Tk()
root.filename =  filedialog.askopenfilename(initialdir = "C:\\Users\\chick\\Desktop\\chickling\\",title = "Select file",filetypes = (("csv files","*.csv"),("all files","*.*")))
#root.filename =  filedialog.askopenfilename(initialdir = "C:\\Users\\chick",title = "Select file",filetypes = (("csv files","*.csv"),("all files","*.*")))
filename = root.filename
root.destroy()
print(filename)
S2 = np.hsplit(np.genfromtxt(filename,delimiter=',',usecols=(0,1,2,3),skip_header=15),4)

S2_V1 = S2[1]
S2_V2 = S2[2]
S2_V3 = S2[3]
if (np.round(np.mean(S1[1]),1) == np.round(mode(S1[1])[0],1) and np.round(np.mean(S1[2]),1) == np.round(mode(S1[2])[0],1) and np.round(np.mean(S1[3]),1) == np.round(mode(S1[3])[0],1) and np.round(np.mean(S1[4]),1) == np.round(mode(S1[4])[0],1) and np.round(np.mean(S1[1]),1) == np.round(mode(S1[1])[0],1) and np.round(np.mean(S1[2]),1) == np.round(mode(S1[2])[0],1) and np.round(np.mean(S1[3]),1) == np.round(mode(S1[3])[0],1)):
	print("All values are equal, no shift requires")
	S1_T = S1[0]
	S2_T = S2[0]
else:
	print("Shifted to align HiPIMPS Pulse")
	S1_T = S1[0] - S1[0][S1[1].argmin()]
	S2_T = S2[0] - S2[0][S2[1].argmax()] - S2[0][S2[1].argmin()]

plt.figure("All Data")
for i in range(0,4):
	plt.plot(S1_T, S1[i+1], label="Scope 1 Channel "+str(i+1))
for i in range(0,3):
	plt.plot(S2_T, S2[i+1], label="Scope 2 Channel "+str(i+1))


plt.legend()
plt.xlabel("Time, s")
plt.ylabel("Voltage, V")

fig = plt.figure("Electron Temperature")
ax1 = fig.add_subplot(111)

T_e = (S2_V3 - S2_V2)/np.log(2)

color = 'tab:red'
ax1.set_xlabel("Time, s")
ax1.set_ylabel("Voltage, V", color=color)
lns1 = ax1.plot(S1_T, S1[1],color=color,label = "HiPIMPS")
ax1.tick_params(axis='y', labelcolor=color)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
color = 'tab:blue'
ax2.set_ylabel("Electron Temperature, eV")  # we already handled the x-label with ax1
lns2 = ax2.plot(S2_T, T_e, color=color,label = "Electron Temperature")
ax2.tick_params(axis='y', labelcolor=color)
#ax2.set_ylim(bottom=0)

lns = lns1+lns2
labs = [l.get_label() for l in lns]
ax1.legend(lns, labs)

fig.tight_layout()  # otherwise the right y-label is slightly clipped

plt.figure("Current")
current = np.abs(S1_V3 - S1_V4)/220
plt.plot(S1_T, current)
plt.xlabel("Time, s")
plt.ylabel("Current, A")

probe_radius = 0.05E-3
probe_circ = probe_radius * 2.0 * np.pi
probe_length = 10.0E-3
probe_area = probe_circ * probe_length

density = current / (0.61 * probe_area * 1.6E-19 * np.sqrt((1.38E-23 * T_e * 11600)/(40 * 1.67E-27)))

fig = plt.figure("Density / Electron Temperature")
ax1 = fig.add_subplot(111)

color = 'tab:red'
ax1.set_xlabel("Time, s")
ax1.set_ylabel("Electron Temperature, eV", color=color)
lns1 = ax1.plot(S2_T, T_e, color=color,label = "Electron Temperature")
ax1.tick_params(axis='y', labelcolor=color)
ax1.set_ylim(bottom=0)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
color = 'tab:blue'
ax2.set_ylabel("Density, $m^{-3}$", color=color)  # we already handled the x-label with ax1
lns2 = ax2.plot(S2_T, density, color=color,label = "Density")
ax2.tick_params(axis='y', labelcolor=color)
ax2.set_ylim(bottom=0)

lns = lns1+lns2
labs = [l.get_label() for l in lns]
ax1.legend(lns, labs)

fig.tight_layout()  # otherwise the right y-label is slightly clipped

plt.show()

'''
x = PrettyTable()

x.field_names = ["Scope", "Position", "Voltage"]
x.add_row(["1", 1, S1_V1])
x.add_row(["1", 2, S1_V2])
x.add_row(["1", 3, S1_V3])
x.add_row(["1", 4, S1_V4])
x.add_row(["2", 1, S2_V1])
x.add_row(["2", 2, S2_V2])
x.add_row(["2", 3, S2_V3])

print(x)
'''	
