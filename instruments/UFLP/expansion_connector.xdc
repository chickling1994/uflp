### Pin outs for expansion connector

set_property IOSTANDARD LVCMOS33 [get_ports Ext_trigger]
set_property IOSTANDARD LVCMOS33 [get_ports ext_debug_led_2]
set_property IOSTANDARD LVCMOS33 [get_ports ext_debug_led_3]
set_property IOSTANDARD LVCMOS33 [get_ports Cap_*]

set_property PACKAGE_PIN G18 [get_ports Ext_trigger]
set_property PACKAGE_PIN J16 [get_ports ext_debug_led_2]
set_property PACKAGE_PIN M15 [get_ports ext_debug_led_3]


set_property PACKAGE_PIN G17 [get_ports Cap_0]
set_property PACKAGE_PIN H16 [get_ports Cap_1]
set_property PACKAGE_PIN J18 [get_ports Cap_2]
set_property PACKAGE_PIN K17 [get_ports Cap_3]
set_property PACKAGE_PIN L14 [get_ports Cap_4]
set_property PACKAGE_PIN L16 [get_ports Cap_5]
set_property PACKAGE_PIN K16 [get_ports Cap_6]
set_property PACKAGE_PIN M14 [get_ports Cap_7]

