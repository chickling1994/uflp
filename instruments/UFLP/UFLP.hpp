/// UFLP driver
///
/// (c) Koheron

#ifndef __DRIVERS_UFLP_HPP__
#define __DRIVERS_UFLP_HPP__

#include <atomic>
#include <thread>
#include <chrono>

#include <context.hpp>

// http://www.xilinx.com/support/documentation/ip_documentation/axi_fifo_mm_s/v4_1/pg080-axi-fifo-mm-s.pdf
namespace Fifo_regs {
  constexpr uint32_t rdfr = 0x18;
  constexpr uint32_t rdfo = 0x1C;
  constexpr uint32_t rdfd = 0x20;
  constexpr uint32_t rlr = 0x24;

}
//constexpr uint32_t dac_size = mem::dac_range/sizeof(uint32_t);
constexpr uint32_t adc_buff_size = 16777216;

class UFLP {
public:
  bool Rst_Val = 0;
  
  UFLP(Context& ctx_)
    : ctx(ctx_)
    , ctl(ctx.mm.get<mem::control>())
    , sts(ctx.mm.get<mem::status>())
    , adc_fifo_map(ctx.mm.get<mem::adc_fifo>())
    , adc_data(adc_buff_size)
  {
    start_fifo_acquisition();
  }
  
  //  UFLP
  void set_led(uint32_t led) {
    ctl.write<reg::led>(led);
  }

  void set_acquisition_length(float AcqTime, float Freq) {
    Time_conversion = Freq / AcqTime;
    ctl.write<reg::Acquisition_length>(uint32_t(Time_conversion));
  }

  void set_bias_on_clock_cycles(float bias_on_clock_cycles) {
    ctl.write<reg::set_bias_on_clock_cycles>(uint32_t(bias_on_clock_cycles));
  }

  void set_sample_data_clock_cycles(float sample_data_clock_cycles) {
    ctl.write<reg::set_sample_data_clock_cycles>(uint32_t(sample_data_clock_cycles));
  }

  void set_capacitors(uint32_t cap_bank_control) {
    ctl.write<reg::cap_bank_control>(cap_bank_control);
  }

  void set_cap_manual(uint32_t cap_manual) {
    ctl.write<reg::cap_manual>(cap_manual);
  }

  void set_voltage_scale(float voltage_scale) {
    voltage_scale_conversion = voltage_scale*256;
    ctl.write<reg::set_voltage_scale>(uint32_t(voltage_scale_conversion));
  }

  uint32_t get_voltage_scale() {
    uint32_t voltage_scale_value = sts.read<reg::get_voltage_scale>();
    return voltage_scale_value;
  }

  void set_voltage_offset(float voltage_offset) {
    ctl.write<reg::set_voltage_offset>(uint32_t(voltage_offset));
  }
  uint32_t get_voltage_offset() {
    uint32_t voltage_offset_value = sts.read<reg::get_voltage_offset>();
    return voltage_offset_value;
  }

  void set_current_scale(float current_scale) {
    current_scale_conversion = current_scale * 256;
    ctl.write<reg::set_current_scale>(uint32_t(current_scale_conversion));
  }
  void set_current_offset(float current_offset) {
    ctl.write<reg::set_current_offset>(uint32_t(current_offset));
  }

  void set_bias_scale(float bias_scale) {
    bias_scale_conversion = bias_scale * 256;
    ctl.write<reg::set_bias_scale>(uint32_t(bias_scale_conversion));
  }
  void set_bias_offset(float bias_offset) {
    ctl.write<reg::set_bias_offset>(uint32_t(bias_offset));
  }
  void set_output_scale(float output_scale) {
    ctl.write<reg::set_output_scale>(uint32_t(output_scale*256));
  }

  void set_output_offset(float output_offset) {
    ctl.write<reg::set_output_offset>(uint32_t(output_offset));
  }

  uint32_t get_capacitors() {
    return sts.read<reg::cap_bank_control>();
  }

  uint32_t get_acquisition_length() {
    uint32_t Acquisition_length_value = sts.read<reg::get_Acquisition_length>();
    return Acquisition_length_value;
  }

  uint32_t get_sample_data_clock_cycles() {
    uint32_t sample_data_clock_cycles_value = sts.read<reg::get_sample_data_clock_cycles>();
    return sample_data_clock_cycles_value;
  }
  
  void set_trigger() {
    ctl.set_bit<reg::Trigger, 0>();
    ctl.clear_bit<reg::Trigger, 0>();
  }

  void set_Reset() {
    if (Rst_Val == 0) {
      Rst_Val = 1;
      ctl.write<reg::Reset>(Rst_Val);
    } else{
      Rst_Val = 0;
      ctl.write<reg::Reset>(Rst_Val);
    }
  }

  void set_normal_data(uint32_t Normal_Data_Val) {
    ctl.write<reg::set_normal_data>(Normal_Data_Val);
  }

  void set_output(uint32_t bit_set) {    
    if (bit_set == 0) {
      ctl.write<reg::Trigger>(0);
    } else{
      ctl.write<reg::Trigger>(2);
    }
  }
  
  
  uint32_t get_Temperature() {
    uint32_t Temperature_value = sts.read<reg::Temperature>();
    return Temperature_value;
  }
  
  uint32_t get_Isaturation() {
    uint32_t Isat_value = sts.read<reg::Isaturation>();
    return Isat_value;
  }
  
  uint32_t get_vFloat() {
    uint32_t vFloat_value = sts.read<reg::vFloat>();
    return vFloat_value;
  }

  uint32_t get_Reset() {
    uint32_t Reset_value = sts.read<reg::get_Reset>();
    return Reset_value;
  }
  
  // uint32_t get_Timestamp() {
  //   uint32_t timestamp_value = sts.read<reg::Timestamp>();
  //   return timestamp_value;
  // }
  
  //Adc FIFO
  
  uint32_t get_fifo_occupancy() {
    return adc_fifo_map.read<Fifo_regs::rdfo>();
  }
  
  void reset_fifo() {
    adc_fifo_map.write<Fifo_regs::rdfr>(0x000000A5);
  }
  
  uint32_t read_fifo() {
    return adc_fifo_map.read<Fifo_regs::rdfd>();
  }
  
  uint32_t get_fifo_length() {
    return (adc_fifo_map.read<Fifo_regs::rlr>() & 0x3FFFFF) >> 2;
  }
  
  void wait_for(uint32_t n_pts) {
    do {} while (get_fifo_length() < n_pts);
  }
  
  
  void start_fifo_acquisition();
  
  // Function to return the buffer length
  uint32_t get_buffer_length() {      
    return collected;
  }
  
  // Function to return data
  std::vector<uint32_t>& get_UFLP_data() {
    
    // Will only return a valid array if data is available
    if (dataAvailable) {      
      dataAvailable = false;
      
      return adc_data;
      
    } else {
      return empty_vector;
    }
  }
  
  
private:
  float Time_conversion;
  float voltage_scale_conversion;
  float current_scale_conversion;
  float bias_scale_conversion;
  Context& ctx;
  Memory<mem::control>& ctl;
  Memory<mem::status>& sts;
  Memory<mem::adc_fifo>& adc_fifo_map;
  
  std::vector<uint32_t> adc_data;
  std::vector<uint32_t> empty_vector;
  
  std::atomic<bool> fifo_acquisition_started{false};
  
  std::thread fifo_thread;
  // Member functions
  void fifo_acquisition_thread();
  uint32_t fill_buffer(uint32_t);
  // Member variables
  std::atomic<bool> dataAvailable{false};
  std::atomic<uint32_t> collected{0};         //number of currently collected data
};

inline void UFLP::start_fifo_acquisition() {
  if (! fifo_acquisition_started) {
    fifo_thread = std::thread{&UFLP::fifo_acquisition_thread, this};
    fifo_thread.detach();
  }
}

inline void UFLP::fifo_acquisition_thread() {
  constexpr auto fifo_sleep_for = std::chrono::nanoseconds(5000);
  fifo_acquisition_started = true;
  ctx.log<INFO>("Starting fifo acquisition");
  adc_data.reserve(16777216);
  adc_data.resize(0);
  empty_vector.resize(0);
  
  uint32_t dropped=0;
  
  // While loop to reserve the number of samples needed to be collected
  while (fifo_acquisition_started){
    if (collected == 0){
      // Checking that data has not yet been collected      
      if ((dataAvailable == false) && (adc_data.size() > 0)){
      	// Sleep to avoid a race condition while data is being transferred
      	std::this_thread::sleep_for(fifo_sleep_for);
      	// Clearing vector back to zero
      	adc_data.resize(0);
      	ctx.log<INFO>("vector cleared, adc_data size: %d", adc_data.size());
      }
    }
    
    dropped = fill_buffer(dropped);
    if (dropped > 0){
      ctx.log<INFO>("Dropped samples: %d", dropped);
    }
    // std::this_thread::sleep_for(fifo_sleep_for);
  }// While loop
}

// Member function to fill buffer array
inline uint32_t UFLP::fill_buffer(uint32_t dropped) {
  // Retrieving the number of samples to collect
  uint32_t samples=get_fifo_length();
  
  // Collecting samples in buffer
  if (samples > 0) {
    // Checking for dropped samples
    if (samples >= 32768){  	
      dropped += 1;
    }
    // Assigning data to array
    for (size_t i=0; i < samples; i++){	  
      adc_data.push_back(read_fifo());	  
      collected = collected + 1;
    }
  }
  // if statement for setting the acquisition completed flag
  if (samples == 0) {
    if (collected > 0) {
      dataAvailable = true;
      collected = 0;
      dropped = 0;
    }
  }
  return dropped;
}

#endif // __DRIVERS_UFLP_HPP__
