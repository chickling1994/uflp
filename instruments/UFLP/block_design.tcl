source $board_path/config/ports.tcl

# Add PS and AXI Interconnect
set board_preset $board_path/config/board_preset.tcl
source $sdk_path/fpga/lib/starting_point.tcl

# Add ADCs and DACs
source $sdk_path/fpga/lib/redp_adc_dac.tcl
set adc_dac_name adc_dac
add_redp_adc_dac $adc_dac_name

# Add processor system reset synchronous to adc clock
set adc_clk $adc_dac_name/adc_clk
set rst_adc_clk_name proc_sys_reset_adc_clk

cell xilinx.com:ip:proc_sys_reset:5.0 $rst_adc_clk_name {} {
  ext_reset_in $ps_name/FCLK_RESET0_N
  slowest_sync_clk $adc_clk
}

# Add config and status registers
source $sdk_path/fpga/lib/ctl_sts.tcl
add_ctl_sts $adc_clk $rst_adc_clk_name/peripheral_aresetn

# Connect LEDs
connect_port_pin led_o [get_slice_pin [ctl_pin led] 7 0]

# Connect DAC to config and ADC to status
for {set i 0} {$i < [get_parameter n_adc]} {incr i} {
  # connect_pins [ctl_pin dac$i] adc_dac/dac[expr $i+1]
  connect_pins [sts_pin adc$i] adc_dac/adc[expr $i+1]
}

# Use AXI Stream clock converter (ADC clock -> FPGA clock)
set intercon_idx 0
set idx [add_master_interface $intercon_idx]
cell xilinx.com:ip:axis_clock_converter:1.1 adc_clock_converter {
  TDATA_NUM_BYTES 4
} {
  s_axis_aresetn $rst_adc_clk_name/peripheral_aresetn
  m_axis_aresetn [set rst${intercon_idx}_name]/peripheral_aresetn
  s_axis_aclk $adc_clk
  m_axis_aclk [set ps_clk$intercon_idx]
}

# Add AXI stream FIFO to read pulse data from the PS
cell xilinx.com:ip:axi_fifo_mm_s:4.1 adc_axis_fifo {
  C_USE_TX_DATA 0
  C_USE_TX_CTRL 0
  C_USE_RX_CUT_THROUGH true
  C_RX_FIFO_DEPTH 32768
  C_RX_FIFO_PF_THRESHOLD 32763
} {
  s_axi_aclk [set ps_clk$intercon_idx]
  s_axi_aresetn [set rst${intercon_idx}_name]/peripheral_aresetn
  S_AXI [set interconnect_${intercon_idx}_name]/M${idx}_AXI
  AXI_STR_RXD adc_clock_converter/M_AXIS
}

assign_bd_address [get_bd_addr_segs adc_axis_fifo/S_AXI/Mem0]
set memory_segment  [get_bd_addr_segs /${::ps_name}/Data/SEG_adc_axis_fifo_Mem0]
set_property offset [get_memory_offset adc_fifo] $memory_segment
set_property range  [get_memory_range adc_fifo]  $memory_segment


####################################################################################################
# Actual UFLP nets for instantiation
create_bd_cell -type ip -vlnv UOL:user:top_mlp:1.0 top_mlp
connect_bd_net [get_bd_pins top_mlp/clk] [get_bd_pins adc_dac/adc_clk]

connect_bd_net [get_bd_pins top_mlp/voltage_in] [get_bd_pins adc_dac/adc1]
# connect_bd_net [get_bd_pins top_mlp/voltage_in] [get_bd_pins top_mlp/bias_out_dac]
connect_bd_net [get_bd_pins top_mlp/current_in] [get_bd_pins adc_dac/adc2]
connect_bd_net [get_bd_pins top_mlp/output_dac] [get_bd_pins adc_dac/dac1]
connect_bd_net [get_bd_pins top_mlp/bias_out_dac] [get_bd_pins adc_dac/dac2]

# connect_bd_net [get_bd_pins proc_sys_reset_adc_clk/mb_reset] [get_bd_pins top_mlp/rst_button]
create_bd_port -dir O ext_debug_led_2
connect_bd_net [get_bd_ports ext_debug_led_2] [get_bd_pins top_mlp/ext_debug_led_2]
create_bd_port -dir O ext_debug_led_3
create_bd_port -dir I Ext_trigger
# connect_bd_net [get_bd_ports Ext_trigger] [get_bd_pins top_mlp/rst_button]

startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 Reset_Or_Logic
endgroup
set_property -dict [list CONFIG.C_SIZE {1} CONFIG.C_OPERATION {or} CONFIG.LOGO_FILE {data/sym_orgate.png}] [get_bd_cells Reset_Or_Logic]
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 Reset_slice_0
endgroup
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 not_Reset
endgroup
set_property -dict [list CONFIG.C_SIZE {1} CONFIG.C_OPERATION {not} CONFIG.LOGO_FILE {data/sym_notgate.png}] [get_bd_cells not_Reset]
connect_bd_net [get_bd_ports Ext_trigger] [get_bd_pins Reset_Or_Logic/Op1]
# connect_bd_net [get_bd_pins ctl/Reset] [get_bd_pins Reset_slice_0/Din]
connect_bd_net [get_bd_pins ctl/Reset] [get_bd_pins top_mlp/rst_button]
connect_bd_net [get_bd_pins ctl/set_normal_data] [get_bd_pins top_mlp/Normal_Data]
connect_bd_net [get_bd_pins ctl/Reset] [get_bd_pins sts/get_Reset]
connect_bd_net [get_bd_pins Reset_slice_0/Dout] [get_bd_pins not_Reset/Op1]
connect_bd_net [get_bd_pins not_Reset/Res] [get_bd_pins Reset_Or_Logic/Op2]
# connect_bd_net [get_bd_pins Reset_Or_Logic/Res] [get_bd_pins top_mlp/rst_button]
# connect_bd_net [get_bd_pins Reset_Or_Logic/Res] [get_bd_pins sts/get_Reset] -boundary_type upper


create_bd_port -dir O Cap_0
connect_bd_net [get_bd_ports Cap_0] [get_bd_pins top_mlp/GPIO_Cap_0]
create_bd_port -dir O Cap_1
connect_bd_net [get_bd_ports Cap_1] [get_bd_pins top_mlp/GPIO_Cap_1]
create_bd_port -dir O Cap_2
connect_bd_net [get_bd_ports Cap_2] [get_bd_pins top_mlp/GPIO_Cap_2]
create_bd_port -dir O Cap_3
connect_bd_net [get_bd_ports Cap_3] [get_bd_pins top_mlp/GPIO_Cap_3]
create_bd_port -dir O Cap_4
connect_bd_net [get_bd_ports Cap_4] [get_bd_pins top_mlp/GPIO_Cap_4]
create_bd_port -dir O Cap_5
connect_bd_net [get_bd_ports Cap_5] [get_bd_pins top_mlp/GPIO_Cap_5]
create_bd_port -dir O Cap_6
connect_bd_net [get_bd_ports Cap_6] [get_bd_pins top_mlp/GPIO_Cap_6]
create_bd_port -dir O Cap_7
connect_bd_net [get_bd_ports Cap_7] [get_bd_pins top_mlp/GPIO_Cap_7]

#####################################################################################################


# create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 module_const
# set_property -dict [list CONFIG.CONST_WIDTH {18} CONFIG.CONST_VAL {0}] [get_bd_cells module_const]

create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 iSat_concat
set_property -dict [list CONFIG.IN0_WIDTH {14} CONFIG.IN1_WIDTH {18}] [get_bd_cells iSat_concat]
create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 vFloat_concat
set_property -dict [list CONFIG.IN0_WIDTH {14} CONFIG.IN1_WIDTH {18}] [get_bd_cells vFloat_concat]
create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 Temperature_concat
set_property -dict [list CONFIG.IN0_WIDTH {14} CONFIG.IN1_WIDTH {18}] [get_bd_cells Temperature_concat]

# connect_bd_net [get_bd_pins iSat_concat/dout] [get_bd_pins sts/Isaturation]
# connect_bd_net [get_bd_pins vFloat_concat/dout] [get_bd_pins sts/vFloat]
# connect_bd_net [get_bd_pins Temperature_concat/dout] [get_bd_pins sts/Temperature]
connect_bd_net [get_bd_pins top_mlp/timestamp] [get_bd_pins iSat_concat/In1]
connect_bd_net [get_bd_pins top_mlp/timestamp] [get_bd_pins vFloat_concat/In1]
connect_bd_net [get_bd_pins top_mlp/timestamp] [get_bd_pins Temperature_concat/In1]
connect_bd_net [get_bd_pins iSat_concat/dout] [get_bd_pins sts/Isaturation]
connect_bd_net [get_bd_pins vFloat_concat/dout] [get_bd_pins sts/vFloat]
connect_bd_net [get_bd_pins Temperature_concat/dout] [get_bd_pins sts/Temperature]

connect_bd_net [get_bd_pins top_mlp/sat_current_out] [get_bd_pins iSat_concat/In0]
connect_bd_net [get_bd_pins top_mlp/V_float_out] [get_bd_pins vFloat_concat/In0]
connect_bd_net [get_bd_pins top_mlp/Temperature_out] [get_bd_pins Temperature_concat/In0]

connect_bd_net [get_bd_pins ctl/Trigger] [get_bd_pins top_mlp/trigger]
connect_bd_net [get_bd_pins ctl/Acquisition_length] [get_bd_pins top_mlp/acquisition_length]
# connect_bd_net [get_bd_ports ext_debug_led_3] [get_bd_pins ctl/Trigger]
connect_bd_net [get_bd_pins ctl/Acquisition_length] [get_bd_pins sts/get_Acquisition_length] -boundary_type upper

# create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 temperature_const
# set_property -dict [list CONFIG.CONST_WIDTH {14} CONFIG.CONST_VAL {1}] [get_bd_cells temperature_const]
# connect_bd_net [get_bd_pins temperature_const/dout] [get_bd_pins Temperature_concat/In0]

# connect_bd_net [get_bd_pins module_const/dout] [get_bd_pins iSat_concat/In1]
# connect_bd_net [get_bd_pins module_const/dout] [get_bd_pins Temperature_concat/In1]
# connect_bd_net [get_bd_pins module_const/dout] [get_bd_pins vFloat_concat/In1]



# create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 data_out_concat
# set_property -dict [list CONFIG.NUM_PORTS {3} CONFIG.IN1_WIDTH {1}] [get_bd_cells data_out_concat]
# set_property -dict [list CONFIG.IN0_WIDTH {12} CONFIG.IN1_WIDTH {12} CONFIG.IN2_WIDTH {8}] [get_bd_cells data_out_concat]

# connect_bd_net [get_bd_pins top_mlp/sat_current_out] [get_bd_pins data_out_concat/In0]
# connect_bd_net [get_bd_pins top_mlp/Temperature_out] [get_bd_pins data_out_concat/In1]
# connect_bd_net [get_bd_pins top_mlp/V_float_out] [get_bd_pins data_out_concat/In2]
# connect_bd_net [get_bd_pins data_out_concat/dout] [get_bd_pins adc_clock_converter/s_axis_tdata]

connect_bd_net [get_bd_pins ctl/set_voltage_scale] [get_bd_pins top_mlp/voltage_scale]
connect_bd_net [get_bd_pins ctl/set_voltage_offset] [get_bd_pins top_mlp/voltage_offset]
connect_bd_net [get_bd_pins ctl/set_current_scale] [get_bd_pins top_mlp/current_scale]
connect_bd_net [get_bd_pins ctl/set_current_offset] [get_bd_pins top_mlp/current_offset]
connect_bd_net [get_bd_pins ctl/set_bias_scale] [get_bd_pins top_mlp/bias_scale]
connect_bd_net [get_bd_pins ctl/set_bias_offset] [get_bd_pins top_mlp/bias_offset]
connect_bd_net [get_bd_pins ctl/set_output_scale] [get_bd_pins top_mlp/output_scale]
connect_bd_net [get_bd_pins ctl/set_output_offset] [get_bd_pins top_mlp/output_offset]

connect_bd_net [get_bd_pins ctl/set_voltage_scale] [get_bd_pins sts/get_voltage_scale]
connect_bd_net [get_bd_pins ctl/set_voltage_offset] [get_bd_pins sts/get_voltage_offset]

connect_bd_net [get_bd_pins ctl/set_sample_data_clock_cycles] [get_bd_pins sts/get_sample_data_clock_cycles] -boundary_type upper
connect_bd_net [get_bd_pins ctl/set_sample_data_clock_cycles] [get_bd_pins top_mlp/sample_data_clock_cycles_ctl]
connect_bd_net [get_bd_pins ctl/set_bias_on_clock_cycles] [get_bd_pins top_mlp/bias_on_clock_cycles_ctl]

connect_bd_net [get_bd_pins ctl/cap_bank_control] [get_bd_pins top_mlp/cap_selection]
connect_bd_net [get_bd_pins ctl/cap_manual] [get_bd_pins top_mlp/cap_manual_switch]

connect_bd_net [get_bd_pins top_mlp/data_collect_stream] [get_bd_pins adc_clock_converter/s_axis_tdata]
connect_bd_net [get_bd_pins top_mlp/tvalid] [get_bd_pins adc_clock_converter/s_axis_tvalid]



# delete_bd_objs [get_bd_nets ps_0_FCLK_RESET0_N]
# connect_bd_net [get_bd_pins ctl/Reset] [get_bd_pins proc_sys_reset_0/ext_reset_in]
# connect_bd_net [get_bd_pins ctl/Reset] [get_bd_pins proc_sys_reset_adc_clk/ext_reset_in]

validate_bd_design