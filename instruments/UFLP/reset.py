#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import os
import time
import matplotlib.pyplot as plt

from UFLP import UFLP
from koheron import connect

host = os.getenv('HOST', '192.168.1.100')
# host = os.getenv('HOST', '138.253.76.189')
client = connect(host, name='ultra-fast-langmuir-probe')
driver = UFLP(client)

#########################################################
# Function for converting an integer to a signed binary
def int2signed(convInt):
    convInt = int(np.floor(convInt))
    if convInt < 0:
        retBin = '{:032b}'.format(convInt & 0xffffffff)
    else:
        retBin = '{:032b}'.format(convInt)

    print(convInt, retBin)
        
    return retBin
##########################################################

# while True:
#     try:
#         print(driver.get_Reset(), end='\r', flush=True)
#     except KeyboardInterrupt:
#             break