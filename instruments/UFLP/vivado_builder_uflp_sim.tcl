read_vhdl -library packages [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/generic_packages/*/*.v*]
read_vhdl -library packages [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/generic_packages/*.v*]
set_property library sim_packages [get_files [glob  /home/chicklin/workspace/home-working/koheron-sdk/instruments/generic_packages/sim_packages/*.v*]]
read_vhdl -library uflp_sim [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/*/*_tb.v*]
read_vhdl -library uflp [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/*/*.v*]
update_compile_order -fileset sources_1
set_property FILE_TYPE {VHDL 2008} [get_files *_tb.vhd]
update_compile_order -fileset sources_1

# read_vhdl -library packages [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/generic_packages/*/*.v*]
# read_vhdl -library packages [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/generic_packages/*.v*]
# set_property library sim_packages [get_files [glob  /home/chicklin/workspace/home-working/koheron-sdk/instruments/generic_packages/sim_packages/*.v*]]
# read_vhdl -library pcr_sim [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/LP_Current/cores/*/*_tb.v*]
# read_vhdl -library pcr [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/LP_Current/cores/*/*.v*]
# read_vhdl -library uflp_sim [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/*/*_tb.v*]
# read_vhdl -library uflp [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/*/*.v*]
# read_vhdl -library uflp_sim [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/uflp_sim/cores/*/*.v*]
# update_compile_order -fileset sources_1
# set_property top top_uflp_sim_tb [current_fileset]
# update_compile_order -fileset sources_1
# set_property top top_uflp_sim_tb [get_filesets sim_1]
# set_property top_lib uflp_sim [get_filesets sim_1]
# set_property FILE_TYPE {VHDL 2008} [get_files *_tb.vhd]
# update_compile_order -fileset sources_1

read_vhdl -library sim_packages [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/*/sim_*.v*]
read_vhdl -library pcr [glob /home/chicklin/workspace/koheron-sdk/instruments/LP_Current/cores/*/*.v*]
update_compile_order -fileset sources_1
set_property library packages [get_files [glob  /home/chicklin/workspace/koheron-sdk/instruments/LP_Current/cores/packages_v1_0/*_pck.vhd]]
set_property library uflp [get_files [glob  /home/chicklin/workspace/koheron-sdk/instruments/LP_Current/cores/*/reset.vhd]]
set_property FILE_TYPE {VHDL 2008} [get_files *_tb.vhd]
update_compile_order -fileset sources_1

# read_vhdl -library packages [glob /home/chicklin/workspace/koheron-sdk/instruments/UFLP/cores/*/*_pck.v*]
# read_vhdl -library uflp_sim [glob /home/chicklin/workspace/koheron-sdk/instruments/UFLP/cores/*/*_tb.v*]
# read_vhdl -library sim_packages [glob /home/chicklin/workspace/koheron-sdk/instruments/UFLP/cores/*/sim_*.v*]
# read_vhdl -library pcr [glob /home/chicklin/workspace/koheron-sdk/instruments/LP_Current/cores/*/*.v*]
# export_ip_user_files -of_objects  [get_files /home/chicklin/workspace/koheron-sdk/instruments/LP_Current/cores/packages_v1_0/sim_constants_pck.vhd] -no_script -reset -force -quiet
# export_ip_user_files -of_objects  [get_files /home/chicklin/workspace/koheron-sdk/instruments/LP_Current/cores/packages_v1_0/sim_subprograms.vhd] -no_script -reset -force -quiet
# export_ip_user_files -of_objects  [get_files /home/chicklin/workspace/koheron-sdk/instruments/LP_Current/cores/packages_v1_0/goldschmidt_pck.vhd] -no_script -reset -force -quiet
# export_ip_user_files -of_objects  [get_files /home/chicklin/workspace/koheron-sdk/instruments/LP_Current/cores/packages_v1_0/current_LUT_pck.vhd] -no_script -reset -force -quiet
# export_ip_user_files -of_objects  [get_files /home/chicklin/workspace/koheron-sdk/instruments/LP_Current/cores/packages_v1_0/Temperature_profile_pck.vhd] -no_script -reset -force -quiet
# export_ip_user_files -of_objects  [get_files /home/chicklin/workspace/koheron-sdk/instruments/LP_Current/cores/packages_v1_0/constants_pck.vhd] -no_script -reset -force -quiet
# export_ip_user_files -of_objects  [get_files /home/chicklin/workspace/koheron-sdk/instruments/LP_Current/cores/packages_v1_0/types_pck.vhd] -no_script -reset -force -quiet
# read_vhdl -library packages [glob /home/chicklin/workspace/koheron-sdk/instruments/LP_Current/cores/packages_v1_0/Temperature_profile_pck.vhd]
# read_vhdl -library uflp [glob /home/chicklin/workspace/koheron-sdk/instruments/UFLP/cores/*/*.v*]
# read_vhdl -library uflp_sim [glob /home/chicklin/workspace/koheron-sdk/instruments/uflp_sim/cores/*/*.v*]
# update_compile_order -fileset sources_1
# set_property top top_uflp_sim_tb [current_fileset]
# update_compile_order -fileset sources_1
# set_property top top_uflp_sim_tb [get_filesets sim_1]
# set_property top_lib uflp_sim [get_filesets sim_1]
# set_property FILE_TYPE {VHDL 2008} [get_files *_tb.vhd]
# update_compile_order -fileset sources_1

# read_vhdl -library packages [glob /media/2TB/workspace/chicklin/koheron-sdk/instruments/UFLP/cores/*/*_pck.v*]
# read_vhdl -library uflp_sim [glob /media/2TB/workspace/chicklin/koheron-sdk/instruments/UFLP/cores/*/*_tb.v*]
# read_vhdl -library sim_packages [glob /media/2TB/workspace/chicklin/koheron-sdk/instruments/UFLP/cores/*/sim_*.v*]
# read_vhdl -library pcr [glob /media/2TB/workspace/chicklin/koheron-sdk/instruments/LP_Current/cores/*/*_tb.v*]
# export_ip_user_files -of_objects  [get_files /media/2TB/workspace/chicklin/koheron-sdk/instruments/LP_Current/cores/packages_v1_0/sim_constants_pck.vhd] -no_script -reset -force -quiet
# export_ip_user_files -of_objects  [get_files /media/2TB/workspace/chicklin/koheron-sdk/instruments/LP_Current/cores/packages_v1_0/sim_subprograms.vhd] -no_script -reset -force -quiet
# export_ip_user_files -of_objects  [get_files /media/2TB/workspace/chicklin/koheron-sdk/instruments/LP_Current/cores/packages_v1_0/goldschmidt_pck.vhd] -no_script -reset -force -quiet
# export_ip_user_files -of_objects  [get_files /media/2TB/workspace/chicklin/koheron-sdk/instruments/LP_Current/cores/packages_v1_0/current_LUT_pck.vhd] -no_script -reset -force -quiet
# export_ip_user_files -of_objects  [get_files /media/2TB/workspace/chicklin/koheron-sdk/instruments/LP_Current/cores/packages_v1_0/Temperature_profile_pck.vhd] -no_script -reset -force -quiet
# export_ip_user_files -of_objects  [get_files /media/2TB/workspace/chicklin/koheron-sdk/instruments/LP_Current/cores/packages_v1_0/constants_pck.vhd] -no_script -reset -force -quiet
# export_ip_user_files -of_objects  [get_files /media/2TB/workspace/chicklin/koheron-sdk/instruments/LP_Current/cores/packages_v1_0/types_pck.vhd] -no_script -reset -force -quiet
# read_vhdl -library packages [glob /media/2TB/workspace/chicklin/koheron-sdk/instruments/LP_Current/cores/packages_v1_0/Temperature_profile_pck.vhd]
# read_vhdl -library uflp [glob /media/2TB/workspace/chicklin/koheron-sdk/instruments/UFLP/cores/*/*.v*]
# read_vhdl -library uflp_sim [glob /media/2TB/workspace/chicklin/koheron-sdk/instruments/uflp_sim/cores/*/*.v*]
# update_compile_order -fileset sources_1
# set_property top top_uflp_sim_tb [current_fileset]
# update_compile_order -fileset sources_1
# set_property top top_uflp_sim_tb [get_filesets sim_1]
# set_property top_lib uflp_sim [get_filesets sim_1]