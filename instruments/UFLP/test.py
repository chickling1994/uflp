#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import os
import time
import matplotlib.pyplot as plt

from UFLP import UFLP
from koheron import connect

global frequency
global AcqTime
frequency = 81380
AcqTime = 1
# host = os.getenv('HOST', '192.168.1.100')
# host = os.getenv('HOST', '192.168.1.101')
host = os.getenv('HOST', '192.168.1.102')
client = connect(host, name='ultra-fast-langmuir-probe')
driver = UFLP(client)

#########################################################
# Function for converting an integer to a signed binary
def int2signed(convInt):
    convInt = int(np.floor(convInt))
    if convInt < 0:
        retBin = '{:032b}'.format(convInt & 0xffffffff)
    else:
        retBin = '{:032b}'.format(convInt)

    print(convInt, retBin)
        
    return retBin
##########################################################

def run_UFLP(save_bool=True, Normal_Mode=True):
    global frequency
    global AcqTime
    print("Running at " + str(frequency/1000) + "KHz for " + str(AcqTime) + " seconds.")
    if Normal_Mode == True:
        driver.set_normal_data(1)
    else:
        driver.set_normal_data(0)

    buffer_length = []
    i = 0
    if driver.get_Reset() == 0:
        driver.set_Reset()
        time.sleep(1)
        driver.set_Reset()
    else:
        driver.set_Reset()

    # time.sleep(0.01)
    driver.set_trigger()
    # time.sleep(acquisition_length+0.01)
    while True: # driver.get_buffer_length() != 0:
        # try:
        samples = driver.get_buffer_length()
        if samples == 0:
            i = i + 1
            if i == 5000:
                break
        else:
            i = 0
        buffer_length.append(samples)
        # time.sleep(0.2)
        # if samples == 0:
        #     break
        # print(samples)
        # i = i + 1
        # except KeyboardInterrupt:
            # break

    dataArray_uflp = driver.get_UFLP_data()
    print(len(dataArray_uflp))

    if save_bool == True:
        saveStr = 'UFLP_Data/'+save(Normal_Mode)
        print(saveStr)
        np.save(saveStr, dataArray_uflp)




    t = np.arange(0,len(dataArray_uflp))
    t = (AcqTime/len(t)) * t

    Temperature_shift = 11
    iSat_shift = Temperature_shift + 8
    vFloat_shift = Temperature_shift - 2

    y = np.zeros([len(dataArray_uflp), 32])
    sample_length = len(dataArray_uflp)
    for i in range(0,len(dataArray_uflp[0:sample_length])): 
        if (len(bin(dataArray_uflp[i]))) == 34:
            y[i][0:32] = [int(d) for d in str(bin(dataArray_uflp[i]))[2:]]  
            
    if Normal_Mode == True:
        iSat_array = np.zeros(len(dataArray_uflp[0:sample_length])) 
        for i in range(0,len(iSat_array)): 
            for j in range(1,12):
                if j == 1:
                    if int(y[i][j]) == 1:
                        iSat_array[i] = -(2**13)
                elif int(y[i][j]) == 1: 
                    iSat_array[i] = iSat_array[i] + 2**(13-j+1)
            iSat_array[i] = iSat_array[i] / 2**iSat_shift

        Temperature_array = np.zeros(len(dataArray_uflp[0:sample_length])) 
        for i in range(0,len(Temperature_array)): 
            for j in range(12,23): 
                if j == 12:
                    if int(y[i][j]) == 1:
                        Temperature_array[i] = -(2**13)
                elif int(y[i][j]) == 1: 
                    Temperature_array[i] = Temperature_array[i] + 2**(13-j+12) 
            Temperature_array[i] = Temperature_array[i] / 2**Temperature_shift

        vFloat_array = np.zeros(len(dataArray_uflp[0:sample_length])) 
        for i in range(0,len(vFloat_array)): 
            for j in range(23,32): 
                if j == 23:
                    if int(y[i][j]) == 1:
                        vFloat_array[i] = -(2**13)
                elif int(y[i][j]) == 1: 
                    vFloat_array[i] = vFloat_array[i] + 2**(13-j+23)
            vFloat_array[i] = vFloat_array[i] / 2**vFloat_shift

        print('iSat = ', np.round(np.mean(iSat_array),3), "+/- ", np.round(np.std(iSat_array),4))
        print('Temperature = ', np.round(np.mean(Temperature_array),3), "+/- ", np.round(np.std(Temperature_array),2))
        print('vFloat = ', np.round(np.mean(vFloat_array),3), "+/- ", np.round(np.std(vFloat_array),2))

        plt.figure("Temperature")
        plt.plot(t[:sample_length], Temperature_array,'o')
        plt.xlabel("Time, s")
        plt.ylabel("Temperature, eV")

        plt.figure("iSat")
        plt.plot(t[:sample_length], iSat_array,'o')
        plt.xlabel("Time, s")
        plt.ylabel("Ion Saturation Current, A")

        plt.figure("vFloat")
        plt.plot(t[:sample_length], vFloat_array,'o')
        plt.xlabel("Time, s")
        plt.ylabel("Floating Potential, V")
    else:
        Current_array = np.zeros(len(dataArray_uflp[0:sample_length]))
        Start_Pos = 4
        for i in range(0,len(Current_array)): 
            for j in range(Start_Pos,18):
                if j == Start_Pos:
                    if int(y[i][j]) == 1:
                        Current_array[i] = -(2**13)
                elif int(y[i][j]) == 1: 
                    Current_array[i] = Current_array[i] + 2**(13-j+Start_Pos)
            Current_array[i] = Current_array[i] / 2**15

        Bias_array = np.zeros(len(dataArray_uflp[0:sample_length])) 
        for i in range(0,len(Bias_array)):
            for j in range(18,32):
                if j == 18:
                    if int(y[i][j]) == 1:
                        Bias_array[i] = -(2**13)
                elif int(y[i][j]) == 1: 
                    Bias_array[i] = Bias_array[i] + 2**(13-j+18)
            Bias_array[i] = Bias_array[i] / 2**9

        plt.figure("Bias")
        plt.plot(t[:sample_length], Bias_array,'o')
        plt.xlabel("Time, s")
        plt.ylabel("Bias, V")

        plt.figure("Current")
        plt.plot(t[:sample_length], Current_array,'o')
        plt.xlabel("Time, s")
        plt.ylabel("Current, A")

    plt.show()

def change_freq(requested_freq):
    global frequency
    global AcqTime
    frequency = requested_freq * 1000
    clock_rate = 125E6
    total_clock_cycles = clock_rate / (requested_freq*1000)
    cycles_per_bias = int(total_clock_cycles/3)
    driver.set_bias_on_clock_cycles(cycles_per_bias)
    driver.set_sample_data_clock_cycles(cycles_per_bias-1)
    print(frequency)
    print(AcqTime)
    driver.set_acquisition_length(AcqTime, frequency)
    return frequency

def save(Normal_Mode):
    year = str(time.localtime()[0])
    month = str(time.localtime()[1])
    day = str(time.localtime()[2])
    hours = str(time.localtime()[3])
    minutes = str(time.localtime()[4])
    seconds = str(time.localtime()[5])
    if Normal_Mode == True:
        file_prefix = 'UFLP_Data_' + str(frequency)
    else:
        file_prefix = 'Voltage_Current_Data_' + str(frequency)
    date_structure = year + '_' + month + '_' + day + '_' + hours+'_'+ minutes +'_' +seconds
    file_structure = file_prefix + '_' + date_structure
    return file_structure

 #seconds
def acquire_time(acq):
    global AcqTime
    global frequency
    driver.set_acquisition_length(acq, frequency)
    AcqTime = acq
    print(frequency)
    print(AcqTime)
    return AcqTime

def set_cap(cap_pos):
    cap_val = [1E-8, 1E-7, 1E-6, 1E-5, 1E-9]
    cap_val_pos = [64,32,16,8,4]
    driver.set_capacitors(cap_val_pos[cap_pos])
    return cap_val[cap_pos]

def set_scale(V_Scale=1, I_Scale=1, Vb_Scale=1):
    driver.set_voltage_scale(V_Scale)
    driver.set_current_scale(I_Scale)
    driver.set_bias_scale(Vb_Scale)

time.sleep(0.01)
print(bin(driver.get_Temperature()))

# voltage_scale = 0.9737
# voltage_offset = -0.01219 * 2**12
# bias_scale = 1.13
# bias_offset = 57

voltage_scale = 1
voltage_offset = 0
bias_scale = 1
bias_offset = 0

current_scale = 1
current_offset = 0
output_scale = 1
output_offset = 0

change_freq(100)
driver.set_voltage_scale(voltage_scale)
driver.set_voltage_offset(voltage_offset)
driver.set_current_scale(current_scale)
driver.set_current_offset(current_offset)
driver.set_bias_scale(bias_scale)
driver.set_bias_offset(bias_offset)
driver.set_output_scale(output_scale)
driver.set_output_offset(output_offset)

driver.set_cap_manual(1)
set_cap(2)

#seconds
acquire_time(1)
