----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : April 2019
-- Module Name: Temperature_calc
-- Project Name: UFLP
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Calculates the Temperature from current values of:
--              sat_current, vFloat, LP_current, LP_bias_rx
-- 
-- Dependencies: constants, Temperature_LUT, types
----------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

library packages;
    use packages.types.all;
    use packages.Temperature_LUT.all;
    use packages.constants.all;

entity Temperature_calc is
port (
    -- Generic to all cores
    clk         : in std_logic;
    rst         : in std_logic;

    sample_data_clock_cycles_ctl : in std_logic_vector(31 downto 0);
    
    -- External inputs to device
    LP_current  : in std_logic_vector(DAC_bits - 1 downto 0); -- Current from probe
    LP_bias_rx      : in std_logic_vector(DAC_bits - 1 downto 0); -- Bias volt input
    
    -- Relevant bias state
    pos_bias    : in std_logic;

    -- Calculated values from other cores
    V_float     : in std_logic_vector(DAC_bits - 1 downto 0); -- Floating potential input
    sat_current : in std_logic_vector(DAC_bits - 1 downto 0);         -- iSat input
    
    -- Values calculated by this core
    Temperature        : out std_logic_vector(DAC_bits - 1 downto 0); -- Temperature ouput
    Temperature_out    : out std_logic_vector(DAC_bits - 1 downto 0) -- Temperature ouput
  );
end Temperature_calc;

architecture rtl of Temperature_calc is
    -- Signals for the use of the LUT to solve exponential
    -- LUT saved to rom to speed up process
    constant rom : LUT_type := Temperature_LUT;

    -- Registers for the pipelining of the calculation of the address
    signal Temperature_LUT_shift : std_logic_vector(5 downto 0);
    signal Temperature_addr : std_logic_vector(DAC_bits - 1 downto 0);
    signal Temperature_LUT_value : signed(DAC_bits - 1 downto 0);

    signal Temperature_hold : std_logic_vector(50 downto 0);

    -- Registers for division
    signal LP_val : signed(13 downto 0);
    signal iSat_val : signed(13 downto 0);
    signal Temperature_division : signed(13 downto 0);
    signal sub_calc : signed(13 downto 0);

    signal Temperature_ans_shift : unsigned(5 downto 0);
    
    signal Bias_Register : signed(13 downto 0);
    signal vFloat_Register : signed(13 downto 0);

    -- Registers for the pipelining of the calculation of the Temperature
    signal Temperature_hold_output : signed(13 downto 0);
    signal Temperature_multiply : signed(27 downto 0);
    signal Temperature_bit_shift : signed(13 downto 0);
    signal Temperature_subtraction : signed(13 downto 0);

    -- Sample the relevant signals at a valid position within the fet pulse
    signal sample_current : std_logic_vector(DAC_Bits - 1 downto 0);
    signal sample_bias : std_logic_vector(DAC_Bits - 1 downto 0);
    signal sample_iSat : std_logic_vector(DAC_Bits - 1 downto 0);
    signal sample_vFloat : std_logic_vector(DAC_Bits - 1 downto 0);

    -- Division signals
    signal Numerator : std_logic_vector(DAC_Bits - 1 downto 0);
    signal Denominator : std_logic_vector(DAC_Bits - 1 downto 0);
    signal Division_Valid : std_logic;
    signal Division_Result : std_logic_vector(DAC_Bits - 1 downto 0);
    signal Temperature_divide : signed(13 downto 0);

    -- Sampler Signals
    signal delay_counter : unsigned(31 downto 0);
    signal sample_probe : std_logic;
    signal sample_collected : std_logic;

    signal average_current : signed(DAC_Bits - 1 + 8 downto 0);
    signal average_voltage : signed(DAC_Bits - 1 + 8 downto 0);

begin
    -- Instatiate goldschmidt divider entity
    GOLDSCHMIDT_DIVISION : entity packages.gold(rtl)
    PORT MAP(
        clk => clk,
        rst => rst,
        div_complete => Division_Valid,
        Numerator => Numerator,
        Denominator => Denominator,
        Quotient => Division_Result
        );

    -- purpose: Chooses where to sample data based on fet bias pulse
    -- type   : synchronus with reset
    -- inputs : clk, rst, pos_bias, delay_counter, sample_data_clock_cycles
    -- outputs: sample_probe
    PROC_DELAY : process(clk)
    begin
        if rising_edge(clk) then
        if rst = '1' then
            delay_counter <= to_unsigned(0, delay_counter'length);
            sample_probe <= '0';
            sample_collected <= '0';
        else
            if pos_bias = '1' then
                delay_counter <= delay_counter + 1;
                sample_probe <= '0';
                    if delay_counter > unsigned(sample_data_clock_cycles_ctl) - 65  and delay_counter < unsigned(sample_data_clock_cycles_ctl) then
                        average_current <= average_current + signed(LP_Current);
                        average_voltage <= average_voltage + signed(LP_bias_rx);
                    end if;
                    if delay_counter = unsigned(sample_data_clock_cycles_ctl) and sample_probe = '0' then
                        sample_probe <= '1';
                        sample_collected <= '1';
                    end if;
                else
                    delay_counter <= to_unsigned(0, delay_counter'length);
                    average_current <= to_signed(0, average_current'length);
                    average_voltage <= to_signed(0, average_voltage'length);
                    sample_probe <= '0';
                end if;
            end if;
        end if;
    end process; -- PROC_DELAY

    -- purpose: Sample calculation variables towards the end of the fet pulse
    -- type   : synchronus process with reset
    -- inputs : clk , LP_current, LP_bias_rx, V_float, Temperature
    -- outputs: sample_current, sample_bias, sample_Temperature, sample_vFloat
    PROC_SAMPLE : process(clk)
    begin
        if rising_edge(clk) then
        if rst = '1' then
            sample_current <= LP_current;
            sample_bias <= LP_bias_rx;
            sample_iSat <= sat_current;
            sample_vFloat <= V_float;
        else
            if sample_probe = '1' then
            -- sample_current <= LP_current;
            sample_current <= std_logic_vector(to_signed(to_integer(shift_right(average_current,6)),sample_current'length));
            sample_bias <= LP_bias_rx;
            -- sample_bias <= std_logic_vector(to_signed(to_integer(shift_right(average_voltage,6)),sample_bias'length));
            sample_iSat <= sat_current;
            sample_vFloat <= V_float;
            end if;
        end if;
        end if;
    end process; -- PROC_SAMPLE

    -- purpose: Choose correct addr for rom
    -- type   : synchronus process with reset
    -- inputs : clk , rst, sample_current, sample_iSat
    -- outputs: iSat_addr
    PROC_ADDR : process(clk)
    begin -- Fix all types so that arithmatic can be done
        if rising_edge(clk) then
            LP_val <= signed(sample_current);
            iSat_val <= signed(sample_iSat);
            
            Numerator <= std_logic_vector(LP_val);
            Denominator <= std_logic_vector(iSat_val);
            if Division_Valid = '1' then
                Temperature_division <= shift_left(signed(Division_Result),1);
            end if;
            sub_calc <= Temperature_division + 2**11; 

            -- Finds the correct address in the LUT based on the current Bias, Temperature and V_f
            Temperature_addr <= std_logic_vector(sub_calc);
        end if;
    end process; -- PROC_ADDR

    -- purpose: Return LUT value at given address and the bit shift associated
    --          with that value. Bit shift input manually. LUT generated in python.
    -- type   : synchronus process
    -- inputs : clk, rom, Temperature_addr, DAC_bits
    -- outputs: Temperature_LUT_value, Temperature_LUT_shift
    PROC_LUT : process(clk)
        variable LUT_seg : integer range 0 to 2**DAC_bits;
    begin
        if rising_edge(clk) then
            LUT_seg := (to_integer(unsigned(Temperature_addr)))/ 2**11 ;
            case LUT_seg is
                when 0  =>
                -- Bits either side of the binary point are 5_9
                Temperature_LUT_shift <= std_logic_vector(to_unsigned(9,Temperature_LUT_shift'length));
                when 1 =>
                -- Bits either side of the binary point are 5_9
                Temperature_LUT_shift <=  std_logic_vector(to_unsigned(9,Temperature_LUT_shift'length));
                when others =>
                -- Bits either side of the binary point are 2_12
                Temperature_LUT_shift <= std_logic_vector(to_unsigned(12,Temperature_LUT_shift'length));
            end case;
            Temperature_LUT_value <= rom(to_integer(unsigned(Temperature_addr)));
        end if;
    end process; -- PROC_LUT

    -- purpose: Using LUT value and sampled current, calculate Temperature 
    -- type   : synchronus process with reset
    -- inputs : clk, Temperature_LUT_value, Temperature_LUT_shift, 
    --          sample_vFloat, sample_bias, sample_current
    -- outputs: Temperature
    PROC_CALC : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' or sample_collected = '0' then
                Temperature <= std_logic_vector(to_signed(Temperature_guess, Temperature'length));
                Temperature_out <= std_logic_vector(to_signed(Temperature_guess, Temperature_out'length));
            else
                Temperature_ans_shift <= unsigned(Temperature_LUT_shift) - (Temperature_shift - bias_shift);
                Bias_Register <= signed(sample_bias);
                vFloat_Register <= signed(sample_vFloat);
                Temperature_subtraction <= Bias_Register - vFloat_Register;
                Temperature_multiply <= Temperature_subtraction * Temperature_LUT_value;
                Temperature_bit_shift <= to_signed(to_integer(Temperature_multiply) / 2**to_integer(Temperature_ans_shift), Temperature_bit_shift'length);
                -- if Temperature_bit_shift /= 0 then
                if Temperature_bit_shift > 100 then
                    Temperature <=  std_logic_vector(to_signed(to_integer(Temperature_bit_shift), Temperature'length));
                else
                    Temperature <=  std_logic_vector(to_signed(Temperature_guess, Temperature'length));
                end if;
                -- Temperature <=  std_logic_vector(to_signed(Temperature_guess, Temperature'length));
                Temperature_out <=  std_logic_vector(to_signed(to_integer(Temperature_bit_shift), Temperature'length)); 
                -- Temperature_out <=  std_logic_vector(to_signed(Temperature_guess, Temperature'length));
            end if;
        end if;
    end process; -- PROC_CALC
end architecture;

