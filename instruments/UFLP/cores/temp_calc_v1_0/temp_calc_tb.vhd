----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : April 2019
-- Module Name: Temperature_calc_tb
-- Project Name: UFLP
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Calculates the Temperature from current values of:
--              sat_current, vFloat, LP_current, LP_bias_rx
-- 
-- Dependencies: constants, Temperature_LUT, types, sim_constants, sim_subprograms
----------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use std.env.finish;
    use std.textio.all;

library uflp;

library packages;
    use packages.types.all;
    use packages.Temperature_LUT.all;
    use packages.constants.all;

library sim_packages;
    use sim_packages.sim_constants.all;
    use sim_packages.sim_subprograms.all;

entity Temperature_calc_tb is
end Temperature_calc_tb; 

architecture sim of Temperature_calc_tb is
    signal clk : std_logic := '1';
    signal rst  : std_logic := '1';

    signal LP_current : std_logic_vector(DAC_bits - 1 downto 0) := (others => '0');
    signal LP_bias_rx : std_logic_vector(DAC_bits - 1 downto 0);

    signal pos_bias : std_logic;
    signal neg_bias : std_logic;

    signal sat_current : std_logic_vector(DAC_bits - 1 downto 0);
    signal V_float : std_logic_vector(DAC_bits - 1 downto 0);
    signal Temperature : std_logic_vector(DAC_bits - 1 downto 0) := (others => '0');
    signal Temperature_out : std_logic_vector(DAC_bits - 1 downto 0) := (others => '0');


    -- Completion signals
    signal v_msg : string(1 to 4);
    signal t_msg : string(1 to 4);

    signal v_comp : boolean := false;
    signal t_comp : boolean := false;

    signal sat_current_pcr : integer := integer(0.25 * 2**11);
    signal Temperature_pcr : integer := integer(28.43 * 2**7);
    signal V_float_pcr : integer := integer(-74.75 * 2**5);

    signal vc_solve_failed : boolean := false;
    signal vc_enable : boolean := false;
begin
    gen_clock(clk);
    
    -- Instanciate DUT
    DUT : entity uflp.Temperature_calc(rtl) port map (
        clk => clk,
        rst => rst,

        LP_current => LP_current,
        LP_bias_rx => LP_bias_rx,

        pos_bias => pos_bias,

        sat_current => sat_current,
        V_float => V_float,
        Temperature => Temperature,
        Temperature_out => Temperature_out
    );

    -- Instanciate the current solver verfication component
    vc : entity sim_packages.current_solver_vc(sim) 
    port map (
        sat_current_input => sat_current_pcr,
        Temperature_input => Temperature_pcr,
        V_float_input => V_float_pcr,
        enable => vc_enable,
        bias_input => LP_bias_rx,
        bias_state_input => neg_bias,
        
        solved_current => LP_current,
        solve_failed => vc_solve_failed
    );

    -- Simulate the negative bias state used in Temperature calculations
    SIM_BIAS_STATE : process
    begin
        pos_bias <= '0';
        wait for bias_off_clock_cycles*clock_period;
        pos_bias <= '1';
        wait for bias_on_clock_cycles * clock_period;
    end process; -- SIM_FET_DRIVE

    -- Set variables used in equations
    SIM_VARIABLE_SET : process
    begin
        sat_current <= std_logic_vector(to_signed(sat_current_pcr,sat_current'length));
        V_float <= std_logic_vector(to_signed(V_float_pcr,V_float'length));
        wait until rising_edge(clk);
    end process; -- SIM_CONSTANTS

    -- Set the bias used in the verification component
    SIM_VC : process
    begin
        LP_bias_rx <= std_logic_vector(to_signed(-3 * Temperature_pcr / 2,DAC_bits));
        vc_enable <= true;
        wait until rising_edge(clk);
        vc_enable <= false;
        wait for 1 ns;
    end process ; -- SIM_VC

    -- Standard reset process
    PROC_RESET : process
    begin
        wait for 20*clock_period;
        rst <= '0';
        wait for 10000*clock_period;
    end process; -- PROC_RESET

end architecture;
