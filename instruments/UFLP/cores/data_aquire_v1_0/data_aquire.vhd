----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : Feb 2020
-- 
-- Module Name: data_aquire
-- Project Name: UFLP
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Aquisition Calibration
-- 
-- Dependencies: constants
----------------------------------------------------------------------------------

library ieee ;
use ieee.std_logic_1164.all ;
use ieee.numeric_std.all ;

library packages;
use packages.constants.all;

entity data_aquire is
  port (
    -- Generic to all cores
    clk : in std_logic;
    rst : in std_logic; -- '1' = in reset

    voltage_scale : in std_logic_vector(31 downto 0);
    voltage_offset : in std_logic_vector(31 downto 0);
    current_scale : in std_logic_vector(31 downto 0);
    current_offset : in std_logic_vector(31 downto 0);

    -- Inputs to the core
    voltage_in : in std_logic_vector(DAC_Bits - 1 downto 0);
    current_in : in std_logic_vector(DAC_Bits - 1 downto 0);
    sample_data_clock_cycles_ctl : in std_logic_vector(31 downto 0);
    bias_on_clock_cycles_ctl : in std_logic_vector(31 downto 0);

    -- Outputs from the core
    current_out : out std_logic_vector(DAC_Bits - 1 downto 0);
    bias_out : out std_logic_vector(DAC_Bits - 1 downto 0)
  ) ;
end data_aquire ; 

architecture rtl of data_aquire is
-- Calibration Signals
signal voltage_cal_sample : signed(DAC_Bits - 1 downto 0);
signal voltage_cal_multiply : signed(2*DAC_Bits - 1 downto 0);
signal voltage_cal_shift : signed(2*DAC_Bits - 1 downto 0);
signal voltage_cal_offset : signed(2*DAC_Bits - 1 downto 0);
signal voltage_cal_resize : signed(DAC_Bits - 1 downto 0);

signal current_cal_sample : signed(DAC_Bits - 1 downto 0);
signal current_cal_multiply : signed(2*DAC_Bits - 1 downto 0);
signal current_cal_shift : signed(2*DAC_Bits - 1 downto 0);
signal current_cal_offset : signed(2*DAC_Bits - 1 downto 0);
signal current_cal_resize : signed(DAC_Bits - 1 downto 0);

-- Arithmatic Signals
signal voltage_sample : signed(DAC_Bits - 1 downto 0);
signal voltage_multiply : signed(2*DAC_Bits - 1 downto 0);
signal voltage_shift : signed(2*DAC_Bits - 1 downto 0);
signal voltage_resize : signed(DAC_Bits - 1 downto 0);

signal current_sample : signed(DAC_Bits - 1 downto 0);
signal current_multiply : signed(2*DAC_Bits - 1 downto 0);
signal current_shift : signed(2*DAC_Bits - 1 downto 0);
signal current_resize : signed(DAC_Bits - 1 downto 0);

-- Solved Signals
signal probe_voltage : signed(DAC_Bits - 1 downto 0);
signal voltage_voltage_temp : signed(2*DAC_Bits - 1 downto 0);
signal voltage_current : signed(DAC_Bits - 1 downto 0);
signal voltage_current_multiply : signed(2*DAC_Bits - 1 downto 0);
signal voltage_current_shift : signed(2*DAC_Bits - 1 downto 0);
signal probe_current : signed(DAC_Bits - 1 downto 0);
signal current_voltage_temp : signed(2*DAC_Bits - 1 downto 0);
signal current_current : signed(DAC_Bits - 1 downto 0);
signal current_current_multiply : signed(2*DAC_Bits - 1 downto 0);
signal current_current_shift : signed(2*DAC_Bits - 1 downto 0);

-- Calibration signals
signal voltage_scale_proxy : signed(13 downto 0) := to_signed(256, 14);
signal voltage_offset_proxy : signed(13 downto 0) := to_signed(0, 14);
signal current_scale_proxy : signed(13 downto 0) := to_signed(256, 14);
signal current_offset_proxy : signed(13 downto 0) := to_signed(0, 14);

begin
    -- purpose: Provide gradient and offset to an input
    -- type   : synchronus with reset
    -- inputs : clk, rst, current_in
    -- outputs: current_out
    PROC_Calibration : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                voltage_scale_proxy <= to_signed(256, 14);
                voltage_offset_proxy <= to_signed(0, 14);
                current_scale_proxy <= to_signed(256, 14);
                current_offset_proxy <= to_signed(0, 14);
            else
                if voltage_scale(13 downto 0) = std_logic_vector(to_signed(0, 14)) then
                    voltage_scale_proxy <= to_signed(256, 14);
                else
                    voltage_scale_proxy <= to_signed(to_integer(signed(voltage_scale)), 14);
                    -- voltage_scale_proxy <= signed(voltage_scale(voltage_scale_proxy'range));
                end if; 
                if voltage_offset(13 downto 0) = std_logic_vector(to_signed(0, 14)) then
                    voltage_offset_proxy <= to_signed(0, 14);
                else
                    voltage_offset_proxy <= to_signed(to_integer(signed(voltage_offset)), 14);
                    -- voltage_offset_proxy <= signed(voltage_offset(voltage_offset_proxy'range));
                end if; 
                if current_scale(13 downto 0) = std_logic_vector(to_signed(0, 14)) then
                    current_scale_proxy <= to_signed(256, 14);
                else
                    current_scale_proxy <= signed(current_scale(current_scale_proxy'range));
                end if; 
                if current_offset(13 downto 0) = std_logic_vector(to_signed(0, 14)) then
                    current_offset_proxy <= to_signed(0, 14);
                else
                    current_offset_proxy <= signed(current_offset(current_offset_proxy'range));
                end if; 
            end if;
        end if ;
    end process ; -- PROC_Calibration
    
    -- purpose: Provide gradient and offset to an input
    -- type   : synchronus with reset
    -- inputs : clk, rst, current_in
    -- outputs: current_out
    PROC_current_Cal : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then

            else
                -- y = 1.16x + 83
                current_cal_sample <= signed(current_in);
                -- current_cal_multiply <= current_cal_sample * 4645;
                current_cal_multiply <= current_cal_sample * to_signed((to_integer(current_scale_proxy)),14);
                current_cal_shift <= current_cal_multiply / 2**8;
                current_cal_offset <= current_cal_shift + to_signed((to_integer(current_offset_proxy)),14);
                current_cal_resize <= current_cal_offset(current_cal_resize'range);

                -- -- Uncomment to remove calibration
                -- current_cal_resize <= signed(current_in);
            end if;
        end if ;
    end process ; -- PROC_current_Cal

    -- purpose: Provide gradient and offset to an input
    -- type   : synchronus with reset
    -- inputs : clk, rst, bias_in
    -- outputs: bias_out
    PROC_voltage_Cal : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then

            else
                -- y = 1.16x + 11
                voltage_cal_sample <= signed(voltage_in);
                voltage_cal_multiply <= voltage_cal_sample * to_signed((to_integer(voltage_scale_proxy)),14);
                voltage_cal_shift <= voltage_cal_multiply / 2**8;
                voltage_cal_offset <= voltage_cal_shift + to_signed((to_integer(voltage_offset_proxy)),14);
                voltage_cal_resize <= voltage_cal_offset(voltage_cal_resize'range);

                -- Uncomment to remove calibration
                -- probe_voltage <= signed(voltage_cal_resize);
            end if;
        end if ;
    end process ; -- PROC_voltage_Cal

    -- purpose: Probe current and voltage solver
    -- type   : synchronus with reset
    -- inputs : clk, rst, bias_in
    -- outputs: bias_out
    PROC_Probe : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then

            else
                voltage_sample <= voltage_cal_resize;
                
                -- 1/11th (10R + 100R)
                -- voltage_multiply <= voltage_sample * 1638;
                -- voltage_shift <= voltage_multiply / 2**14;
                -- voltage_resize <= voltage_shift(voltage_resize'range);
                
                -- Voltage and current need to be corrected for resistors
                -- R1 = Load resistor  R2 = Measurement resistor
                -- V=Vr*(R1+R2)/R2
                -- for R1 = 20R and R2 = 10R: multiply by 3
                -- voltage_voltage_temp <= voltage_sample * 3;
                -- voltage_voltage <= voltage_voltage_temp(voltage_voltage'range);
                -- -- voltage_voltage <= voltage_sample(voltage_voltage'range);
                
                -- -- Current
                -- -- I=(Vr*(R1+R2)/R2)/(R1+R2)
                -- -- for R1 = 20R and R2 = 10R: divide by 10
                -- -- voltage_current_multiply <= voltage_sample * 1638;
                -- voltage_current_multiply <= voltage_sample * 5641;
                -- voltage_current_shift <= voltage_current_multiply / 2**14;
                -- voltage_current <= voltage_current_shift(voltage_current'range);
                -- voltage_current <= voltage_resize;
                -- voltage_current <= voltage_cal_resize;

            end if;
        end if ;
    end process ; -- PROC_Probe

    -- purpose: Dummy current and voltage solver
    -- type   : synchronus with reset
    -- inputs : clk, rst, bias_in
    -- outputs: bias_out
    PROC_Dummy : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then

            else
                -- current_sample <= current_cal_resize;
                -- current_multiply <= current_sample * 1638;
                -- current_shift <= current_multiply / 2**14;
                -- current_resize <= current_shift(current_resize'range);
                -- current_current <= current_resize;

                -- -- Voltage and current need to be corrected for resistors
                -- -- current_voltage_temp <= current_sample * 11;
                -- -- current_voltage <= current_voltage_temp(current_voltage'range);
                -- current_voltage <= current_sample(current_voltage'range);
                -- current_current <= current_sample(current_current'range);
            end if;
        end if ;
    end process ; -- PROC_Dummy

    -- purpose: Simple subtraction to give outputs
    -- type   : synchronus with reset
    -- inputs : clk, rst, bias_in
    -- outputs: bias_out
    PROC_Output : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                bias_out <= std_logic_vector(to_signed(-5000, bias_out'length));
                current_out <= std_logic_vector(to_signed(-800, current_out'length));
            else
                bias_out <= std_logic_vector(voltage_cal_resize);
                current_out <= std_logic_vector(current_cal_resize);
                -- bias_out <= std_logic_vector(voltage_voltage);
                -- current_out <= std_logic_vector(voltage_current - current_current);
            end if;
        end if ;
    end process ; -- PROC_Output


end architecture ;