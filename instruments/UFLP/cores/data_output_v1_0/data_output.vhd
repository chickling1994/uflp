----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : Feb 2020
-- 
-- Module Name: data_output
-- Project Name: UFLP
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Output Calibration
-- 
-- Dependencies: constants
----------------------------------------------------------------------------------

library ieee ;
use ieee.std_logic_1164.all ;
use ieee.numeric_std.all ;

library packages;
use packages.constants.all;

entity data_output is
  port (
    -- Generic to all cores
    clk : in std_logic;
    rst : in std_logic; -- '1' = in reset

    bias_scale : in std_logic_vector(31 downto 0);
    bias_offset : in std_logic_vector(31 downto 0);
    output_scale : in std_logic_vector(31 downto 0);
    output_offset : in std_logic_vector(31 downto 0);

    -- Inputs to the core
    LP_Bias_tx : in std_logic_vector(DAC_Bits - 1 downto 0);
    output_sig : in std_logic_vector(DAC_Bits - 1 downto 0);

    -- Outputs from the core
    output_dac : out std_logic_vector(DAC_Bits - 1 downto 0);
    bias_out_dac : out std_logic_vector(DAC_Bits - 1 downto 0)
  ) ;
end data_output ; 

architecture rtl of data_output is
-- Arithmatic Signals for Bias
signal bias_sample : signed(DAC_Bits - 1 downto 0);
signal bias_multiply : signed(2*DAC_Bits - 1 downto 0);
signal bias_shift : signed(2*DAC_Bits - 1 downto 0);
signal bias_intercept : signed(2*DAC_Bits - 1 downto 0);
signal bias_resize : signed(DAC_Bits - 1 downto 0);

-- Arithmatic Signals for Ouput
signal output_sample : signed(DAC_Bits - 1 downto 0);
signal output_multiply : signed(2*DAC_Bits - 1 downto 0);
signal output_shift : signed(2*DAC_Bits - 1 downto 0);
signal output_intercept : signed(2*DAC_Bits - 1 downto 0);
signal output_resize : signed(DAC_Bits - 1 downto 0);

signal bias_scale_proxy : signed(13 downto 0) := to_signed(256, 14);
signal bias_offset_proxy : signed(13 downto 0) := to_signed(0, 14);
signal output_scale_proxy : signed(13 downto 0) := to_signed(256, 14);
signal output_offset_proxy : signed(13 downto 0) := to_signed(0, 14);

begin
    -- purpose: Provide gradient and offset to an input
    -- type   : synchronus with reset
    -- inputs : clk, rst, current_in
    -- outputs: current_out
    PROC_Calibration : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                bias_scale_proxy <= to_signed(256, 14);
                bias_offset_proxy <= to_signed(0, 14);
                output_scale_proxy <= to_signed(256, 14);
                output_offset_proxy <= to_signed(0, 14);
            else
                if bias_scale(13 downto 0) = std_logic_vector(to_signed(0, 14)) then
                    bias_scale_proxy <= to_signed(256, 14);
                else
                    bias_scale_proxy <= to_signed(to_integer(signed(bias_scale)), 14);
                    -- bias_scale_proxy <= signed(bias_scale(bias_scale_proxy'range));
                end if; 
                if bias_offset(13 downto 0) = std_logic_vector(to_signed(0, 14)) then
                    bias_offset_proxy <= to_signed(0, 14);
                else
                    bias_offset_proxy <= to_signed(to_integer(signed(bias_offset)), 14);
                    -- bias_offset_proxy <= signed(bias_offset(bias_offset_proxy'range));
                end if; 
                if output_scale(13 downto 0) = std_logic_vector(to_signed(0, 14)) then
                    output_scale_proxy <= to_signed(256, 14);
                else
                    output_scale_proxy <= signed(output_scale(output_scale_proxy'range));
                end if; 
                if output_offset(13 downto 0) = std_logic_vector(to_signed(0, 14)) then
                    output_offset_proxy <= to_signed(0, 14);
                else
                    output_offset_proxy <= signed(output_offset(output_offset_proxy'range));
                end if; 
            end if;
        end if ;
    end process ; -- PROC_Calibration

    -- purpose: Provide gradient and offset to an input
    -- type   : synchronus with reset pipelined multiplication
    -- inputs : clk, rst, LP_Bias_tx
    -- outputs: bias_out_dac
    PROC_Bias : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                bias_out_dac <= std_logic_vector(to_signed(-5000, bias_out_dac'length));
            else
                -- y = 0.969x - 98
                bias_sample <= signed(LP_Bias_tx);
                bias_multiply <= bias_sample * to_signed((to_integer(signed(bias_scale_proxy))),14);
                bias_shift <= bias_multiply / 2**8;
                bias_intercept <= bias_shift + to_signed((to_integer(signed(bias_offset_proxy))),14);
                bias_resize <= bias_intercept(bias_resize'range);
                bias_out_dac <= std_logic_vector(bias_resize);

                -- Uncomment to remove calibration
                -- bias_out_dac <= LP_Bias_tx;
                
            end if;
        end if ;
    end process ; -- PROC_Current

    -- purpose: Provide gradient and offset to an input
    -- type   : synchronus with reset pipelined multiplication
    -- inputs : clk, rst, output_sig
    -- outputs: output_dac
    PROC_Output : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                output_dac <= std_logic_vector(to_signed(-800, output_dac'length));
            else
                -- y = 0.969x - 98
                output_sample <= signed(output_sig);
                output_multiply <= output_sample * to_signed((to_integer(signed(output_scale_proxy))),14);
                output_shift <= output_multiply / 2**12;
                output_intercept <= output_shift + to_signed((to_integer(signed(output_offset_proxy))),14);
                output_resize <= output_intercept(output_resize'range);
                output_dac <= std_logic_vector(output_resize);

                -- -- Uncomment to remove calibration
                -- output_dac <= output_sig;

                
            end if;
        end if ;
    end process ; -- PROC_Current

end architecture ;