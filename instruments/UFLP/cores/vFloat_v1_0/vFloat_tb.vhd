----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : April 2019
-- Module Name: iSat_tb
-- Project Name: UFLP
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Calculates the iSat from current values of:
--              Temperature, vFloat, LP_current, LP_bias_rx
-- 
-- Dependencies: constants, vFloat_LUT, types
----------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.math_real.all;
    use std.env.finish;
    use std.textio.all;

library uflp;

library packages;
    use packages.types.all;
    use packages.vFloat_LUT.all;
    use packages.constants.all;

library sim_packages;
    use sim_packages.sim_constants.all;
    use sim_packages.sim_subprograms.all;

entity vFloat_tb is
end vFloat_tb; 

architecture sim of vFloat_tb is
    signal clk : std_logic := '1';
    signal rst  : std_logic := '1';

    signal LP_current : std_logic_vector(DAC_bits - 1 downto 0) := (others => '0');
    signal LP_bias_rx : std_logic_vector(DAC_bits - 1 downto 0);

    signal zero_bias : std_logic := '0';

    signal sat_current : std_logic_vector(DAC_bits - 1 downto 0);
    signal Temperature : std_logic_vector(DAC_bits - 1 downto 0) := (others => '0');
    
    signal V_float : std_logic_vector(DAC_bits - 1 downto 0);

    signal vFloat_LUT_shift : std_logic_vector(3 downto 0);
    signal vFloat_addr : std_logic_vector(DAC_bits - 1 downto 0);
    signal vFloat_LUT_value : signed(DAC_bits - 1 downto 0);

    -- Completion signals
    signal v_msg : string(1 to 4);
    signal t_msg : string(1 to 4);

    signal v_comp : boolean := false;
    signal t_comp : boolean := false;

    signal sat_current_pcr : integer := integer(0.25 * 2**11);
    signal Temperature_pcr : integer := integer(28.43 * 2**7);
    signal V_float_pcr : integer := integer(-74.75 * 2**5);

    signal vc_solve_failed : boolean := false;
    signal vc_enable : boolean := false;

begin
    -- Generate clock as defined by sim_subprograms and sim_constants
    gen_clock(clk);

    -- Instanciate DUT
    DUT : entity uflp.vFloat(rtl) port map (
        clk => clk,
        rst => rst,

        LP_current => LP_current,
        LP_bias_rx => LP_bias_rx,

        zero_bias => zero_bias,

        sat_current => sat_current,
        Temperature => Temperature,
        V_float => V_float
    );

    -- Instanciate the current solver verfication component
    vc : entity sim_packages.current_solver_vc(sim) 
    port map (
        sat_current_input => sat_current_pcr,
        Temperature_input => Temperature_pcr,
        V_float_input => V_float_pcr,
        enable => vc_enable,
        bias_input => LP_bias_rx,
        bias_state_input => zero_bias,
        
        solved_current => LP_current,
        solve_failed => vc_solve_failed
    );

    -- Set variables used in equations
    SIM_VARIABLE_SET : process
    begin
        Temperature <= std_logic_vector(to_signed(Temperature_pcr,DAC_bits));
        sat_current <= std_logic_vector(to_signed(sat_current_pcr,DAC_bits));
        wait until rising_edge(clk);
    end process ; -- SIM_VARIABLE_SET
    
    -- Simulate the negative bias state used in iSat calculations
    SIM_BIAS_STATE : process
    begin
        zero_bias <= '0';
        wait for bias_off_clock_cycles*clock_period;
        zero_bias <= '1';
        wait for bias_on_clock_cycles * clock_period;
    end process ; -- SIM_BIAS_STATE

    -- Set the bias used in the verification component
    SIM_VC : process
    begin
        LP_bias_rx <= std_logic_vector(to_signed(V_float_pcr,DAC_bits));
        vc_enable <= true;
        wait until rising_edge(clk);
        vc_enable <= false;
        wait for 1 ns;
    end process ; -- SIM_VC

    -- Standard reset process
    PROC_RESET : process
    begin
        wait for 20*clock_period;
        rst <= '0';
        wait for 10000*clock_period;
    end process; -- PROC_RESET
end architecture;
