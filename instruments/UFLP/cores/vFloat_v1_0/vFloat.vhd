----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : April 2019
-- Module Name: iSat
-- Project Name: UFLP
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Calculates the V_Float from current values of:
--              Temperature, sat_current, LP_current, LP_bias_rx
-- 
-- Dependencies: constants, vFloat_LUT, types
----------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

library packages;
    use packages.types.all;
    use packages.vFloat_LUT.all;
    use packages.constants.all;

entity vFloat is
    port ( 
        -- Generic to all cores
        clk         : in std_logic;
        rst         : in std_logic;

        sample_data_clock_cycles_ctl : in std_logic_vector(31 downto 0);
        
        -- External inputs to device
        LP_current  : in std_logic_vector(DAC_bits - 1 downto 0); -- Current from probe
        LP_bias_rx  : in std_logic_vector(DAC_bits - 1 downto 0); -- Bias volt input

        -- Relevant bias state
        zero_bias    : in std_logic;

        -- Calculated values from other cores
        Temperature        : in std_logic_vector(DAC_bits - 1 downto 0); -- Temperature input
        sat_current : in std_logic_vector(DAC_bits - 1 downto 0);

        -- Values calculated by this core
        V_float     : out std_logic_vector(DAC_bits - 1 downto 0); -- Floating potential input
        V_float_out     : out std_logic_vector(DAC_bits - 1 downto 0) -- Floating potential input
    );
end vFloat;

architecture rtl of vFloat is
    -- LUT saved to rom to speed up process
    constant rom : LUT_type := vFloat_LUT;
    
    -- Signals for the use of the LUT to solve exponential
    signal vFloat_LUT_shift : std_logic_vector(5 downto 0);
    signal vFloat_addr : std_logic_vector(DAC_bits - 1 downto 0);
    signal vFloat_LUT_value : signed(DAC_bits - 1 downto 0);

    -- Pipelining for calculation of V_float
    signal vFloat_multiply : signed(27 downto 0);
    signal vFloat_bit_shift : signed(27 downto 0);
    signal vFloat_subtraction : signed(27 downto 0);
    signal vFloat_resize : signed(13 downto 0);
    signal ans_shift : unsigned(5 downto 0);

    -- Sample the relevant signals at a valid position within the fet pulse
    signal sample_current : std_logic_vector(DAC_Bits - 1 downto 0);
    signal sample_bias : std_logic_vector(DAC_Bits - 1 downto 0);
    signal sample_iSat : std_logic_vector(DAC_Bits - 1 downto 0);
    signal sample_Temperature : std_logic_vector(DAC_Bits - 1 downto 0);
    
    -- Division signals
    signal Addr_LP_val : signed(13 downto 0);
    signal Addr_iSat_val : signed(13 downto 0);
    signal Addr_Temperature_division : signed(13 downto 0);
    signal Addr_sub_calc : signed(13 downto 0);
    signal Numerator : std_logic_vector(DAC_Bits - 1 downto 0);
    signal Denominator : std_logic_vector(DAC_Bits - 1 downto 0);
    signal Division_Valid : std_logic;
    signal Division_Result : std_logic_vector(DAC_Bits - 1 downto 0);
    signal Temperature_division : signed(13 downto 0);
    signal Temperature_divide : signed(13 downto 0);

    -- Sampler Signals
    signal delay_counter : unsigned(31 downto 0);
    signal sample_probe : std_logic;
    signal sample_collected : std_logic;

    signal average_current : signed(DAC_Bits - 1 + 8 downto 0);
    signal average_voltage : signed(DAC_Bits - 1 + 8 downto 0);

begin
    -- Import goldschmidt divider entity
    GOLDSCHMIDT_DIVISION : entity packages.gold(rtl)
    PORT MAP(
        clk => clk,
        rst => rst,
        div_complete => Division_Valid,
        Numerator => Numerator,
        Denominator => Denominator,
        Quotient => Division_Result
    );
    -- purpose: Chooses where to sample data based on fet bias pulse
    -- type   : synchronus with reset
    -- inputs : clk, rst, zero_bias, delay_counter, output_average_counter
    -- outputs: sample_probe
    PROC_DELAY : process(clk)
    begin
        if rising_edge(clk) then
        if rst = '1' then
            delay_counter <= to_unsigned(0, delay_counter'length);
            sample_probe <= '0';
            sample_collected <= '0';
            average_current <= to_signed(0, average_current'length);
        else
            if zero_bias = '1' then
                delay_counter <= delay_counter + 1;
                sample_probe <= '0';
                if delay_counter > unsigned(sample_data_clock_cycles_ctl) - 65  and delay_counter < unsigned(sample_data_clock_cycles_ctl) then
                    average_current <= average_current + signed(LP_Current);
                    average_voltage <= average_voltage + signed(LP_bias_rx);
                end if;
                if delay_counter = unsigned(sample_data_clock_cycles_ctl) and sample_probe = '0' then
                    sample_probe <= '1';
                    sample_collected <= '1';
                end if;
            else
                delay_counter <= to_unsigned(0, delay_counter'length);
                sample_probe <= '0';
                average_current <= to_signed(0, average_current'length);
                end if;
            end if;
        end if;
    end process; -- PROC_DELAY


    -- purpose: Chooses where to sample data based on fet bias pulse
    -- type   : synchronus with reset
    -- inputs : clk, rst, zero_bias, delay_counter, sample_data_clock_cycles
    -- outputs: sample_probe
    PROC_SAMPLE : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                sample_current <= LP_current;
                sample_bias <= LP_bias_rx;
                sample_iSat <= sat_current;
                sample_Temperature <= Temperature;
            else
                if sample_probe = '1' then
                    -- sample_current <= LP_current;
                    sample_current <= std_logic_vector(to_signed(to_integer(shift_right(average_current,6)),sample_current'length));
                    sample_bias <= LP_bias_rx;
                    -- sample_bias <= std_logic_vector(to_signed(to_integer(shift_right(average_voltage,6)),sample_bias'length));
                    sample_iSat <= sat_current;
                    sample_Temperature <= Temperature;
                end if;
            end if;
        end if;
    end process; -- PROC_SAMPLE

    -- purpose: Choose correct addr for rom
    -- type   : synchronus process with reset
    -- inputs : clk , rst, sample_current, sample_iSat
    -- outputs: vFloat_addr
    PROC_ADDR : process(clk)
    begin -- Fix all types so that arithmatic can be done
        if rising_edge(clk) then
            -- Register signals used in calculation
            Addr_LP_val <= signed(sample_current);
            Addr_iSat_val <= signed(sample_iSat);

            -- 2x 14 bit input to the divider
            Numerator <= std_logic_vector(Addr_LP_val);
            Denominator <= std_logic_vector(Addr_iSat_val);

            -- Wait until division is valid, then return the division.
            -- Result is (N/D) * 2**10 
            if Division_Valid = '1' then
                Temperature_division <= shift_right(signed(Division_Result),0);
            end if;

            -- Addition register
            Addr_sub_calc <= Temperature_division + 2**10; 
    
            -- Finds the correct address in the LUT based on the current Bias, Temperature and V_f
            vFloat_addr <= std_logic_vector(Addr_sub_calc - 1);
        end if;
    end process; -- PROC_ADDR

    -- purpose: Return LUT value at given address and the bit shift associated
    --          with that value. Bit shift input manually. LUT generated in python.
    -- type   : synchronus process
    -- inputs : clk, rom, vFloat_addr, DAC_bits
    -- outputs: vFloat_LUT_value, vFloat_LUT_shift
    PROC_LUT : process(clk)
    variable LUT_seg : integer range 0 to 2**DAC_bits;
    begin
        if rising_edge(clk) then
            LUT_seg := (to_integer(unsigned(vFloat_addr)))/ 2**10 ;
            case LUT_seg is
                when 0  =>
                -- Bits either side of the binary point are 2_12
                vFloat_LUT_shift <= std_logic_vector(to_unsigned(12,vFloat_LUT_shift'length));
                when 1 to 3=>
                -- Bits either side of the binary point are 1_13
                vFloat_LUT_shift <=  std_logic_vector(to_unsigned(12,vFloat_LUT_shift'length));
                when others =>
                -- Bits either side of the binary point are 10_4
                vFloat_LUT_shift <= std_logic_vector(to_unsigned(11,vFloat_LUT_shift'length));
            end case;
            vFloat_LUT_value <= rom(to_integer(unsigned(vFloat_addr)));
        end if;
    end process; -- PROC_LUT


    -- purpose: Using LUT value and sampled current, calculate V_Float 
    -- type   : synchronus process with reset
    -- inputs : clk, iSat_LUT_value, vFloat_LUT_shift, Temperature_shift,
    --          sample_Temperature, vFloat_LUT_value
    -- outputs: V_Float, V_Float_out
    PROC_CALC : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' or sample_collected = '0' then
                V_Float <= std_logic_vector(to_signed(V_float_guess, V_float'length));
                V_Float_out <= std_logic_vector(to_signed(V_float_guess, V_Float_out'length));
            else
                ans_shift <= unsigned(vFloat_LUT_shift) + Temperature_shift - Bias_Shift;
                vFloat_multiply <= signed(sample_Temperature) * vFloat_LUT_value;
                vFloat_bit_shift <= vFloat_multiply/2**to_integer(ans_shift);
                vFloat_subtraction <= signed(sample_bias) - vFloat_bit_shift;
                vFloat_resize <= to_signed(to_integer(vFloat_subtraction), vFloat_resize'length);
                V_Float <=  std_logic_vector(vFloat_resize);
                V_Float_out <=  std_logic_vector(vFloat_resize);
                -- V_Float <= std_logic_vector(to_signed(V_float_guess, V_float'length));
                -- V_Float_out <= std_logic_vector(to_signed(V_float_guess, V_Float_out'length));
            end if;
        end if;
    end process; -- PROC_CALC
end architecture;
