----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : Feb 2020
-- 
-- Module Name: data_aquire
-- Project Name: PCR
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Aquisition Calibration
-- 
-- Dependencies: constants
----------------------------------------------------------------------------------
-- WHOLE CORE NEEDS REDESIGNING
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library packages;
use packages.constants.all;
use packages.types.all;
use packages.capacitor_bank_LUT.all;

entity cap_switch is
    port (
        clk : in std_logic;
        rst : in std_logic;

        sat_current : in std_logic_vector(DAC_bits - 1 downto 0);

        cap_manual_switch : in std_logic;
        cap_selection : in std_logic_vector(31 downto 0);

        GPIO_Cap_0 : out std_logic;
        GPIO_Cap_1 : out std_logic;
        GPIO_Cap_2 : out std_logic;
        GPIO_Cap_3 : out std_logic;
        GPIO_Cap_4 : out std_logic;
        GPIO_Cap_5 : out std_logic;
        GPIO_Cap_6 : out std_logic;
        GPIO_Cap_7 : out std_logic
    );
end cap_switch; 

architecture rtl of cap_switch is

-- LUT saved to rom to speed up process
constant rom : LUT_type := capacitor_bank_LUT;

signal Cap_LUT_value : signed(DAC_bits - 1 downto 0);

begin
    -- purpose: Return LUT value at given address and the bit shift associated
    --          with that value. Bit shift input manually. LUT generated in python.
    -- type   : synchronus process
    -- inputs : sat_current
    -- outputs: correct GPIO pin
    PROC_LUT : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                GPIO_Cap_0 <= '0';
                GPIO_Cap_1 <= '0';
                GPIO_Cap_2 <= '0';
                GPIO_Cap_3 <= '0';
                GPIO_Cap_4 <= '0';
                GPIO_Cap_5 <= '0';
                GPIO_Cap_6 <= '0';
                GPIO_Cap_7 <= '0';
            else
                -- Return value from ROM
                GPIO_Cap_0 <= '1';
                GPIO_Cap_1 <= '1';
                GPIO_Cap_2 <= '1';
                GPIO_Cap_3 <= '1';
                GPIO_Cap_4 <= '1';
                GPIO_Cap_5 <= '1';
                GPIO_Cap_6 <= '1';
                GPIO_Cap_7 <= '1';

                Cap_LUT_value <= rom(to_integer(unsigned(sat_current(sat_current'length - 2 downto 0))));
                if cap_manual_switch = '1' then
                    GPIO_Cap_0 <= cap_selection(0);
                    GPIO_Cap_1 <= cap_selection(1);
                    GPIO_Cap_2 <= cap_selection(2);
                    GPIO_Cap_3 <= cap_selection(3);
                    GPIO_Cap_4 <= cap_selection(4);
                    GPIO_Cap_5 <= cap_selection(5);
                    GPIO_Cap_6 <= cap_selection(6);
                    GPIO_Cap_7 <= cap_selection(7);
                -- else
                --     if Cap_LUT_value = 0 then
                --         GPIO_Cap_0 <= '1';
                --         GPIO_Cap_1 <= '1';
                --         GPIO_Cap_2 <= '1';
                --         GPIO_Cap_3 <= '1';
                --         GPIO_Cap_4 <= '1';
                --         GPIO_Cap_5 <= '1';
                --         GPIO_Cap_6 <= '1';
                --         GPIO_Cap_7 <= '1';
                --     else
                --         -- GPIO_Cap_0 <= Cap_LUT_value(0);
                --         -- GPIO_Cap_1 <= Cap_LUT_value(1);
                --         -- GPIO_Cap_2 <= Cap_LUT_value(2);
                --         -- GPIO_Cap_3 <= Cap_LUT_value(3);
                --         -- GPIO_Cap_4 <= Cap_LUT_value(4);
                --         -- GPIO_Cap_5 <= Cap_LUT_value(5);
                --         -- GPIO_Cap_6 <= Cap_LUT_value(6);
                --         -- GPIO_Cap_7 <= Cap_LUT_value(7);
                --     end if;
                end if;
            end if;
        end if;
    end process; -- PROC_LUT
end architecture;