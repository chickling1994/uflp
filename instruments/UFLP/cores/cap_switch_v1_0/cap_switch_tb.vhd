library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use std.env.finish;
use std.textio.all;

library uflp;

library packages;
use packages.types.all;
use packages.Temperature_LUT.all;
use packages.constants.all;

library sim_packages;
use sim_packages.sim_constants.all;
use sim_packages.sim_subprograms.all;

entity cap_switch_tb is
end cap_switch_tb; 

architecture sim of cap_switch_tb is
    signal clk : std_logic := '1';
    signal rst : std_logic := '1';
    signal sat_current : std_logic_vector(DAC_bits - 1 downto 0);

    signal capacitance : std_logic_vector(5 downto 0);
begin
    gen_clock(clk);

    DUT : entity uflp.cap_switch(rtl) 
    port map (
        clk => clk,
        rst => rst,
        sat_current => sat_current,
    
        capacitance => capacitance
    );

    -- Standard reset process
    PROC_RESET : process
    begin
        wait for 20*clock_period;
        rst <= '0';
        wait for 10000*clock_period;
    end process; -- PROC_RESET
end architecture;