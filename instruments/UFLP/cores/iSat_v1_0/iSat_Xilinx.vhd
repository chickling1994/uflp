----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : April 2019
-- Module Name: iSat
-- Project Name: UFLP
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Calculates the iSat from current values of:
--              Temperature, vFloat, LP_current, LP_bias_rx
-- 
-- Dependencies: constants, iSat_LUT, types
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library packages;
    use packages.types.all;
    use packages.iSat_LUT.all;
    use packages.constants.all;

entity Xilinx_iSat is
    port ( 
        -- Generic to all cores
        clk         : in std_logic;
        rst         : in std_logic;
        
        -- External inputs to device
        LP_current  : in std_logic_vector(DAC_bits - 1 downto 0); -- Current from probe
        LP_bias_rx      : in std_logic_vector(DAC_bits - 1 downto 0); -- Bias volt input
        
        -- Relevant bias state
        neg_bias    : in std_logic;

        -- Calculated values from other cores
        Temperature        : in std_logic_vector(DAC_bits - 1 downto 0); -- Temperature input
        V_float     : in std_logic_vector(DAC_bits - 1 downto 0); -- Floating potential input

        -- Values calculated by this core
        sat_current : out std_logic_vector(DAC_bits - 1 downto 0);
        sat_current_out : out std_logic_vector(DAC_bits - 1 downto 0)
    );
end Xilinx_iSat;

architecture rtl of Xilinx_iSat is

    -- LUT saved to rom to speed up process
    constant rom : LUT_type := iSat_LUT;
    
    -- Signals for the use of the LUT to solve exponential
    signal iSat_LUT_shift : std_logic_vector(3 downto 0);
    signal iSat_addr : std_logic_vector(DAC_bits - 1 downto 0);
    signal iSat_LUT_value : signed(DAC_bits - 1 downto 0);
    signal LUT_seg : unsigned(3 downto 0);

    -- Registers for the pipelining of the calculation of the address
    signal V_float_temp : signed(DAC_Bits - 1 downto 0) ;
    signal Te_temp : signed(DAC_Bits - 1 downto 0) ;

    signal LP_bias_temp : signed(DAC_Bits - 1 downto 0) ;
    
    signal Last_Temperature_subtraction : signed(DAC_Bits - 1 downto 0) ;
    signal Temperature_subtraction : signed(DAC_Bits - 1 downto 0) ;
    signal Temperature_divide : signed(DAC_Bits - 1 downto 0);
    signal Temperature_division : signed(DAC_Bits - 1 downto 0);
    signal sub_calc : signed(DAC_Bits - 1 downto 0);
    
    -- Registers for the pipelining of the calculation of the sat current
    signal Temperature_multiply : signed(27 downto 0);
    signal ans_shift : unsigned(3 downto 0);

    -- Sample the relevant signals at a valid position within the fet pulse
    signal sample_current : std_logic_vector(DAC_Bits - 1 downto 0);
    signal sample_bias : std_logic_vector(DAC_Bits - 1 downto 0);
    signal sample_Temperature : std_logic_vector(DAC_Bits - 1 downto 0);
    signal sample_vFloat : std_logic_vector(DAC_Bits - 1 downto 0);

    -- Signal inputs into the goldschmidt divider
    signal Numerator : std_logic_vector(DAC_Bits - 1 downto 0);
    signal Denominator : std_logic_vector(DAC_Bits - 1 downto 0);
    signal Division_Valid : std_logic;
    signal Division_Result : std_logic_vector(DAC_Bits - 1 downto 0);

    -- Sampler Signals
    signal delay_counter : unsigned(11 downto 0);
    signal sample_probe : std_logic;

    signal s_axis_divisor_tvalid : STD_LOGIC;
    signal s_axis_divisor_tdata : STD_LOGIC_VECTOR(15 DOWNTO 0);
    signal s_axis_dividend_tvalid : STD_LOGIC;
    signal s_axis_dividend_tdata : STD_LOGIC_VECTOR(15 DOWNTO 0);
    signal m_axis_dout_tvalid : STD_LOGIC;
    signal m_axis_dout_tdata : STD_LOGIC_VECTOR(31 DOWNTO 0);

        ------------- Begin Cut here for COMPONENT Declaration ------ COMP_TAG
    COMPONENT div_gen_0
    PORT (
        aclk : IN STD_LOGIC;
        s_axis_divisor_tvalid : IN STD_LOGIC;
        s_axis_divisor_tdata : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        s_axis_dividend_tvalid : IN STD_LOGIC;
        s_axis_dividend_tdata : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        m_axis_dout_tvalid : OUT STD_LOGIC;
        m_axis_dout_tdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
        );
    END COMPONENT;
begin

    ------------- Begin Cut here for INSTANTIATION Template ----- INST_TAG
    Xilinx_Divider_Core : div_gen_0
    PORT MAP (
        aclk => clk,
        s_axis_divisor_tvalid => s_axis_divisor_tvalid,
        s_axis_divisor_tdata => s_axis_divisor_tdata,
        s_axis_dividend_tvalid => s_axis_dividend_tvalid,
        s_axis_dividend_tdata => s_axis_dividend_tdata,
        m_axis_dout_tvalid => m_axis_dout_tvalid,
        m_axis_dout_tdata => m_axis_dout_tdata
    );

    -- Import goldschmidt divider entity
    GOLDSCHMIDT_DIVISION : entity packages.gold(rtl)
    PORT MAP(
        clk => clk,
        rst => rst,
        div_complete => Division_Valid,
        Numerator => Numerator,
        Denominator => Denominator,
        Quotient => Division_Result
    );

    -- purpose: Chooses where to sample data based on fet bias pulse
    -- type   : synchronus with reset
    -- inputs : clk, rst, neg_bias, delay_counter, sample_data_clock_cycles
    -- outputs: sample_probe
    PROC_DELAY : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                delay_counter <= to_unsigned(0, delay_counter'length);
                sample_probe <= '0';
            else
                if neg_bias = '1' and sample_probe = '0' then
                delay_counter <= delay_counter + 1;
                sample_probe <= '0';
                if delay_counter = sample_data_clock_cycles  then
                    sample_probe <= '1';
                end if;
                else
                delay_counter <= to_unsigned(0, delay_counter'length);
                sample_probe <= '0';
                end if;
            end if; 
        end if;
    end process; -- PROC_DELAY

    -- purpose: Sample calculation variables towards the end of the fet pulse
    -- type   : synchronus process with reset
    -- inputs : clk , LP_current, LP_bias_rx, V_float, Temperature
    -- outputs: sample_current, sample_bias, sample_Temperature, sample_vFloat
    PROC_SAMPLE : process(clk)
    begin
        if rising_edge(clk) then
        if rst = '1' then
            sample_current <= LP_current;
            sample_bias <= LP_bias_rx;
            sample_Temperature <= Temperature;
            sample_vFloat <= V_float;
        else
            if sample_probe = '1' then
                sample_current <= LP_current;
                sample_bias <= LP_bias_rx;
                sample_Temperature <= Temperature;
                sample_vFloat <= V_float;
            end if;
        end if;
        end if;
    end process; -- PROC_SAMPLE

    -- purpose: Choose correct addr for rom
    -- type   : synchronus process with reset
    -- inputs : clk , rst, sample_vFloat, sample_bias, sample_Temperature
    -- outputs: iSat_addr
    PROC_ADDR : process(clk)
    begin 
        if rising_edge(clk) then
            if rst = '1' then
                iSat_addr <= std_logic_vector(to_signed(0, iSat_addr'length));
                V_float_temp  <= signed(sample_vFloat); 
                LP_bias_temp  <= signed(sample_bias);
                Te_temp  <= signed(sample_Temperature);
                Last_Temperature_subtraction  <= (others => '0');
            else
                -- Register signals used in calculation
                V_float_temp  <= signed(sample_vFloat); 
                LP_bias_temp  <= shift_right(signed(sample_bias), V_float_shift - bias_shift);
                Te_temp  <= shift_right(signed(sample_Temperature), (Temperature_shift - bias_shift));
                
                -- Subtraction Register
                Last_Temperature_subtraction  <= Temperature_Subtraction;
                Temperature_subtraction  <= LP_bias_temp - V_float_temp;
                
                -- Numerator <= std_logic_vector(Temperature_subtraction);
                -- Denominator <= std_logic_vector(Te_Temp);
                Numerator <= std_logic_vector(to_signed(4000, Numerator'length));
                Denominator <= std_logic_vector(to_signed(-4000, Denominator'length));

                -- 2x 14 bit input to the divider]
                if NOT(Temperature_Subtraction = Last_Temperature_subtraction) then
                    s_axis_dividend_tdata <= std_logic_vector(to_signed(to_integer(signed(Numerator)),s_axis_dividend_tdata'length));
                    -- s_axis_dividend_tdata <= std_logic_vector(to_signed(to_integer(Temperature_subtraction),s_axis_dividend_tdata'length));
                    s_axis_divisor_tdata <= std_logic_vector(to_signed(to_integer(signed(Denominator)),s_axis_divisor_tdata'length));
                    -- s_axis_divisor_tdata <= std_logic_vector(to_signed(to_integer(Te_Temp),s_axis_divisor_tdata'length));
                    s_axis_divisor_tvalid <= '1';
                    s_axis_dividend_tvalid <= '1';
                end if;

                -- Wait until division is valid, then return the division.
                -- Result is (N/D) * 2**10 
                if m_axis_dout_tvalid = '1' then
                    if m_axis_dout_tdata(m_axis_dout_tdata'high) = '1' then
                        Temperature_divide <= "1" & signed(m_axis_dout_tdata(18 downto 16)) & signed(m_axis_dout_tdata(14 downto 5));
                    else
                        Temperature_divide <= "0" & signed(m_axis_dout_tdata(18 downto 16)) & signed(m_axis_dout_tdata(14 downto 5));
                    end if;
                    s_axis_divisor_tvalid <= '0';
                    s_axis_dividend_tvalid <= '0';
                end if;

                -- Addition register
                sub_calc <= Temperature_divide + 2**13;
                -- Output address
                iSat_addr <= std_logic_vector(sub_calc);
            end if;
        end if;
    end process; -- PROC_ADDR

    -- purpose: Return LUT value at given address and the bit shift associated
    --          with that value. Bit shift input manually. LUT generated in python.
    -- type   : synchronus process
    -- inputs : clk, rom, iSat_addr, DAC_bits
    -- outputs: iSat_LUT_value, iSat_LUT_shift
    PROC_LUT : process(clk)
    begin
        if rising_edge(clk) then
            -- Return value from ROM
            iSat_LUT_value <= rom(to_integer(unsigned(iSat_addr)));
            
            -- Find shift associated with ROM location
            LUT_seg <= unsigned(iSat_addr(iSat_addr'length - 1 downto iSat_addr'length - 4)) ;
            case to_integer(LUT_seg) is
                when 0 to 6  =>
                -- Bits either side of the binary point are 2_12
                iSat_LUT_shift <= std_logic_vector(to_unsigned(12,iSat_LUT_shift'length));
                when 9 to 15 =>
                -- Bits either side of the binary point are 1_13
                iSat_LUT_shift <=  std_logic_vector(to_unsigned(13,iSat_LUT_shift'length));
                when others =>
                -- Bits either side of the binary point are 6_8
                iSat_LUT_shift <= std_logic_vector(to_unsigned(8,iSat_LUT_shift'length));
            end case;
        end if;
    end process; -- PROC_LUT


    -- purpose: Using LUT value and sampled current, calculate iSat 
    -- type   : synchronus process with reset
    -- inputs : clk, iSat_LUT_value, sample_current, iSat_LUT_shift
    -- outputs: sat_current
    PROC_CALC : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                sat_current <= std_logic_vector(to_signed(iSat_guess, sat_current'length));
            else
                ans_shift <= unsigned(iSat_LUT_shift);
                Temperature_multiply <= signed(sample_current) * signed(iSat_LUT_value);

                -- Apply correction shift to result based on LUT value
                sat_current <=  std_logic_vector(to_signed(to_integer(shift_right(Temperature_multiply, to_integer(ans_shift))), sat_current'length)); 
                sat_current_out <=  std_logic_vector(to_signed(to_integer(shift_right(Temperature_multiply, to_integer(ans_shift))), sat_current'length)); 
            end if;
        end if;
    end process; -- PROC_CALC

end architecture;

