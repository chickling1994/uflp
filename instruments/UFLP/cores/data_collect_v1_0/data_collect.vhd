----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : Feb 2020
-- 
-- Module Name: data_collect
-- Project Name: PCR
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Output to fifo in 32-bit register
-- 
-- Dependencies: constants
----------------------------------------------------------------------------------

library ieee ;
use ieee.std_logic_1164.all ;
use ieee.numeric_std.all ;

library packages;
use packages.constants.all;

entity data_collect is
  port (
    -- Generic to all cores
    clk : in std_logic;
    rst : in std_logic; -- '1' = in reset

    bias_on_clock_cycles_ctl : in std_logic_vector(31 downto 0);

    LP_current : in std_logic_vector(DAC_Bits - 1 downto 0);
    LP_bias_rx : in std_logic_vector(DAC_Bits - 1 downto 0);

    -- Inputs to the core
    sat_current : in std_logic_vector(DAC_Bits - 1 downto 0);
    Temperature : in std_logic_vector(DAC_Bits - 1 downto 0);
    V_Float : in std_logic_vector(DAC_Bits - 1 downto 0);

    trigger : in std_logic;
    acquisition_length : in std_logic_vector(31 downto 0);
    data_valid : in std_logic;

    Normal_Data : in std_logic;

    -- Outputs from the core
    -- Fifo is 32-bit
    data_out : out std_logic_vector(31 downto 0);
    tvalid : out std_logic
  ) ;
end data_collect ; 

architecture rtl of data_collect is
    signal acquire_complete : std_logic;
    signal acquisition_counter : unsigned(31 downto 0);
    signal clock_counter : unsigned(34 downto 0);
    signal acquisition_period : unsigned(31 downto 0);
begin
    -- purpose: Concatenate 3 variables with a 1 padding at the start
    --          ensures bits are not lost returning from fifo
    -- type   : synchronus with reset
    -- inputs : clk, rst, sat_current, Temperature, V_Float
    -- outputs: data_out
    PROC_DATA_OUT : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                data_out <= (others => '0');
            else
                if Normal_Data = '1' then
                    data_out <= '1' &
                        sat_current(sat_current'length - 1 downto 3) &
                        Temperature(Temperature'length - 1 downto 3) &
                        V_Float(V_Float'length - 1 downto 5);
                else
                    data_out(data_out'high) <= '1';
                    data_out(30 downto 28) <= (others => '0');
                        data_out(27 downto 14) <= LP_current;
                        data_out(13 downto 0) <= LP_bias_rx;
                end if;
            end if;
        end if ;
    end process ; -- PROC_DATA_OUT

    -- purpose: 
    -- type   : 
    -- inputs : 
    -- outputs: 
    PROC_DATA_VALID : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                tvalid <= '0';
                acquisition_period <= to_unsigned(total_sweep_cycles,acquisition_period'length);
                clock_counter <= (others => '0');
                acquire_complete <= '1';
                acquisition_counter <= (others => '0');
            else
                tvalid <= '0';
                if trigger = '1' and acquire_complete = '1' then
                    acquire_complete <= '0';
                end if;
                if acquire_complete = '0' then
                    clock_counter <= clock_counter + 1;
                    if clock_counter = unsigned(bias_on_clock_cycles_ctl) * 3 then
                        tvalid <= '1';
                        clock_counter <= (others => '0');
                        acquisition_counter <= acquisition_counter + 1;
                        if acquisition_counter = unsigned(acquisition_length) then
                            acquire_complete <= '1';
                            acquisition_counter <= (others => '0');
                        end if;
                    end if;
                end if;
            end if;
        end if ;
    end process ; -- PROC_DATA_OUT
end architecture ;