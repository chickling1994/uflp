----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : April 2019
-- Module Name: output_mux
-- Project Name: UFLP
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Cycles the 3 ouputs in a MUX fashion based on fet cycles
-- 
-- Dependencies: constants
----------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

library packages;
    use packages.constants.all;

entity output_mux is
    port (
        clk : in std_logic;
        rst : in std_logic;

        Temperature        : in std_logic_vector(DAC_bits - 1 downto 0); -- Temperature input
        V_float     : in std_logic_vector(DAC_bits - 1 downto 0); -- Floating potential input
        sat_current : in std_logic_vector(DAC_bits - 1 downto 0);

        neg_bias : in std_logic;
        zero_bias : in std_logic;
        pos_bias : in std_logic;

        output_sig : out std_logic_vector(DAC_bits - 1 downto 0)
    );
end output_mux; 

architecture rtl of output_mux is
    signal delay_counter : unsigned(11 downto 0);
    signal sample_results : std_logic;
begin
    -- purpose: Chooses where to sample data based on fet bias pulse
    -- type   : synchronus with reset
    -- inputs : clk, rst, neg_bias, delay_counter, output_average_counter
    -- outputs: sample_results
    PROC_DELAY : process(clk)
    begin
        if rising_edge(clk) then
        if rst = '1' then
            delay_counter <= to_unsigned(0, delay_counter'length);
            sample_results <= '0';
        else
            if (zero_bias = '1' or pos_bias = '1' or neg_bias = '1') and sample_results = '0' then
                delay_counter <= delay_counter + 1;
                sample_results <= '0';
                if delay_counter = sample_data_clock_cycles then
                    sample_results <= '1';
                end if;
            else
                delay_counter <= to_unsigned(0, delay_counter'length);
                sample_results <= '0';
                end if;
            end if;
        end if;
    end process; -- PROC_DELAY

    -- purpose: Chooses the corrected output signal based on the fet pulse
    -- type   : synchronus with reset
    -- inputs : clk, rst, neg_bias, pos_bias, zero_bias, V_float, Temperature,
    --          sat_current
    -- outputs: output_sig
    PROC_SIG : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                output_sig <= (others => '0');
            else
                if sample_results = '1' then
                    if neg_bias = '1'  then
                        output_sig <= V_float;
                    end if;
                    if pos_bias = '1' then
                        output_sig <= sat_current;
                    end if;
                    if zero_bias = '1' then
                        output_sig <= Temperature;
                    end if;
                end if;
            end if;
        end if;
    end process; -- PROC_SIG
end architecture;