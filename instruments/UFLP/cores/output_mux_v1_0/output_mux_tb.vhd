----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : April 2019
-- Module Name: output_mux
-- Project Name: UFLP
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Cycles the 3 ouputs in a MUX fashion based on fet cycles
-- 
-- Dependencies: constants, sim_constants, sim_subprograms
----------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use std.env.finish;
    use std.textio.all;

library packages;
    use packages.constants.all;

library sim_packages;
    use sim_packages.sim_constants.all;
    use sim_packages.sim_subprograms.all;

entity output_mux_tb is
end output_mux_tb; 

architecture sim of output_mux_tb is
    -- DUT Inputs
    signal clk : std_logic := '1';
    signal rst : std_logic := '1';

    signal Temperature        : std_logic_vector(DAC_bits - 1 downto 0); -- Temperature input
    signal V_float     : std_logic_vector(DAC_bits - 1 downto 0); -- Floating potential input
    signal sat_current : std_logic_vector(DAC_bits - 1 downto 0);

    signal output_sig : std_logic_vector(DAC_bits - 1 downto 0);
begin
    -- Generate clock as defined by sim_subprograms and sim_constants
    gen_clock(clk);

    -- Instanciate DUT
    DUT : entity uflp.output_mux(rtl)
    port map (
        clk => clk , 
        rst => rst , 
    
        Temperature => Temperature ,        
        V_float => V_float ,     
        sat_current => sat_current , 
    
        output_sig => output_sig
    );

    -- Standard reset process
    PROC_RESET : process
    begin
        wait for 20*clock_period;
        rst <= '0';
        wait for 10000*clock_period;
    end process; -- PROC_RESET
end architecture;