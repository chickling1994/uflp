----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : Feb 2020
-- 
-- Module Name: data_aquire
-- Project Name: PCR
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Generate timestamps for the status registers
-- 
-- Dependencies: constants
----------------------------------------------------------------------------------


library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;

library packages;
    use packages.constants.all;

entity time_stamp is
  port (
    -- Generic to all cores
    clk : in std_logic;
    rst : in std_logic; -- '1' = in reset

    timestamp : out std_logic_vector(17 downto 0)
  ) ;
end time_stamp ; 

architecture rtl of time_stamp is
signal counter : unsigned(19 downto 0);
signal timestamp_counter : unsigned(timestamp'length - 2 downto 0);
begin

    -- purpose: Count number of complete cycles (all 3 calculated variables)
    -- type   : synchronus with reset
    -- inputs : clk, rst, total_sweep_cycles, counter, timestamp_counter
    -- outputs: timestamp
    PROC_COUNTER : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                timestamp <= (others => '0');
                counter <= (others => '0');
                timestamp_counter <= (others => '0');
            else 
                timestamp(timestamp'high) <= '1';
                -- timestamp(18 downto 7) <= std_logic_vector(total_sweep_cycles);
                if counter = total_sweep_cycles then
                    timestamp(16 downto 0) <= std_logic_vector(timestamp_counter + 1);
                    timestamp_counter <= timestamp_counter + 1;
                end if;
                counter <= counter + 1;
            end if;
        end if ;
    end process ; -- PROC_COUNTER
    
end architecture ;