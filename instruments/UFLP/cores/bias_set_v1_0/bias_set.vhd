----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : March 2020
-- 
-- Module Name: bias_set
-- Project Name: UFLP
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Calculate next cycles bias values based off temperature
--              Output bias in all states
-- 
-- Dependencies: constants
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library packages;
use packages.constants.all;

entity bias_set is
  port (
        -- Generic to all cores
        clk : in std_logic;
        rst : in std_logic;


        bias_on_clock_cycles_ctl : in std_logic_vector(31 downto 0);

        -- Inputs to the core
        Temperature : in std_logic_vector(DAC_bits - 1 downto 0);
        V_float : in std_logic_vector(DAC_bits - 1 downto 0);

        neg_bias : in std_logic;
        zero_bias : in std_logic;
        pos_bias : in std_logic;

        -- Outputs from the core
        change_bias_out : out std_logic;
        LP_bias_tx : out std_logic_vector(DAC_Bits - 1 downto 0)
  );
end bias_set; 

architecture rtl of bias_set is

    -- Registers for calculation
    signal temperature_input : signed(Dac_Bits - 1 downto 0);
    signal vFloat_input : signed(Dac_Bits - 1 downto 0);
    signal multiple_factor : signed(Dac_Bits - 1 downto 0);
    signal Calc_multiply : signed(multiple_factor'length + temperature_input'length - 1 downto 0);
    signal Calc_shift : signed(Calc_multiply'range);
    signal Calc_resize : signed(Dac_Bits - 1 downto 0);

    -- Stores concatenated FET states
    signal bias_state_concatenate : std_logic_vector(2 downto 0);

    -- Used to generate delay to apply bias change at correct time
    signal delay_counter : unsigned(31 downto 0);
    signal change_bias : std_logic;
begin
    
    -- purpose: Return relevant multiply factor based on current FET state
    -- type   : synchronus process
    -- inputs : clk , neg_bias, pos_bias, zero_bias
    -- outputs: multiple_factor
    PROC_MULTIPLY_FACTOR : process(clk)
    begin
        if rising_edge(clk) then
            bias_state_concatenate <= neg_bias & pos_bias & zero_bias ;
            Case bias_state_concatenate is
                when "100" =>
                    -- V- = -2.4eV * 2**11
                    -- multiple_factor <= to_signed(-4915, multiple_factor'length);
                    -- V- = -3.325eV * 2**11
                    multiple_factor <= to_signed(-6810, multiple_factor'length);
                when "010" =>
                    -- V+ = +0.64eV * 2**11
                    multiple_factor <= to_signed(1310, multiple_factor'length);
                when "001" =>
                    multiple_factor <= to_signed(0, multiple_factor'length);
                when others =>
                    NULL;
            end case;
        end if;
    end process; -- PROC_MULTIPLY

    -- purpose: Chooses where to sample data based on fet bias pulse
    --          always 1 clock cycle after zero_bias is sampled
    -- type   : synchronus with reset
    -- inputs : clk, rst, zero_bias, delay_counter, sample_data_clock_cycles
    -- outputs: change_bias
    PROC_DELAY : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                delay_counter <= to_unsigned(0, delay_counter'length);
                change_bias <= '0';
            else
                if zero_bias = '1' and change_bias = '0' then
                    delay_counter <= delay_counter + 1;
                    change_bias <= '0';
                    if delay_counter =  unsigned(bias_on_clock_cycles_ctl) - 15 then
                        change_bias <= '1';
                    end if;
                else
                    delay_counter <= to_unsigned(0, delay_counter'length);
                    change_bias <= '0';
                end if;
                change_bias_out <= change_bias;
            end if;
        end if;
    end process; -- PROC_DELAY

    -- purpose: Return the bias to request from the powersupply
    -- type   : synchronus process with reset
    -- inputs : clk , bias_temp, Temperature_guess, Capacitor_PD
    -- outputs: LP_bias_tx
    PROC_SET_BIAS : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                temperature_input <= to_signed(Temperature_guess, temperature_input'length);
                LP_bias_tx <= (others => '0');
            else
                -- -- Fixed output Bias
                -- if change_bias = '1' then
                --     temperature_input <= to_signed(2500,14);
                --     vFloat_input <= signed(V_float);
                -- end if;
                -- -- Registers to perform calculations
                -- Calc_multiply <= shift_right(temperature_input * multiple_factor, 11);
                -- Calc_shift <= shift_right(Calc_multiply, (Temperature_shift - bias_shift));
                -- Calc_resize <= to_signed(to_integer((Calc_shift + vFloat_input)),Calc_resize'length);
                -- Calc_resize <= to_signed(to_integer(Calc_shift),Calc_resize'length);
                -- LP_bias_tx <= std_logic_vector(Calc_resize);

                -- Only request new bias after sample for vFloat has occured
                if change_bias = '1' then
                    temperature_input <= signed(Temperature);
                    -- temperature_input <= to_signed(4500,14);
                    vFloat_input <= signed(V_float);
                end if;
                -- Registers to perform calculations
                Calc_multiply <= shift_right(temperature_input * multiple_factor, 11);
                -- Calc_shift <= Calc_multiply;
                Calc_shift <= shift_right(Calc_multiply, (Temperature_shift - bias_shift));
                -- Calc_resize <= to_signed(to_integer((Calc_shift + vFloat_input)),Calc_resize'length);
                Calc_resize <= to_signed(to_integer((Calc_shift)),Calc_resize'length);
                -- Calc_resize <= to_signed(to_integer((Calc_shift + signed(V_float))),Calc_resize'length);
                LP_bias_tx <= std_logic_vector(Calc_resize);
            end if;
        end if;
    end process; -- PROC_SET_BIAS
end architecture;