----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : March 2020
-- 
-- Module Name: bias_set_tb
-- Project Name: UFLP
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Calculate next cycles bias values based off temperature
--              Output bias in all states
-- 
-- Dependencies: constants, sim_constants, sim_packages, uflp
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bias_set_tb is
end bias_set_tb; 

library uflp;

library packages;
use packages.constants.all;

library sim_packages;
use sim_packages.sim_constants.all;
use sim_packages.sim_subprograms.all;

architecture sim of bias_set_tb is
    -- DUT Inputs
    signal clk : std_logic := '1';
    signal rst : std_logic := '1';

    signal Temperature : std_logic_vector(DAC_bits - 1 downto 0) := (others => '1');
    signal V_float : std_logic_vector(DAC_bits - 1 downto 0) := (others => '1');

    signal neg_bias : std_logic;
    signal zero_bias : std_logic;
    signal pos_bias : std_logic;

    -- DUT Outputs
    signal change_bias_out : std_logic;
    signal LP_bias_tx : std_logic_vector(DAC_Bits - 1 downto 0);
begin
    gen_clock(clk);

    BIAS_SET : entity uflp.bias_set(rtl)
    port map (
        clk => clk,
        rst => rst,

        Temperature => Temperature,
    
        neg_bias => neg_bias,
        zero_bias => zero_bias,
        pos_bias => pos_bias,
    
        change_bias_out => change_bias_out,
        LP_bias_tx => LP_bias_tx
    );

    PROC_RST : process
    begin
        wait for 10*clock_period;
        rst <= '0';
        wait until rising_edge(clk);
    end process; -- PROC_BIAS

    -- Simulate generic temperature
    PROC_TEMP : process
    begin
        Temperature <= std_logic_vector(to_signed(3600, Temperature'length));
        wait until rising_edge(clk);
    end process ; -- PROC_TEMP

    -- Simulate fet drive
    PROC_FET_DRIVE : process
    begin
        neg_bias <= '1';
        wait for bias_on_clock_cycles* clock_period;
        neg_bias <= '0';
        wait until rising_edge(clk);
        pos_bias <= '1';
        wait for bias_on_clock_cycles* clock_period;
        pos_bias <= '0';
        wait until rising_edge(clk);
        zero_bias <= '1';
        wait for bias_on_clock_cycles* clock_period;
        zero_bias <= '0';
        wait until rising_edge(clk);
    end process; -- PROC_BAIS

end architecture;