library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library packages;
use packages.constants.all;

entity capacitence_sample is
  port (
    clk : in std_logic;
    rst : in std_logic;

    zero_bias : in std_logic;
    LP_bias_rx : in std_logic_vector(DAC_bits - 1 downto 0);
    V_float : in std_logic_vector(DAC_bits - 1 downto 0);
    Capacitence_PD_sample : in std_logic_vector(DAC_bits - 1 downto 0);

    Capacitor_PD_measure : out std_logic_vector(DAC_bits - 1 downto 0)
  );
end capacitence_sample; 

architecture rtl of capacitence_sample is
  -- Sampler Signals
  signal delay_counter : unsigned(14 downto 0);
  signal sample_probe : std_logic;
begin

  -- purpose: Chooses where to sample data based on fet bias pulse
  -- type   : synchronus with reset
  -- inputs : clk, rst, neg_bias, delay_counter, sample_data_clock_cycles
  -- outputs: sample_probe
  PROC_DELAY : process(clk)
  begin
      if rising_edge(clk) then
        if rst = '1' then
            delay_counter <= to_unsigned(0, delay_counter'length);
            sample_probe <= '0';
        else
            if zero_bias = '1' and sample_probe = '0' then
              delay_counter <= delay_counter + 1;
              sample_probe <= '0';
              if delay_counter = ((sample_data_clock_cycles / 2) + 5) then
                  sample_probe <= '1';
              end if;
            else
              delay_counter <= to_unsigned(0, delay_counter'length);
              sample_probe <= '0';
            end if;
        end if; 
      end if;
  end process; -- PROC_DELAY

  PROC_CAP_SAMPLE : process(clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        Capacitor_PD_measure <= std_logic_vector(to_signed(V_float_guess, Capacitor_PD_measure'length));
      else
        if sample_probe = '1' then
          Capacitor_PD_measure <= LP_bias_rx;
          -- Capacitor_PD_measure <= Capacitence_PD_sample;
          -- Capacitor_PD_measure <= std_logic_vector(to_signed(-2392, Capacitor_PD_measure'length));
        end if;
        --Capacitor_PD_measure <= V_float;
      end if;
    end if;
  end process; -- PROC_CAP_SAMPLE

end architecture;