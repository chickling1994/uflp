----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : April 2019
-- Module Name: top_mlp
-- Project Name: UFLP
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Structural module to package all cores
-- 
-- Dependencies: constants, uflp
----------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

-- Import UFLP library
library uflp;

library packages;
    use packages.constants.all;

entity top_mlp is
  port (
    -- Generic to all cores
    clk : in std_logic;
    -- External reset buttons
    rst_button : in std_logic;

    sample_data_clock_cycles_ctl : in std_logic_vector(31 downto 0);
    bias_on_clock_cycles_ctl : in std_logic_vector(31 downto 0);

    -- External inputs to device
    -- probe_in : in std_logic_vector(DAC_Bits - 1 downto 0);
    -- dummy_in : in std_logic_vector(DAC_Bits - 1 downto 0);
    voltage_in : in std_logic_vector(DAC_Bits - 1 downto 0);
    current_in : in std_logic_vector(DAC_Bits - 1 downto 0);

    voltage_scale : in std_logic_vector(31 downto 0);
    voltage_offset : in std_logic_vector(31 downto 0);
    current_scale : in std_logic_vector(31 downto 0);
    current_offset : in std_logic_vector(31 downto 0);
    -- probe_scale : in std_logic_vector(31 downto 0);
    -- probe_offset : in std_logic_vector(31 downto 0);
    -- dummy_scale : in std_logic_vector(31 downto 0);
    -- dummy_offset : in std_logic_vector(31 downto 0);
    
    bias_scale : in std_logic_vector(31 downto 0);
    bias_offset : in std_logic_vector(31 downto 0);
    output_scale : in std_logic_vector(31 downto 0);
    output_offset : in std_logic_vector(31 downto 0);

    Normal_Data : in std_logic;

    trigger : in std_logic;
    acquisition_length : in std_logic_vector(31 downto 0);
    tvalid : out std_logic;

    -- Debug LEDs
    debug_leds : out std_logic_vector(7 downto 0);

    -- Copies of outputs to allow status register monitoring
    sat_current_out : out std_logic_vector(DAC_bits - 1 downto 0);
    Temperature_out : out std_logic_vector(DAC_bits - 1 downto 0); -- Temperature input
    V_float_out     : out std_logic_vector(DAC_bits - 1 downto 0); -- Floating potential input
    
    output_dac : out std_logic_vector(DAC_Bits - 1 downto 0);
    bias_out_dac : out std_logic_vector(DAC_bits - 1 downto 0);

    -- timestamp for saving of data
    timestamp : out std_logic_vector(17 downto 0);

    -- capacitor controller
    cap_manual_switch : in std_logic;
    cap_selection : in std_logic_vector(31 downto 0);
    GPIO_Cap_0 : out std_logic;
    GPIO_Cap_1 : out std_logic;
    GPIO_Cap_2 : out std_logic;
    GPIO_Cap_3 : out std_logic;
    GPIO_Cap_4 : out std_logic;
    GPIO_Cap_5 : out std_logic;
    GPIO_Cap_6 : out std_logic;
    GPIO_Cap_7 : out std_logic;

    ext_debug_led_2 : out std_logic;
    ext_debug_led_3 : out std_logic;

    data_collect_stream : out std_logic_vector(31 downto 0)
);
end top_mlp; 

architecture str of top_mlp is
    signal neg_bias : std_logic;
    signal zero_bias : std_logic;
    signal pos_bias : std_logic;

    signal change_bias : std_logic;
    
    signal LP_current : std_logic_vector(DAC_bits - 1 downto 0);
    signal LP_bias_rx : std_logic_vector(DAC_Bits - 1 downto 0);      
    
    signal sat_current : std_logic_vector(DAC_bits - 1 downto 0);
    signal Temperature : std_logic_vector(DAC_Bits - 1 downto 0);    
    signal V_float : std_logic_vector(DAC_Bits - 1 downto 0);
    
    signal LP_bias_tx : std_logic_vector(DAC_bits - 1 downto 0);
    signal output_sig : std_logic_vector(DAC_Bits - 1 downto 0);

    -- Reset Signals
    signal rst : std_logic;
    signal rst_hang : std_logic;

    -- signal sample_data_clock_cycles_ctl : std_logic_vector(31 downto 0) := std_logic_vector(to_unsigned(392,32));

begin
    ext_debug_led_2 <= rst_hang;
    -- Import ALL core dependancies as entities

    -- LP_Current <= probe_in;
    -- bias_out_dac <= LP_Bias_tx;

    RESET : entity packages.reset(rtl)
    port map (
        clk => clk,
        rst_in => rst_button,
        trigger => trigger,
        acquisition_length => acquisition_length,

        rst_out => rst,
        rst_hang => rst_hang
    );

    GEN_TIMESTAMP : entity uflp.time_stamp(rtl)
    port map (
        clk => clk ,
        rst => rst ,

        timestamp => timestamp
    );

    data_aquire : entity uflp.data_aquire(rtl)
    port map(
        clk => clk,
        rst =>  rst,

        voltage_in => voltage_in,
        current_in => current_in,

        voltage_scale => voltage_scale,
        voltage_offset => voltage_offset,
        current_scale => current_scale,
        current_offset => current_offset,

        sample_data_clock_cycles_ctl => sample_data_clock_cycles_ctl,
        bias_on_clock_cycles_ctl => bias_on_clock_cycles_ctl,

        current_out => LP_current,
        bias_out => LP_bias_rx
    );    
    
    data_output : entity uflp.data_output(rtl)
    port map(
        clk => clk,
        rst =>  rst,

        LP_Bias_tx => LP_Bias_tx,
        output_sig => output_sig,

        bias_scale => bias_scale,
        bias_offset => bias_offset,
        output_scale => output_scale,
        output_offset => output_offset,

        output_dac => output_dac,
        bias_out_dac => bias_out_dac
    );

    FET_Drive_INST : entity uflp.FET_Drive(rtl)
    port map (
        clk => clk ,
        rst =>  rst ,
        bias_on_clock_cycles_ctl => bias_on_clock_cycles_ctl,
        neg_bias => neg_bias ,
        zero_bias => zero_bias ,
        pos_bias => pos_bias
    );

    cap_switch : entity uflp.cap_switch(rtl)
    port map (
        clk => clk,
        rst => rst,

        sat_current => sat_current,

        cap_manual_switch => cap_manual_switch,
        cap_selection => cap_selection,
    
        GPIO_Cap_0 => GPIO_Cap_0,
        GPIO_Cap_1 => GPIO_Cap_1,
        GPIO_Cap_2 => GPIO_Cap_2,
        GPIO_Cap_3 => GPIO_Cap_3,
        GPIO_Cap_4 => GPIO_Cap_4,
        GPIO_Cap_5 => GPIO_Cap_5,
        GPIO_Cap_6 => GPIO_Cap_6,
        GPIO_Cap_7 => GPIO_Cap_7
    );

    iSat : entity uflp.iSat(rtl) 
    port map (
        clk => clk,
        rst => rst,

        sample_data_clock_cycles_ctl => sample_data_clock_cycles_ctl,

        LP_current => LP_current,
        LP_bias_rx => LP_bias_rx,
        -- LP_bias_rx => LP_bias_tx,

        neg_bias => neg_bias,

        Temperature => Temperature,
        V_float => V_float,
        sat_current => sat_current,
        sat_current_out => sat_current_out
    );

    Temperature_calc : entity uflp.Temperature_calc(rtl) 
    port map (
        clk => clk,
        rst => rst,

        sample_data_clock_cycles_ctl => sample_data_clock_cycles_ctl,

        LP_current => LP_current,
        LP_bias_rx => LP_bias_rx,
        -- LP_bias_rx => LP_bias_tx,

        pos_bias => pos_bias,

        sat_current => sat_current,
        V_float => V_float,
        Temperature => Temperature,
        Temperature_out => Temperature_out
    );

    vFloat : entity uflp.vFloat(rtl) 
    port map (
        clk => clk,
        rst => rst,

        sample_data_clock_cycles_ctl => sample_data_clock_cycles_ctl,

        LP_current => LP_current,
        LP_bias_rx => LP_bias_rx,
        -- LP_bias_rx => LP_bias_tx,

        zero_bias => zero_bias,

        sat_current => sat_current,
        Temperature => Temperature,
        V_float => V_float,
        V_float_out => V_float_out
    );

    BIAS_SET : entity uflp.bias_set(rtl)
    port map (
        clk => clk,
        rst => rst,

        bias_on_clock_cycles_ctl => bias_on_clock_cycles_ctl,
    
        Temperature => Temperature,
        V_float => V_float,
    
        neg_bias => neg_bias,
        zero_bias => zero_bias,
        pos_bias => pos_bias,

        change_bias_out => change_bias,
    
        LP_bias_tx => LP_bias_tx
    );

    MUX_OUT : entity uflp.output_mux(rtl)
    port map (
        clk => clk, 
        rst => rst, 
    
        neg_bias => neg_bias,
        zero_bias => zero_bias,
        pos_bias => pos_bias,

        Temperature => Temperature,        
        V_float => V_float,     
        sat_current => sat_current, 
        output_sig => output_sig
    );

    DATA_COLLECT : entity uflp.data_collect(rtl)
    port map (
        clk => clk, 
        rst => rst, 

        bias_on_clock_cycles_ctl => bias_on_clock_cycles_ctl,

        LP_current => LP_current,
        LP_bias_rx => LP_bias_rx,

        sat_current => sat_current,
        Temperature => Temperature,
        V_float => V_float,

        trigger => trigger,
        acquisition_length => acquisition_length,
        data_valid => change_bias,

        Normal_Data => Normal_Data,

        data_out => data_collect_stream,
        tvalid => tvalid
    );
end architecture;