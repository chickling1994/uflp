----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : April 2019
-- Module Name: top_mlp
-- Project Name: UFLP
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Structural module to package all cores
-- 
-- Dependencies: constants, uflp
----------------------------------------------------------------------------------

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  use std.env.finish;
  use std.textio.all;

library uflp;
library pcr;

library packages;
    use packages.constants.all;

library sim_packages;
    use sim_packages.sim_constants.all;
    use sim_packages.sim_subprograms.all;
    use sim_packages.current_solver_vc;

entity top_mlp_tb is
end top_mlp_tb; 

architecture sim of top_mlp_tb is
    -- DUT signals
    signal clk : std_logic := '1';
    signal rst_button : std_logic := '1';

    signal cap_manual_switch : std_logic := '0';
    signal cap_selection : std_logic_vector(31 downto 0) := std_logic_vector(to_unsigned(0,32));

    signal sample_data_clock_cycles_ctl : std_logic_vector(31 downto 0) := std_logic_vector(to_unsigned(250,32));
    signal bias_on_clock_cycles_ctl : std_logic_vector(31 downto 0) := std_logic_vector(to_unsigned(450,32));

    signal voltage_in : std_logic_vector(DAC_Bits - 1 downto 0) := std_logic_vector(to_signed(-2880,DAC_bits));
    signal current_in : std_logic_vector(DAC_Bits - 1 downto 0) := std_logic_vector(to_signed(0,DAC_bits));
    
    signal debug_leds : std_logic_vector(7 downto 0) ;

    signal sat_current_out : std_logic_vector(DAC_bits - 1 downto 0);
    signal Temperature_out        : std_logic_vector(DAC_bits - 1 downto 0); -- Temperature input
    signal V_float_out    : std_logic_vector(DAC_bits - 1 downto 0); -- Floating potential input

    signal voltage_scale : std_logic_vector(31 downto 0);
    signal voltage_offset : std_logic_vector(31 downto 0);
    signal current_scale : std_logic_vector(31 downto 0);
    signal current_offset : std_logic_vector(31 downto 0);
    
    signal bias_scale : std_logic_vector(31 downto 0);
    signal bias_offset : std_logic_vector(31 downto 0);
    signal output_scale : std_logic_vector(31 downto 0);
    signal output_offset : std_logic_vector(31 downto 0);

    signal tvalid : std_logic;
    signal pcr_tvalid : std_logic;
    
    signal output_dac : std_logic_vector(DAC_Bits - 1 downto 0);
    signal bias_out_dac : std_logic_vector(DAC_bits - 1 downto 0);

    signal current_LUT_shift : std_logic_vector(3 downto 0);
    signal current_addr : std_logic_vector(DAC_bits - 1 downto 0);
    signal current_LUT_value : signed(DAC_bits - 1 downto 0);

    signal neg_bias : std_logic;
    signal zero_bias : std_logic;
    signal pos_bias : std_logic;

    signal sat_current_pcr : integer;
    signal Temperature_pcr : integer;
    signal V_float_pcr : integer;

    type plasma_parameter_set is (set_1, set_2, set_3, set_4, set_5, set_6, set_7);
    signal State : plasma_parameter_set := set_1;

    signal counter : unsigned(17 downto 0) := to_unsigned(0, 18);

    signal vc_solve_failed : boolean := false;
    signal vc_enable : boolean := false;

    signal rst : std_logic;
    signal trigger : std_logic := '1';
    signal rst_hang : std_logic;
    signal acquisition_length : std_logic_vector(31 downto 0);

    signal pcr_Switch : std_logic_vector(31 downto 0) := (others => '0');
    signal pcr_data_out : std_logic_vector(31 downto 0);
    signal pcr_data_valid : std_logic;
begin
    gen_clock(clk);

    DUT : entity uflp.top_mlp(str)
    port map (
        clk => clk,  
        rst_button => rst_button,
        cap_manual_switch => cap_manual_switch,
        cap_selection => cap_selection,
        sample_data_clock_cycles_ctl => sample_data_clock_cycles_ctl,
        bias_on_clock_cycles_ctl => bias_on_clock_cycles_ctl,
        voltage_in => voltage_in,
        voltage_scale => voltage_scale,
        voltage_offset => voltage_offset,
        current_scale => current_scale,
        current_offset => current_offset,
        bias_scale => bias_scale,
        bias_offset => bias_offset,
        output_scale => output_scale,
        output_offset => output_offset,
        current_in => current_in,
        trigger => trigger, 
        acquisition_length => acquisition_length,
        tvalid => tvalid,
        debug_leds => debug_leds,  
        sat_current_out => sat_current_out,  
        Temperature_out => Temperature_out,         
        V_float_out => V_float_out,  
        output_dac => output_dac,
        bias_out_dac => bias_out_dac
        -- rst => rst
    );

    RESET : entity packages.reset(rtl)
    port map (
        clk => clk,
        rst_in => rst_button,
        trigger => trigger,
        acquisition_length => acquisition_length,

        rst_out => rst,
        rst_hang => rst_hang
    );

    FET_Drive_INST : entity uflp.FET_Drive(rtl)
    port map (
        clk => clk ,
        rst =>  rst ,
        bias_on_clock_cycles_ctl => bias_on_clock_cycles_ctl,
        neg_bias => neg_bias ,
        zero_bias => zero_bias ,
        pos_bias => pos_bias
    );

    -- vc : entity sim_packages.current_solver_vc(sim) 
    -- port map (
    --   sat_current_input => sat_current_pcr,
    --   Temperature_input => Temperature_pcr,
    --   V_float_input => V_float_pcr,
    --   enable => vc_enable,
    --   bias_input => bias_out_dac,
      
    --   solved_current => current_in,
    --   solve_failed => vc_solve_failed
    -- );

    DUT_PCR : entity pcr.top_pcr(str)
    port map (
        clk => clk,
        rst_button => rst_button,
    
        -- Python configurable inputs
        Switch => pcr_Switch,
        sat_current_in => std_logic_vector(to_signed(sat_current_pcr,14)),
        temperature_in => std_logic_vector(to_signed(Temperature_pcr,14)),
        v_Float_in => std_logic_vector(to_signed(V_float_pcr,14)),
        
        -- Input of interest
        bias_in => bias_out_dac,
        -- Outputs of interest
        current_out_dac => current_in,
        bias_out_dac => voltage_in,

        trigger => trigger,
        acquisition_length => acquisition_length,
    
        -- Concatenated data for status register retrieval
        tdata => pcr_data_out,
        -- Data valid signal for data_out
        tvalid => pcr_tvalid
    );

    SIM_INITIALISATION : process
    begin
        case State is
            when set_1 =>
                sat_current_pcr <= integer(1500);
                -- sat_current_pcr <= integer(0.2 * 2**11);
                -- sat_current_pcr <= integer(iSat_guess);
                Temperature_pcr <= integer(2500);
                -- Temperature_pcr <= integer(28.43 * 2**7);
                -- Temperature_pcr <= integer(Temperature_guess);
                V_float_pcr <= integer(-4096);
                -- V_float_pcr <= integer(-74.75 * 2**5);
                -- V_float_pcr <= integer(V_float_guess);
              if counter(counter'high) = '1' then
                State <= set_2;
                counter <= to_unsigned(0,counter'length);
              end if;
            when set_2 =>
                sat_current_pcr <= integer(0.35 * 2**11);
                Temperature_pcr <= integer(28.43 * 2**7);
                V_float_pcr <= integer(-74.75 * 2**5);
              if counter(counter'high) = '1' then
                State <= set_3;
                counter <=to_unsigned(0,counter'length);
              end if;
            when set_3 =>
                sat_current_pcr <= integer(0.35 * 2**11); 
                Temperature_pcr <= integer(35.43 * 2**7);
                V_float_pcr <= integer(-74.75 * 2**5);
              if counter(counter'high) = '1' then
                State <= set_4;
                counter <=to_unsigned(0,counter'length);
              end if;
            when set_4 => 
                sat_current_pcr <= integer(0.35 * 2**11);
                Temperature_pcr <= integer(35.43 * 2**7);
                V_float_pcr <= integer(-84.75 * 2**5);
              if counter(counter'high) = '1' then
                State <= set_5;
                counter <=to_unsigned(0,counter'length);
              end if;
            when set_5 =>
                sat_current_pcr <= integer(0.65 * 2**11);
                Temperature_pcr <= integer(45.43 * 2**7);
                V_float_pcr <= integer(-84.75 * 2**5);
              if counter(counter'high) = '1' then
                State <= set_6;
                counter <=to_unsigned(0,counter'length);
              end if;
            when set_6 =>
                sat_current_pcr <= integer(0.55 * 2**11);
                Temperature_pcr <= integer(45.43 * 2**7);
                V_float_pcr <= integer(-101.75 * 2**5);
              if counter(counter'high) = '1' then
                State <= set_7;
                counter <=to_unsigned(0,counter'length);
              end if;
            when set_7 =>
                sat_current_pcr <= integer(0.65 * 2**11);
                Temperature_pcr <= integer(45.43 * 2**7);
                V_float_pcr <= integer(-101.75 * 2**5);
              if counter(counter'high) = '1' then
                State <= set_1;
                counter <=to_unsigned(0,counter'length);
                print_test_ok_gen;
                finish;
              end if;
            when others => NULL;
          end case;
          if counter(counter'high) = '0' then
              counter <= counter + 1;
          end if;
          wait until rising_edge(clk);
    end process; -- PROC_VAL_SET

    SIM_VC : process
    begin
      -- voltage_in <= std_logic_vector(signed(bias_out_dac)/11 + V_float_pcr);
      -- voltage_in <= bias_out_dac;
      -- voltage_in <= std_logic_vector(to_signed(to_integer(signed(voltage_in)*3),14));
      vc_enable <= true;
      wait until rising_edge(clk);
      vc_enable <= false;
      wait for 1 ns;
    end process ; -- SIM_VC

    PROC_RST : process
    begin
      wait for 150*clock_period;
      rst_button <= '0';
      wait until rising_edge(clk);
    end process; -- PROC_RST

    PROC_ACQUISITION : process
    begin
      Acquisition_Length <= std_logic_vector(to_unsigned(200,Acquisition_Length'length));

      voltage_scale <= std_logic_vector(to_signed(256,32));
      voltage_offset <= std_logic_vector(to_signed(0,32));
      current_scale <= std_logic_vector(to_signed(256,32));
      current_offset <= std_logic_vector(to_signed(0,32));
      bias_scale <= std_logic_vector(to_signed(256,32));
      bias_offset <= std_logic_vector(to_signed(0,32));
      output_scale <= std_logic_vector(to_signed(256,32));
      output_offset <= std_logic_vector(to_signed(0,32));

      wait for 2500*clock_period;
      Trigger <= '0';

    end process; -- PROC_RST
 

end architecture;