----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : April 2019
-- 
-- Module Name: FET_Drive_tb
-- Project Name: uflp
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Drives the bias states for all cores
-- 
-- Dependencies: constants, sim_constants, sim_packages, uflp
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library uflp;

use std.env.finish;
use std.textio.all;

library sim_packages;
use sim_packages.sim_subprograms.all;
use sim_packages.sim_constants.all;

entity FET_Drive_tb is
end FET_Drive_tb; 

architecture sim of FET_Drive_tb is   
    component FET_Drive
    port(
        signal clk : in std_logic;
        signal rst : in std_logic;

        signal neg_bias : out std_logic;
        signal zero_bias : out std_logic;
        signal pos_bias : out std_logic
    );
    end component;

    -- Initialise Values
    signal clk : std_logic := '1';
    signal rst : std_logic := '1';

    signal neg_bias : std_logic := '0';
    signal zero_bias : std_logic := '0';
    signal pos_bias : std_logic := '0';

begin
    -- Generate clock as defined by sim_subprograms and sim_constants
    gen_clock(clk);

    DUT : entity uflp.FET_Drive(rtl)
    port map (
        clk => clk ,
        rst =>  rst ,
        neg_bias => neg_bias ,
        zero_bias => zero_bias ,
        pos_bias => pos_bias
    );

    -- Standard reset process
    PROC_RESET : process
    begin
        wait for 20*clock_period;
        rst <= '0';
        wait for 10000*clock_period;
    end process; -- PROC_RESET
end architecture;