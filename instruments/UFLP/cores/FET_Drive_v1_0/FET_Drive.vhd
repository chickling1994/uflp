----------------------------------------------------------------------------------
-- Company: University of Liverpool
-- Engineer: Christopher Hickling, PhD Student
-- Date : April 2019
-- 
-- Module Name: FET_Drive
-- Project Name: uflp
-- Target Devices: Red Pitaya
-- Tool Versions: Vivado 2019.1
-- Description: Drives the bias states for all cores
-- 
-- Dependencies: constants
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library packages;
use packages.constants.all;

entity FET_Drive is
  port (
    -- Generic to all cores
    clk : in std_logic;
    rst : in std_logic;

    bias_on_clock_cycles_ctl : in std_logic_vector(31 downto 0);
    
    -- Outputs from the core
    neg_bias : out std_logic;
    zero_bias : out std_logic;
    pos_bias : out std_logic
  );
end FET_Drive; 

architecture rtl of FET_Drive is
    type bias_state is (iSat, Temperature, vFloat);
    signal State : bias_state;
    signal Counter : unsigned(31 downto 0);
    begin
        -- purpose: Provide 3 signals that are on for a specified time and never overlap
        -- type   : synchronus with reset
        -- inputs : clk, rst
        -- outputs: neg_bias, zero_bias, pos_bias
    PROC_State : process(clk)
    -- Values are set in packages.constants
    
    -- e.g. bias_off_cycles = 1 ==> 2 clock cycles, 0 not possible
    constant bias_on_cycles : integer := bias_on_clock_cycles;
    constant bias_off_cycles : integer := bias_off_clock_cycles; -- +1 for clock cycle delay
    begin
        if rising_edge(clk) then
            if rst = '1' then
                -- Reset Values
                State <= iSat;
                -- bias_on_cycles <= to_integer(unsigned(bias_on_clock_cycles_ctl));
                neg_bias <= '0';
                zero_bias <= '0';
                pos_bias <= '0';
                Counter <= to_unsigned(0, Counter'length);
            else
                Counter <= Counter + 1;
                case State is
                    when iSat =>
                        if Counter < unsigned(bias_on_clock_cycles_ctl) then 
                            neg_bias <= '1';
                        else
                            State <= Temperature;
                            Counter <= to_unsigned(0, Counter'length);
                            neg_bias <= '0';
                        end if;
                        -- case to_integer(Counter) is
                        --     when 0 to bias_on_cycles - 1 =>
                        --         neg_bias <= '1';
                        --     when bias_on_cycles to bias_off_cycles + bias_on_cycles - 1 =>
                        --         neg_bias <= '0';
                        --     when bias_off_cycles + bias_on_cycles =>
                        --         Counter <= to_unsigned(0, Counter'length);
                        --         State <= Temperature;
                        --     when others =>
                        --         neg_bias <= '0';
                        --         Counter <= to_unsigned(0, Counter'length);
                        -- end case;
                    when Temperature =>
                        if Counter < unsigned(bias_on_clock_cycles_ctl) then 
                            pos_bias <= '1';
                        else
                            State <= vFloat;
                            Counter <= to_unsigned(0, Counter'length);
                            pos_bias <= '0';
                        end if;
                        -- case to_integer(Counter) is
                        --     when 0 to bias_on_cycles - 1 =>
                        --         pos_bias <= '1';
                        --     when bias_on_cycles to bias_off_cycles + bias_on_cycles - 1 =>
                        --         pos_bias <= '0';
                        --     when bias_off_cycles + bias_on_cycles =>
                        --         Counter <= to_unsigned(0, Counter'length);
                        --         State <= vFloat;
                        --     when others =>
                        --         pos_bias <= '0';
                        --         Counter <= to_unsigned(0, Counter'length);
                        -- end case;
                    when vFloat =>
                        if Counter < unsigned(bias_on_clock_cycles_ctl) then 
                            zero_bias <= '1';
                        else
                            State <= iSat;
                            Counter <= to_unsigned(0, Counter'length);
                            zero_bias <= '0';
                        end if;
                        -- case to_integer(Counter) is
                        --     when 0 to bias_on_cycles - 1 =>
                        --         zero_bias <= '1';
                        --     when bias_on_cycles to bias_off_cycles + bias_on_cycles - 1 =>
                        --         zero_bias <= '0';
                        --     when bias_off_cycles + bias_on_cycles =>
                        --         Counter <= to_unsigned(0, Counter'length);
                        --         State <= iSat;
                        --     when others =>
                        --         zero_bias <= '0';
                        --         Counter <= to_unsigned(0, Counter'length);
                        -- end case;
                end case;
                
            end if;
        end if;
    end process; -- PROC_State

end architecture;