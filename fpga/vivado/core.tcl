
set core_path [lindex $argv 0]
set part [lindex $argv 1]
set output_path [lindex $argv 2]

set core_name [lindex [split $core_path /] end]

set elements [split $core_name _]
set project_name [join [lrange $elements 0 end-2] _]
set version [string trimleft [join [lrange $elements end-1 end] .] v]

file delete -force $output_path/$core_name $output_path/$project_name.cache $output_path/$project_name.hw $output_path/$project_name.xpr

create_project -part $part $project_name $output_path


add_files -norecurse [glob $core_path/*.v*]

# Remove testbench files
set testbench_files [glob -nocomplain $core_path/*_tb.v*]
if {[llength testbench_files] > 0} {
  remove_files $testbench_files
}

puts $core_path
puts $core_name
set pc_path /home/chicklin/workspace/home-working
# set pc_path /media/2TB/workspace/chicklin
# set pc_path /home/chicklin/workspace
if {$core_name == "gen_cal_v1_0"} {
  read_vhdl -library sim_packages [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/generic_packages/sim_packages/*.v*]
  read_vhdl -library packages [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/generic_packages/*/*.v*]
  read_vhdl -library packages [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/generic_packages/*.v*]
  update_compile_order -fileset sources_1

  set testbench_files [glob -nocomplain /home/chicklin/workspace/home-working/koheron-sdk/instruments/calibration/cores/*/*_tb.v*]
  if {[llength testbench_files] > 0} {
    remove_files $testbench_files
  }

  set verification_comp_files [glob -nocomplain /home/chicklin/workspace/home-working/koheron-sdk/instruments/generic_packages/*/*_vc.v*]
  if {[llength verification_comp_files] > 0} {
    remove_files $verification_comp_files
  }


  set verification_comp_files [glob -nocomplain /home/chicklin/workspace/home-working/koheron-sdk/instruments/generic_packages/reset_v1_0/*]
  if {[llength verification_comp_files] > 0} {
    remove_files $verification_comp_files
  }
}

if {$core_name == "top_uflp_sim_v1_0"} {
  read_vhdl -library uflp [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/*/*.v*]
  read_vhdl -library packages [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/generic_packages/*/*_pck.v*]
  update_compile_order -fileset sources_1
  reorder_files -front [get_files [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/*/*_vc.vhd]]
  reorder_files -front [get_files [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/*/sim_*.vhd]]
  reorder_files -front [get_files [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/*/*lut*.vhd]]
  reorder_files -front [get_files [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/*/*_pck.vhd]]
  reorder_files -front [get_files [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/*/types_pck.vhd]]
  reorder_files -front [get_files [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/*/constants_pck.vhd]]
  reorder_files -back [get_files [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/*/top_mlp.vhd]]

  set testbench_files [glob -nocomplain /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/*/*_tb.v*]
  if {[llength testbench_files] > 0} {
    remove_files $testbench_files
  }

  set dont_want [glob -nocomplain /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/*/sub_programs_pck_.v*]
  if {[llength dont_want] > 0} {
    remove_files $dont_want
  }

  set verification_comp_files [glob -nocomplain /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/*/*_vc.v*]
  if {[llength verification_comp_files] > 0} {
    remove_files $verification_comp_files
  }

read_vhdl -library pcr [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/LP_Current/cores/*/*.v*]
  update_compile_order -fileset sources_1
  set_property library packages [get_files [glob  /home/chicklin/workspace/home-working/koheron-sdk/instruments/LP_Current/cores/packages_v1_0/*_pck.vhd]]
  set_property library uflp [get_files [glob  /home/chicklin/workspace/home-working/koheron-sdk/instruments/LP_Current/cores/*/reset.vhd]]
  update_compile_order -fileset sources_1
  # reorder_files -front [get_files [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/LP_Current/cores/*/*_vc.vhd]]
  # reorder_files -front [get_files [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/LP_Current/cores/*/sim_*.vhd]]
  # reorder_files -front [get_files [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/LP_Current/cores/*/*lut*.vhd]]
  # reorder_files -front [get_files [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/LP_Current/cores/*/*_pck.vhd]]
  # reorder_files -front [get_files [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/LP_Current/cores/*/types_pck.vhd]]
  # reorder_files -front [get_files [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/LP_Current/cores/*/constants_pck.vhd]]
  # reorder_files -back [get_files [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/LP_Current/cores/*/top_pcr.vhd]]

  set pcr_testbench_files [glob -nocomplain /home/chicklin/workspace/home-working/koheron-sdk/instruments/LP_Current/cores/*/*_tb.v*]
  if {[llength testbench_files] > 0} {
    remove_files $pcr_testbench_files
  }


}

if {$core_name == "top_mlp_v1_0"} {
  read_vhdl -library uflp [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/*/*.v*]
  read_vhdl -library packages [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/generic_packages/*/*.v*]
  read_vhdl -library packages [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/generic_packages/*.v*]
  update_compile_order -fileset sources_1

  set testbench_files [glob -nocomplain /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/*/*_tb.v*]
  if {[llength testbench_files] > 0} {
    remove_files $testbench_files
  }

  set verification_comp_files [glob -nocomplain /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/*/*_vc.v*]
  if {[llength verification_comp_files] > 0} {
    remove_files $verification_comp_files
  }
}

if {$core_name == "top_pcr_v1_0"} {
  read_vhdl -library pcr [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/LP_Current/cores/*/*.v*]
  read_vhdl -library packages [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/generic_packages/*/*.v*]
  read_vhdl -library packages [glob /home/chicklin/workspace/home-working/koheron-sdk/instruments/generic_packages/*.v*]
  update_compile_order -fileset sources_1

  set testbench_files [glob -nocomplain /home/chicklin/workspace/home-working/koheron-sdk/instruments/LP_Current/cores/*/*_tb.v*]
  if {[llength testbench_files] > 0} {
    remove_files $testbench_files
  }

  set verification_comp_files [glob -nocomplain /home/chicklin/workspace/home-working/koheron-sdk/instruments/LP_Current/cores/*/*_vc.v*]
  if {[llength verification_comp_files] > 0} {
    remove_files $verification_comp_files
  }
}

ipx::package_project -import_files -root_dir $output_path/$core_name

set core [ipx::current_core]

set_property VERSION $version $core
set_property NAME $project_name $core
set_property LIBRARY {user} $core
set_property SUPPORTED_FAMILIES {zynq Production} $core

proc core_parameter {name display_name description} {
  set core [ipx::current_core]

  set parameter [ipx::get_user_parameters $name -of_objects $core]
  set_property DISPLAY_NAME $display_name $parameter
  set_property DESCRIPTION $description $parameter

  set parameter [ipgui::get_guiparamspec -name $name -component $core]
  set_property DISPLAY_NAME $display_name $parameter
  set_property TOOLTIP $description $parameter
}

source $core_path/core_config.tcl

# set_property library packages [get_files  /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/packages/constants_pck.vhd]
# set_property library packages [get_files  /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/packages/constants_pck.vhd]
# set_property library packages [get_files  /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/packages/iSat_LUT_pck.vhd]
# set_property library packages [get_files  /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/packages/temp_LUT_pck.vhd]
# set_property library packages [get_files  /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/packages/vFloat_LUT_pck.vhd]
# set_property library packages [get_files  /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/packages/types_pck.vhd]

# set_property library uflp [get_files  /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/bias_set_v1_0/bias_set.vhd]
# set_property library uflp [get_files  /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/capacitor_switch_v1_0/capacitor_switch.vhd]
# set_property library uflp [get_files  /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/capacitor_switch_v1_0/capacitor_switch.vhd]
# set_property library uflp [get_files  /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/FET_Drive_v1_0/FET_Drive.vhd]
# set_property library uflp [get_files  /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/iSat_v1_0/iSat.vhd]
# set_property library uflp [get_files  /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/reset_v1_0/reset.vhd]
# set_property library uflp [get_files  /home/chicklin/workspace/home-working/koheron-sdk/instruments/UFLP/cores/temp_calc_v1_0/temp_calc.vhd]

rename core_parameter {}

ipx::create_xgui_files $core
ipx::update_checksums $core
ipx::save_core $core

close_project
